package com.icesoft.test.token;


import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * URL安全的Base64编码和解码
 */

public final class UrlSafeBase64 {

    private UrlSafeBase64() {
    }   // don't instantiate

    /**
     * 编码字符串
     *
     * @param data 待编码字符串
     * @return 结果字符串
     */
    public static String encodeToString(String data) {
        return encodeToString(data.getBytes(StandardCharsets.UTF_8));
    }

    /**
     * 编码数据
     *
     * @param data 字节数组
     * @return 结果字符串
     */
    public static String encodeToString(byte[] data) {
        return new String(Base64.getUrlEncoder().encode(data), StandardCharsets.US_ASCII);
    }

    /**
     * 解码数据
     *
     * @param data 编码过的字符串
     * @return 原始数据
     */
    public static byte[] decode(String data) {
        return Base64.getUrlDecoder().decode(data.getBytes(StandardCharsets.US_ASCII));
    }

    public static String decodeToString(String data) {
        return new String(decode(data), StandardCharsets.UTF_8);
    }
}
