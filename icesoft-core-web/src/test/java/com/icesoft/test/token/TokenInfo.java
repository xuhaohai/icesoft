package com.icesoft.test.token;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.icesoft.core.web.suppose.token.IAuthToken;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TokenInfo implements IAuthToken {
    /**
     * 是否需要token访问
     */
    private boolean security;
    /**
     * 到期时间，单位秒
     */
    private long deadline;

    /**
     * 随机字符串
     */
    private String nonceStr;
    /**
     * 项目名
     */
    private String application;
}
