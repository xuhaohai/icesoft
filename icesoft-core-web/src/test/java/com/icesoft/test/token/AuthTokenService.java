package com.icesoft.test.token;

import com.icesoft.core.web.suppose.token.IAuthToken;
import com.icesoft.core.web.suppose.token.service.IAuthTokenService;
import org.springframework.stereotype.Component;

@Component
public class AuthTokenService implements IAuthTokenService {
    @Override
    public IAuthToken findByToken(String token) {
        TokenInfo authToken = new TokenInfo();
        authToken.setApplication(token);
        return authToken;
    }
}
