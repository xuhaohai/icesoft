package com.icesoft.test.web.safehttp;

import com.icesoft.core.common.helper.Resp;
import com.icesoft.core.web.suppose.safehttp.SafeRequest;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Validated
@RequestMapping("app/test")
@SafeRequest
public interface TestAppRequestApi {

	@PostMapping("paramTest")
	public Resp<TestDTO> paramTest(@RequestParam int id, @RequestParam String name, @RequestParam("phone") String tel,
			@NotNull Date startTime,
			@RequestParam(required = false, defaultValue = "TRUCK_DELIVERY") DeliveryType deliveryType,
			@Valid TestDTO testDTO);
}
