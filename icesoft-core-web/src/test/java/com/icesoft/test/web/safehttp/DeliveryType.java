package com.icesoft.test.web.safehttp;

public enum DeliveryType {

    /**
     * 自提
     */
    SELF_MENTION("个人自提", "0"),
    /**
     * 货车出货
     */
    TRUCK_DELIVERY("货车出货", "1"),
    ;

    private String explain;
    private String code;

    DeliveryType(String explain, String code) {
        this.explain = explain;
        this.code = code;
    }

    public String getExplain() {
        return explain;
    }

    public void setExplain(String explain) {
        this.explain = explain;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return this.explain + "_" + this.code;
    }
}
