package com.icesoft.test.web.safehttp;

import com.icesoft.core.common.helper.Resp;
import com.icesoft.core.web.suppose.mapping.HeaderMapping;
import com.icesoft.core.web.suppose.mapping.VersionCodeMapping;
import com.icesoft.core.web.suppose.sms.SmsCodeCheck;
import com.icesoft.core.web.suppose.token.AuthTokenCheck;
import com.icesoft.test.token.TokenInfo;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Date;

@RestController
public class TestAppRequestController implements TestAppRequestApi {
    @Override
    public Resp<TestDTO> paramTest(int id, String name, String tel, Date startTime, DeliveryType deliveryType,
                                   TestDTO testDTO) {
        Assert.notNull(testDTO, "testDTO is null");
        Assert.notNull(deliveryType, "deliveryType is null");
        return Resp.success(testDTO);
    }


    @PostMapping("testRequestBody")
    public Resp<TestDTO> testRequestBody(@Valid TestDTO testDTO) {
        Assert.notNull(testDTO, "testDTO is null");
        return Resp.success(testDTO);
    }

    @PostMapping("testApiVersion")
    public Resp<String> testApiVersionMiss(@RequestParam(required = false, defaultValue = "0") int id) {
        return Resp.success(null);
    }

    @PostMapping("testApiVersion")
    @HeaderMapping(name = "versionCode", value = "1")
    public Resp<String> testApiVersion(String name) {
        return Resp.success(name);
    }

    @PostMapping("testApiVersion")
    @HeaderMapping(name = "versionCode", value = "2")
    public Resp<String> testApiVersion() {
        return Resp.error("error");
    }

    @PostMapping("testApiVersion")
    @VersionCodeMapping(min = 10)
    public Resp<Integer> testApiVersion10(HttpServletRequest request) {
        return Resp.success(10);
    }

    @PostMapping("testApiVersion")
    @VersionCodeMapping(min = 15)
    public Resp<Integer> testApiVersion15(HttpServletRequest request) {
        return Resp.success(15);
    }

    @PostMapping("testApiVersionMin")
    @VersionCodeMapping(min = 10)
    public Resp<String> testApiVersionMin(HttpServletRequest request) {
        return Resp.success(request.getHeader("versionCode"));
    }

    @PostMapping("testSmsCodeFilter")
    @SmsCodeCheck
    public Resp<String> testSmsCodeFilter(String smsCode, String phone) {
        return Resp.success(phone);

    }

    @RequestMapping("authTokenTest")
    @AuthTokenCheck
    public Resp<TokenInfo> authTokenTest(TokenInfo authToken, String name) {
        assert name != null;
        return Resp.success(authToken);
    }
}
