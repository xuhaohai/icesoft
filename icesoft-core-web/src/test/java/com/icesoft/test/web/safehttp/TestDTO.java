package com.icesoft.test.web.safehttp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.icesoft.core.common.helper.ConstValue;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class TestDTO {
    @NotNull
    private String user;
    @Min(value = 0, message = "年龄不能小于0")
    @Max(value = 200, message = "年龄不能大于200")
    private int age;
    @NotNull
    @JsonFormat(pattern = ConstValue.DATE_TIME_PATTEN)
    private Date endTime;
}
