package com.icesoft.test.web.safehttp;

import com.fasterxml.jackson.core.type.TypeReference;
import com.icesoft.core.common.helper.Resp;
import com.icesoft.core.web.model.SmsCodeDTO;
import com.icesoft.core.web.service.SmsService;
import com.icesoft.core.web.suppose.sms.config.SmsCodeCacheService;
import com.icesoft.mock.builder.SafeRequestMockBuilder;
import com.icesoft.mock.service.MockService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableAutoConfiguration
@AutoConfigureMockMvc
public class SmsCodeTest {
    @Autowired
    private MockService mockService;
    @MockBean
    private SmsService smsService;
    @SpyBean
    private SmsCodeCacheService smsCodeCacheService;

    @Test
    public void testSendCode() {
        Mockito.when(smsService.sendSms(Mockito.anyString(), Mockito.anyString())).thenReturn(true);
        SafeRequestMockBuilder mockBuilder = SafeRequestMockBuilder.httpPost("/api/sms/sendCode");
        mockBuilder.param("phone", "19999125760");
        Resp<String> resp = mockService.safeRequest(mockBuilder, new TypeReference<Resp<String>>() {
        });
        Assertions.assertEquals(true, resp.isSuccess());

        mockBuilder = SafeRequestMockBuilder.httpPost("/api/sms/sendCode");
        mockBuilder.param("phone", "19999125761");
        resp = mockService.safeRequest(mockBuilder, new TypeReference<Resp<String>>() {
        });
        Assertions.assertEquals(false, resp.isSuccess());
        Assertions.assertEquals("请不要频繁发送", resp.getMsg());
        Mockito.verify(smsService, Mockito.only()).sendSms(Mockito.anyString(), Mockito.anyString());

    }

    @Test
    public void testSendCodeCheckFilter() {
        String phone = "19999125760";
        SafeRequestMockBuilder mockBuilder = SafeRequestMockBuilder.httpPost("/app/test/testSmsCodeFilter");
        SmsCodeDTO smsCodeDTO = SmsCodeDTO.builder().code("123456").createTime(System.currentTimeMillis())
                .phone(phone).build();
        Mockito.when(smsCodeCacheService.getCache(phone)).thenReturn(smsCodeDTO);
        mockBuilder.param("phone", smsCodeDTO.getPhone());
        mockBuilder.param("smsCode", smsCodeDTO.getCode());
        Resp<String> resp = mockService.safeRequest(mockBuilder, new TypeReference<Resp<String>>() {
        });
		Assertions.assertEquals(true, resp.isSuccess());
		Assertions.assertEquals(smsCodeDTO.getPhone(), resp.getData());
    }
}
