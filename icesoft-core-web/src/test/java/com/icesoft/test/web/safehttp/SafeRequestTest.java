package com.icesoft.test.web.safehttp;

import com.fasterxml.jackson.core.type.TypeReference;
import com.icesoft.Application;
import com.icesoft.core.common.helper.Resp;
import com.icesoft.core.common.helper.RespCode;
import com.icesoft.core.web.helper.http.SafeRequestHttpHelp;
import com.icesoft.mock.builder.SafeRequestMockBuilder;
import com.icesoft.mock.service.MockService;
import com.icesoft.test.token.TokenInfo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.HashMap;
import java.util.Map;

@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@EnableAutoConfiguration
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class SafeRequestTest {
    @Autowired
    private MockService mockService;

    private static String PRE_URL = "/app/test/";
    private static String BASE_URL = "http://localhost:8080/";

    @Test
    public void testParam() {
        Map<String, String> param = new HashMap<>();
        param.put("id", "1");
        param.put("name", "nameVale在震");
        param.put("phone", "18195994181");
        param.put("startTime", "2019-05-27 12:05:27");
        param.put("endTime", "2019-05-27 12:05:27");
        param.put("user", "uuu");
        // param.put("deliveryType", "SELF_MENTION");
        param.put("age", "50");
        SafeRequestMockBuilder mockBuilder = SafeRequestMockBuilder.httpPost(PRE_URL + "paramTest");
        mockBuilder.addParams(param);
        Resp<TestDTO> resp = mockService.safeRequest(mockBuilder, new TypeReference<Resp<TestDTO>>() {
        });
        Assertions.assertNotNull(resp.getData());
    }

    @Test
    public void testRequestBody() {
        Map<String, String> param = new HashMap<>();
        param.put("endTime", "2019-05-27 12:05:27");
        param.put("user", "uuu");
        param.put("age", "50");
        SafeRequestMockBuilder mockBuilder = SafeRequestMockBuilder.httpPost(PRE_URL + "testRequestBody");
        mockBuilder.addParams(param);
        Resp<TestDTO> resp = mockService.safeRequest(mockBuilder, new TypeReference<Resp<TestDTO>>() {
        });
        Assertions.assertNotNull(resp.getData());

        resp = SafeRequestHttpHelp.builder(BASE_URL, "app/test/testRequestBody").params(param)
                .post(new TypeReference<Resp<TestDTO>>() {
                });
        Assertions.assertNotNull(resp.getData());
    }

    @Test
    public void testParamMissName() {
        Map<String, String> param = new HashMap<>();
        param.put("id", "1");
        // param.put("name", "nameVale");
        param.put("tel", "18195994181");
        param.put("startTime", "2019-05-27 12:05:27");
        param.put("endTime", "2019-05-27 12:05:27");
        param.put("user", "uuu");
        param.put("deliveryType", "SELF_MENTION");
        param.put("age", "50");
        SafeRequestMockBuilder mockBuilder = SafeRequestMockBuilder.httpPost(PRE_URL + "paramTest");
        mockBuilder.addParams(param);
        Resp<TestDTO> resp = mockService.safeRequest(mockBuilder, new TypeReference<Resp<TestDTO>>() {
        });
        Assertions.assertEquals(RespCode.FAIL, resp.getCode());
        Assertions.assertEquals("缺少参数name", resp.getMsg());
    }

    @Test
    public void testParamMissPhone() {
        Map<String, String> param = new HashMap<>();
        param.put("id", "1");
        param.put("name", "nameVale");
        // param.put("tel", "18195994181");
        param.put("startTime", "2019-05-27 12:05:27");
        param.put("endTime", "2019-05-27 12:05:27");
        param.put("user", "uuu");
        param.put("deliveryType", "SELF_MENTION");
        param.put("age", "50");
        SafeRequestMockBuilder mockBuilder = SafeRequestMockBuilder.httpPost(PRE_URL + "paramTest");
        mockBuilder.addParams(param);
        Resp<TestDTO> resp = mockService.safeRequest(mockBuilder, new TypeReference<Resp<TestDTO>>() {
        });
        Assertions.assertEquals(RespCode.FAIL, resp.getCode());
        Assertions.assertEquals("缺少参数phone", resp.getMsg());
    }

    @Test
    public void testVersionCode1() {
        Map<String, String> param = new HashMap<>();
        String name = "test";
        param.put("name", name);
        Map<String, String> header = new HashMap<>();
        header.put("versionCode", "1");
        SafeRequestMockBuilder mockBuilder = SafeRequestMockBuilder.httpPost(PRE_URL + "testApiVersion");
        mockBuilder.addParams(param).addHeaders(header);
        Resp<String> resp = mockService.safeRequest(mockBuilder, new TypeReference<Resp<String>>() {
        });
        Assertions.assertEquals(RespCode.SUCCESS, resp.getCode());
        Assertions.assertEquals(name, resp.getData());
    }

    @Test
    public void testVersionCode2() {
        Map<String, String> header = new HashMap<>();
        String versionCode = "2";
        header.put("versionCode", versionCode);
        SafeRequestMockBuilder mockBuilder = SafeRequestMockBuilder.httpPost(PRE_URL + "testApiVersion");
        mockBuilder.addHeaders(header);
        Resp<String> resp = mockService.safeRequest(mockBuilder, new TypeReference<Resp<String>>() {
        });
        Assertions.assertEquals(RespCode.FAIL, resp.getCode());
    }

    @Test
    public void testVersionCodeMin() {
        Map<String, String> header = new HashMap<>();
        String versionCode = "11";
        header.put("versionCode", versionCode);
        SafeRequestMockBuilder mockBuilder = SafeRequestMockBuilder.httpPost(PRE_URL + "testApiVersionMin");
        mockBuilder.addHeaders(header);
        Resp<String> resp = mockService.safeRequest(mockBuilder, new TypeReference<Resp<String>>() {
        });
        Assertions.assertEquals(RespCode.SUCCESS, resp.getCode());
        Assertions.assertEquals(versionCode, resp.getData());
    }

    @Test
    public void testVersionCodeMin10() {
        Map<String, String> header = new HashMap<>();
        String versionCode = "11";
        header.put("versionCode", versionCode);
        SafeRequestMockBuilder mockBuilder = SafeRequestMockBuilder.httpPost(PRE_URL + "testApiVersion");
        mockBuilder.addHeaders(header);
        Resp<Integer> resp = mockService.safeRequest(mockBuilder, new TypeReference<Resp<Integer>>() {
        });
        Assertions.assertEquals(RespCode.SUCCESS, resp.getCode());
        Assertions.assertEquals(10, (int) resp.getData());
    }

    @Test
    public void testVersionCodeMin15() {
        Map<String, String> header = new HashMap<>();
        String versionCode = "18";
        header.put("versionCode", versionCode);
        SafeRequestMockBuilder mockBuilder = SafeRequestMockBuilder.httpPost(PRE_URL + "testApiVersion");
        mockBuilder.addHeaders(header);
        Resp<Integer> resp = mockService.safeRequest(mockBuilder, new TypeReference<Resp<Integer>>() {
        });
        Assertions.assertEquals(RespCode.SUCCESS, resp.getCode());
        Assertions.assertTrue(resp.getData() == 15);
    }

    @Test
    public void testVersionCodeLess() {
        Map<String, String> header = new HashMap<>();
        String versionCode = "8";
        header.put("versionCode", versionCode);
        SafeRequestMockBuilder mockBuilder = SafeRequestMockBuilder.httpPost(PRE_URL + "testApiVersion");
        mockBuilder.addHeaders(header);
        Resp<Integer> resp = mockService.safeRequest(mockBuilder, new TypeReference<Resp<Integer>>() {
        });
        Assertions.assertEquals(RespCode.SUCCESS, resp.getCode());
        Assertions.assertEquals(null, resp.getData());
    }

    @Test
    public void testAuthTokenRequest() {
        Map<String, String> param = new HashMap<>();
        param.put("name", "test");
        Map<String, String> header = new HashMap<>();
        header.put("token", "123456token");
        SafeRequestMockBuilder mockBuilder = SafeRequestMockBuilder.httpPost(PRE_URL + "authTokenTest");
        mockBuilder.addHeaders(header);
        mockBuilder.addParams(param);
        Resp<TokenInfo> resp = mockService.safeRequest(mockBuilder, new TypeReference<Resp<TokenInfo>>() {
        });
        Assertions.assertEquals(RespCode.SUCCESS, resp.getCode());
        Assertions.assertNotNull(resp.getData());

        resp = SafeRequestHttpHelp.builder(BASE_URL, PRE_URL + "authTokenTest")
                .params(param)
                .headers(header)
                .post(new TypeReference<Resp<TokenInfo>>() {
                });
        Assertions.assertEquals(RespCode.SUCCESS, resp.getCode());
        Assertions.assertNotNull(resp.getData().getApplication());
        Assertions.assertEquals(resp.getData().getApplication(),"123456token");
    }
}
