;(function (root, factory) {
    if (typeof exports === "object") {
        // CommonJS
        module.exports = exports = factory();
    } else if (typeof define === "function" && define.amd) {
        // AMD
        define([], factory);
    } else {
        // Global (browser)
        root.rsaKey = factory();
    }
}(this, function () {
    return {
        keyId: "${keyId}",
        publicKey: "${publicKey}",
        algorithm: "${algorithm}"
    };
}));



