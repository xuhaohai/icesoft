package com.icesoft.mock.base;

import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.icesoft.core.common.helper.Resp;
import com.icesoft.mock.builder.SafeRequestMockBuilder;

public interface ISafeRequestMockListener extends IMockListener {

	default void beforeHttp(SafeRequestMockBuilder mockBuilder,
                            MockHttpServletRequestBuilder mockHttpServletRequestBuilder) {
	}

	default void afterDecrypt(String json) {
	}

	default void handleResp(Resp<?> resp) {
	}
}
