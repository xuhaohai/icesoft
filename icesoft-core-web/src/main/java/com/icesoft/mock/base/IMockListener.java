package com.icesoft.mock.base;

import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import com.icesoft.mock.builder.MockBuilder;

public interface IMockListener {

	default void beforeBuild(MockBuilder mockBuilder) {
	}

	default void beforeHttp(MockBuilder mockBuilder, MockHttpServletRequestBuilder mockHttpServletRequestBuilder) {
	}

	default void afterHttp(String content) {
	}

}
