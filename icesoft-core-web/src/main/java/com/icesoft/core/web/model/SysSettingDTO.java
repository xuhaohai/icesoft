package com.icesoft.core.web.model;

import lombok.Data;

@Data
public class SysSettingDTO  {

	private String groupName;
	private String des;
	private String keyName;
	private String val;
	
}
