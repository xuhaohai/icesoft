package com.icesoft.core.web.suppose.safehttp.service;

import com.icesoft.core.web.model.CryptoKeyPair;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
//@ConditionalOnMissingBean
public class MapCryptoKeyPairPairRepository implements ICryptoKeyPairRepository {

    private Map<String, CryptoKeyPair> cache;

    MapCryptoKeyPairPairRepository() {
        cache = new ConcurrentHashMap<>();
    }

    @Override
    @Nullable
    public CryptoKeyPair findKey(String keyId) {
        return cache.get(keyId);
    }

    @Override
    public Collection<CryptoKeyPair> findAllKey() {
        return cache.values();
    }

    @Override
    public void saveKey(CryptoKeyPair cryptoKeyPair) {
        cache.put(cryptoKeyPair.getKeyId(), cryptoKeyPair);
    }

    @Override
    public void deleteKey(String keyId) {
        cache.remove(keyId);
    }

}
