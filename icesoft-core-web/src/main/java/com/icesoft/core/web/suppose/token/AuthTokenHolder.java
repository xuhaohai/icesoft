package com.icesoft.core.web.suppose.token;

import com.icesoft.core.web.helper.RequestHold;
import com.icesoft.core.web.suppose.token.filter.RequiredAuthTokenFilter;

import javax.servlet.http.HttpServletRequest;

public abstract class AuthTokenHolder {
    public static IAuthToken getAuthToken() {
        return getAuthToken(RequestHold.getRequest());
    }

    public static IAuthToken getAuthToken(HttpServletRequest request) {
        return (IAuthToken) request.getAttribute(RequiredAuthTokenFilter.ATTRIBUTE_AUTH_TOKEN);
    }
}
