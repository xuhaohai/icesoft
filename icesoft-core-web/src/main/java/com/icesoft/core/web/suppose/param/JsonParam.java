package com.icesoft.core.web.suppose.param;


import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface JsonParam {

	/**
	 * Alias for {@link #name}.
	 */
	@AliasFor("name")
	String value() default "";

	/**
	 * The name of the request parameter json to bind to.
	 */
	@AliasFor("value")
	String name() default "";

	Class<?> type() default Object.class;
}
