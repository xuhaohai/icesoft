package com.icesoft.core.web.suppose.mapping;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@ConditionMapping
public @interface VersionCodeMapping {
	/**
	 * 请求头参数名
	 */
	String name() default "versionCode";

	/**
	 * 最小版本号
	 */
	int min();
}
