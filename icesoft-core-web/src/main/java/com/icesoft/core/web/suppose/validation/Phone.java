package com.icesoft.core.web.suppose.validation;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.icesoft.core.web.suppose.validation.impl.PhoneValidator;

@Target({ FIELD, METHOD, PARAMETER, ANNOTATION_TYPE })  
@Retention(RUNTIME)  
//指定验证器  
@Constraint(validatedBy = PhoneValidator.class)
@Documented  
public @interface Phone {
  
    //默认错误消息  
    String message() default "请输入合法的电话号码";

    //分组
    Class<?>[] groups() default { };  
  
    //负载  
    Class<? extends Payload>[] payload() default { };  
  
}
