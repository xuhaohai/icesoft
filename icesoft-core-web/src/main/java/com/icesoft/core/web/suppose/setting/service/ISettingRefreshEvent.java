package com.icesoft.core.web.suppose.setting.service;

import com.icesoft.core.web.suppose.setting.BasePropertySetting;

public interface ISettingRefreshEvent<T extends BasePropertySetting<?>> {

	T getSetting();

	void refresh(String keyName, String value);
}
