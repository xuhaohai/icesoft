package com.icesoft.core.web.helper.excel;

import java.util.Collection;
import java.util.Map;

import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.eventusermodel.XSSFSheetXMLHandler;
import org.apache.poi.xssf.usermodel.XSSFComment;

class XlsxHandler implements XSSFSheetXMLHandler.SheetContentsHandler {

	private int sheet;
	private int row = 0;
	private CellRead cellRead;

	public XlsxHandler() {
		super();
		this.cellRead = new CellRead();
		sheet = -1;
	}

	@Override
	public void startRow(int rowNum) {
		this.row = rowNum;
		if (rowNum == 0) {
			sheet++;
		}
	}

	@Override
	public void endRow(int rowNum) {
	}

	@Override
	public void cell(String cellReference, String formattedValue, XSSFComment comment) {
		int column = (new CellReference(cellReference)).getCol();
		cellRead.handle(formattedValue, sheet, row, column);
	}

	public Collection<Map<String, String>> getDataMap() {
		return cellRead.getDataMap();
	}
}
