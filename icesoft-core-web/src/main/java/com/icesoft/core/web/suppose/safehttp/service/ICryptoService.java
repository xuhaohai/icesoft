package com.icesoft.core.web.suppose.safehttp.service;

public interface ICryptoService {

    /**
     * 公钥加密
     */
    String publicEncrypt(String publicKey, String data);

    /**
     * 私钥解密
     */
    String privateDecrypt(String privateKey, String data);

    /**
     * 对称加密
     */
    String symmetricEncrypt(String key, String data);

    /**
     * 对称解密
     */
    String symmetricDecrypt(String key, String data);

}
