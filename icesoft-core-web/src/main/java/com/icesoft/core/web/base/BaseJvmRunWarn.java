package com.icesoft.core.web.base;

public interface BaseJvmRunWarn {

	public void tomcatThreadWarn();

	public default int tomcatThreadWarnCount() {
		return 50;
	}

	public void JdbcConnectionWarn();

	public default int JdbcConnectionWarnCount() {
		return 10;
	}
}
