package com.icesoft.core.web.suppose.sms;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 进行短信验证码验证，要先调用系统提供的短信发送接口
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface SmsCodeCheck {
	/**
	 * 要校验的验证码请求参数名
	 */
	String smsCode() default "smsCode";

	/**
	 * 要检查的手机号请求参数名，检查此次请求手机号与发送验证码请求的手机号一致
	 */
	String phone() default "phone";
}
