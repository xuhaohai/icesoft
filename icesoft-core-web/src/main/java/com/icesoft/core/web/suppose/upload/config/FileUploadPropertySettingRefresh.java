package com.icesoft.core.web.suppose.upload.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.icesoft.core.web.suppose.setting.service.ISettingRefreshEvent;

@Component
public class FileUploadPropertySettingRefresh implements ISettingRefreshEvent<FileUploadPropertySetting> {

	@Autowired
	FileUploadPropertySetting fileUploadPropertySetting;

	@Override
	public FileUploadPropertySetting getSetting() {
		return fileUploadPropertySetting;
	}

	@Override
	public void refresh(String keyName, String value) {
		fileUploadPropertySetting.refresh();

	}

}
