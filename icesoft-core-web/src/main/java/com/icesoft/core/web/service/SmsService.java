package com.icesoft.core.web.service;

public interface SmsService {
	boolean sendSms(String phone, String content);

}
