package com.icesoft.core.web.suppose.setting;

public interface BaseSettingEnum {

	public String getDes();

	public String getDefaultValue();
}
