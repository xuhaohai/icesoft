package com.icesoft.core.web.suppose.safehttp.service;

import com.icesoft.core.web.model.CryptoKeyPair;

import java.util.Collection;

public interface ICryptoKeyPairRepository {

    CryptoKeyPair findKey(String keyId);

    Collection<CryptoKeyPair> findAllKey();

    void saveKey(CryptoKeyPair cryptoKeyPair);

    void deleteKey(String keyId);

}
