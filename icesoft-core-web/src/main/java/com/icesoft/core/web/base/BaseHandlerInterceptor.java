package com.icesoft.core.web.base;

import java.util.List;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public abstract class BaseHandlerInterceptor extends HandlerInterceptorAdapter {

	public abstract String pathPatten();

	public List<String> excludePathPatterns() {
		return null;
	}

}
