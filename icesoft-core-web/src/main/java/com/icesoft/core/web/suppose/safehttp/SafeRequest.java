package com.icesoft.core.web.suppose.safehttp;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD, ElementType.TYPE })
@Documented
@Inherited
// 最高优先级
@Order(Ordered.HIGHEST_PRECEDENCE)
public @interface SafeRequest {
	/**
	 * 是否禁用
	 * */
	boolean disable() default false;
}
