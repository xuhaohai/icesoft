package com.icesoft.core.web.configurer.test.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.web.servlet.MockMvc;

import com.icesoft.core.web.configurer.test.condition.ConditionOnTestCase;
import com.icesoft.mock.service.MockService;

@ConditionOnTestCase
@Configuration
public class MockServiceMockTestConfig {

	@Autowired(required = false)
	MockMvc mockMvc;

	@Bean
	@ConditionalOnMissingBean
	public MockService mockService() {
		if (mockMvc == null) {
			return null;
		}
		return new MockService(mockMvc);
	}
}
