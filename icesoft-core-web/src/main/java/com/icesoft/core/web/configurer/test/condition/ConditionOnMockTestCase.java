package com.icesoft.core.web.configurer.test.condition;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.context.annotation.Conditional;

/**
 * 单元测试使用@SpringBootTest()或@SpringBootTest(webEnvironment =
 * WebEnvironment.MOCK)时生效 {@link SpringBootTest} {@link WebEnvironment.MOCK )}
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE, ElementType.METHOD })
@Documented
@Conditional(MockTestCaseCondition.class)
public @interface ConditionOnMockTestCase {
}
