package com.icesoft.core.web.suppose.validation.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.util.StringUtils;

import com.icesoft.core.common.util.RegexUtils;
import com.icesoft.core.web.suppose.validation.IpAddress;

public class IpAddressValidator implements ConstraintValidator<IpAddress, String> {

	@Override
	public void initialize(IpAddress constraintAnnotation) {
		// 初始化，得到注解数据
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (StringUtils.isEmpty(value)) {
			return true;
		}
		return RegexUtils.isIpAddress(value);
	}
}
