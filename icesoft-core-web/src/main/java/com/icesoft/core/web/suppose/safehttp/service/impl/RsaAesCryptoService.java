package com.icesoft.core.web.suppose.safehttp.service.impl;

import com.icesoft.core.common.util.AesUtil;
import com.icesoft.core.common.util.KeyPairUtils;
import com.icesoft.core.common.util.RsaUtil;
import com.icesoft.core.web.suppose.safehttp.service.ICryptoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

@ConditionalOnProperty(name = "icesoft.safe.sm-crypto-enable", havingValue = "false", matchIfMissing = true)
@Component
@Slf4j
public class RsaAesCryptoService implements ICryptoService {

    @Override
    public String publicEncrypt(String publicKey, String data) {
        RSAPublicKey rsaPublicKey = (RSAPublicKey) KeyPairUtils.getPublicKey(publicKey);
        return RsaUtil.encryptWithBase64(rsaPublicKey, data);
    }

    @Override
    public String privateDecrypt(String privateKey, String data) {
        RSAPrivateKey rsaPrivateKey = (RSAPrivateKey) KeyPairUtils.getPrivateKey(privateKey);
        return RsaUtil.decryptWithBase64(rsaPrivateKey, data);
    }

    @Override
    public String symmetricEncrypt(String key, String data) {
        return AesUtil.encrypt(key, data);
    }

    @Override
    public String symmetricDecrypt(String key, String data) {
        return AesUtil.decrypt(key, data);
    }

}
