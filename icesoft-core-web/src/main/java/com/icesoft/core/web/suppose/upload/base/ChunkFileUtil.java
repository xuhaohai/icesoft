package com.icesoft.core.web.suppose.upload.base;

import com.icesoft.core.common.exception.CheckException;
import com.icesoft.core.common.util.FileUtil;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

public abstract class ChunkFileUtil {

    public static void write(ChunkFileDTO chunkFileDTO, File outFile, byte[] fileData) {
        try {
            FileUtil.checkFileCanWrite(outFile);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        long chunk = chunkFileDTO.chunk;
        int chunkSize = chunkFileDTO.chunkSize;
        try (RandomAccessFile raf = new RandomAccessFile(outFile, "rw")) {
            long offset = (chunk - 1) * chunkSize;
            raf.seek(offset);
            raf.write(fileData);
        } catch (IOException e) {
            throw new CheckException("文件写入失败，请重试", e);
        }
    }

}
