package com.icesoft.core.web.suppose.upload.base;

import com.icesoft.core.common.exception.CheckException;
import com.icesoft.core.common.exception.CheckNoLogException;
import com.icesoft.core.web.helper.PathUtil;

import java.io.File;

public interface BaseUploadPathConfig {
    /**
     * 上传完成后的处理
     *
     * @param outFile 最后一片完成 ，并且合并的文件
     */
    default void uploadFinish(File outFile) {
        if (!outFile.exists()) {
            throw new CheckException("上传失败，文件不存在");
        }
    }

    /**
     * 最终文件保存路径，也就是碎片合并后保存的文件路径
     */
    default String getOutFilePath(String md5, String originalFileName) {
        String ext = getExt(originalFileName);
        return getRootUploadPath() + ext + "/" + md5 + "." + ext;
    }

    /**
     * 文件上传根路径，不用重写
     */
    default String getRootUploadPath() {
        return PathUtil.getRootUploadPath();
    }

    /**
     * 根据文件名获取文件后缀名，不带.号，不用重写
     */
    default String getExt(String originalFileName) {
        int index = originalFileName.lastIndexOf(".");
        if (index < 0) {
            throw new CheckNoLogException("文件后缀不能为空");
        }
        return originalFileName.substring(index + 1);
    }

}
