package com.icesoft.core.web.suppose.token.service;

import com.icesoft.core.web.suppose.token.IAuthToken;

public interface IAuthTokenService {

    IAuthToken findByToken(String token);

    default boolean isTokenExpired(String token) {
        return false;
    }

    default boolean isAuthTokenEnable(IAuthToken authToken) {
        return true;
    }
}
