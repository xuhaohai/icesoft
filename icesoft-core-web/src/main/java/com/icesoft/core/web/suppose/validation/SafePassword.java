package com.icesoft.core.web.suppose.validation;

import com.icesoft.core.web.suppose.validation.impl.PasswordValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({FIELD, METHOD, PARAMETER, ANNOTATION_TYPE})
@Retention(RUNTIME)
//指定验证器  
@Constraint(validatedBy = PasswordValidator.class)
@Documented
public @interface SafePassword {

    //默认错误消息  
    String message() default "密码要8-32位字母、数字、特殊字符最少2种组合（不能有中文和空格）";

    //分组
    Class<?>[] groups() default {};

    //负载  
    Class<? extends Payload>[] payload() default {};

}
