package com.icesoft.core.web.suppose.safehttp.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface HeadSafeRequestFilter {
    boolean headFilter(HttpServletRequest request, HttpServletResponse response);
}
