package com.icesoft.core.web.configurer;

import com.icesoft.core.web.service.SmsService;
import com.icesoft.core.web.suppose.setting.service.ISysSettingService;
import com.icesoft.core.web.suppose.setting.service.SysSettingFileServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class BeanAutoConfiguration {


    @ConditionalOnMissingBean
    @Bean
    public SmsService smsService() {
        return (phone, content) -> {
            log.error("无短信实现");
            return false;
        };
    }

    @Bean
    @ConditionalOnMissingBean
    public ISysSettingService fileSysSettingService(){
        return new SysSettingFileServiceImpl();
    }
}
