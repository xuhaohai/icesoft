package com.icesoft.core.web.suppose.sms.config;

import com.icesoft.core.common.util.RegexUtils;
import com.icesoft.core.web.suppose.setting.BasePropertySetting;
import com.icesoft.core.web.suppose.setting.BaseSettingEnum;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * 默认实现，不用注入
 */
@Component
public class SmsCodePropertySetting extends BasePropertySetting<SmsCodePropertySetting.SettingEnum> {
    private static final String SMS_SETTING_GROUP_NAME = "验证码配置";

    @Override
    public String getGroupName() {
        return SMS_SETTING_GROUP_NAME;
    }

    public String getCodeMsg(String code) {
        Map<String, String> map = new HashMap<>();
        map.put("code", code);
        return RegexUtils.processTemplate(getTemplate(), map);
    }

    private String getTemplate() {
        String template = getProperty(SettingEnum.bc_sms_template);
        if (!template.contains("${code}")) {
            template = SettingEnum.bc_sms_template.defaultValue;
        }
        return template;
    }


    public int getSendIntervalSecond() {
        return getIntProperty(SettingEnum.bc_sms_send_interval_second);
    }

    public enum SettingEnum implements BaseSettingEnum {
        bc_sms_template("验证码模板，${code}：表示验证码", "您的验证码：${code}，注意验证码不要发送给其他人，有效时间为5分钟"),
        bc_sms_send_interval_second("验证码发送时间间隔，单位：秒", "60"),
        ;

        private String des;
        private String defaultValue;

        SettingEnum(String des, String defaultValue) {
            this.des = des;
            this.defaultValue = defaultValue;
        }

        @Override
        public String getDes() {
            return this.des;
        }

        @Override
        public String getDefaultValue() {
            return this.defaultValue;
        }
    }

}
