package com.icesoft.core.web.configurer.test.condition;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

import com.icesoft.core.web.configurer.init.SimpleConfigEnvironmentPostProcessor;

class TestCaseCondition implements Condition {

	@Override
	public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
		if (metadata.getAnnotationAttributes(ConditionOnTestCase.class.getName()) != null) {
			return SimpleConfigEnvironmentPostProcessor.isTestCase;
		}
		return !SimpleConfigEnvironmentPostProcessor.isTestCase;
	}

}
