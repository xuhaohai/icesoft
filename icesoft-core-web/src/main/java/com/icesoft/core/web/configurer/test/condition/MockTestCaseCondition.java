package com.icesoft.core.web.configurer.test.condition;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

import com.icesoft.core.web.configurer.init.SimpleConfigEnvironmentPostProcessor;

class MockTestCaseCondition implements Condition {

	@Override
	public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
		boolean isMockTestCase = isMockTestCase();
		if (metadata.getAnnotationAttributes(ConditionOnMockTestCase.class.getName()) != null) {
			System.err.println(isMockTestCase);
			return isMockTestCase;
		}
		
		return !isMockTestCase;
	}

	private boolean isMockTestCase() {
		return SimpleConfigEnvironmentPostProcessor.isMockTestCase;
	}

}
