package com.icesoft.core.web.suppose.setting.service;

import com.icesoft.core.web.model.SysSettingDTO;

import java.util.Collection;
import java.util.Map;

public interface ISysSettingService {

	Map<String, String> findByGroup(String groupName);

	void createSettings(Collection<SysSettingDTO> sysSetting);

	void remove(String groupName, String keyName);

}
