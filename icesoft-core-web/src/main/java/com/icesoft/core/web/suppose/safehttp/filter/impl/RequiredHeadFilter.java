package com.icesoft.core.web.suppose.safehttp.filter.impl;

import com.icesoft.core.common.helper.Resp;
import com.icesoft.core.web.helper.ResponseUtils;
import com.icesoft.core.web.suppose.safehttp.filter.HeadSafeRequestFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
@Slf4j
public class RequiredHeadFilter implements HeadSafeRequestFilter {

    @Override
    public boolean headFilter(HttpServletRequest request, HttpServletResponse response) {
        String method = request.getMethod();
        log.trace("http method:{}", method);
        if (request.getHeader("versionCode") == null) {
            ResponseUtils.writeJson(response, Resp.error("缺少请求头：versionCode"));
            return false;
        }
        if (request.getHeader("clientType") == null) {
            ResponseUtils.writeJson(response, Resp.error("缺少请求头：clientType"));
            return false;
        }
        return true;
    }

}
