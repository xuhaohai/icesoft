package com.icesoft.core.web.suppose.safehttp;

public abstract class SafeRequestConst {

    public static final String HEAD_SIGN_PARAM_NAME = "sign";
    public static final String HEAD_KEY_ID_PARAM_NAME = "keyId";
    public static final String ALGORITHM_SM = "SM";
    public static final String ALGORITHM_COMMON = "COMMON";

    public static boolean isEncrypt(SafeRequest safeRequest) {
        return safeRequest != null && !safeRequest.disable();
    }
}
