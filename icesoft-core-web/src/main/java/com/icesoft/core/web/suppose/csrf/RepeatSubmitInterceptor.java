package com.icesoft.core.web.suppose.csrf;

import com.icesoft.core.web.base.BaseAnnotationInterceptor;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
@Slf4j
@AllArgsConstructor
public class RepeatSubmitInterceptor extends BaseAnnotationInterceptor<RepeatSubmit> {
    private TimeUnitNonceStrFilter timeUnitNonceStrFilter;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, HandlerMethod handlerMethod,
                             RepeatSubmit annotation) {
        return timeUnitNonceStrFilter.filter(request, response);
    }

}
