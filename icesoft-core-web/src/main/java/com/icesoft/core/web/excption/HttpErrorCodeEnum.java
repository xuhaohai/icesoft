package com.icesoft.core.web.excption;

import com.icesoft.core.common.helper.Resp;
import com.icesoft.core.common.helper.RespCode;
import lombok.Getter;

public enum HttpErrorCodeEnum {
    SUCCESS("请求成功", RespCode.SUCCESS),
    FAIL("请求失败", RespCode.FAIL),
    NO_PERMISSION("没有访问权限", RespCode.NO_PERMISSION),
    REDIRECT("请求重定向", RespCode.REDIRECT),

    KEY_ERROR("密钥错误", 1000),
    SESSION_EXPIRED("session过期", 1001),
    TOKEN_EXPIRED("token过期", 1002),
    SYSTEM_INNER_ERROR("系统内部错误", 1003),
    ACCESS_DENY("访问拒绝", 1004),
    ;
    @Getter
    private String msg;
    @Getter
    private int code;

    HttpErrorCodeEnum(String msg, int code) {
        this.msg = msg;
        this.code = code;
    }

    public Resp toResp() {
        return Resp.error(this.code, this.msg);
    }
}
