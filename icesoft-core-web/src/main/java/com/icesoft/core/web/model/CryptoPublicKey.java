package com.icesoft.core.web.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class CryptoPublicKey implements Serializable {
    protected String keyId;
    protected String algorithm;
    protected String publicKey;
}
