package com.icesoft.core.web.suppose.validation.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.util.StringUtils;

import com.icesoft.core.web.suppose.validation.ChineseOrEnglishOrNumber;

public class ChineseOrEnglishOrNumberValidator implements ConstraintValidator<ChineseOrEnglishOrNumber, String> {
    static String chineseOrEnglishOrNumberRegexp = "^[\\u4E00-\\u9FA5·\\w（）\\(\\)【】\\[\\]\\s]+$";

    @Override
    public void initialize(ChineseOrEnglishOrNumber constraintAnnotation) {
        //初始化，得到注解数据
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (StringUtils.isEmpty(value)) {
            return true;
        }
        return value.matches(chineseOrEnglishOrNumberRegexp);
    }
}  
