package com.icesoft.core.web.suppose.mapping;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@ConditionMapping
public @interface HeaderMapping {

	/**
	 * 请求头参数名
	 */
	String name();

	/**
	 * 请求头参数值
	 */
	String value();
}
