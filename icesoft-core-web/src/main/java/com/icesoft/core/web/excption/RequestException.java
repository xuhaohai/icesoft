package com.icesoft.core.web.excption;

import lombok.Getter;

/**
 * @author XHH
 */
public class RequestException extends RuntimeException {

    @Getter
    private int code;
    @Getter
    private boolean logMsg;

    public RequestException(String message) {
        this(message, HttpErrorCodeEnum.FAIL);
    }

    public RequestException(String message, int code) {
        this(message, code, false);

    }

    public RequestException(String message, HttpErrorCodeEnum httpErrorCodeEnum) {
        this(message, httpErrorCodeEnum, false);
    }

    public RequestException(String message, int code, boolean logMsg) {
        super(message);
        this.code = code;
        this.logMsg = logMsg;
    }

    public RequestException(String message, HttpErrorCodeEnum httpErrorCodeEnum, boolean logMsg) {
        super(httpErrorCodeEnum.getMsg() + ": " + message);
        this.code = httpErrorCodeEnum.getCode();
        this.logMsg = logMsg;
    }


}
