package com.icesoft.core.web.suppose.validation.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.util.StringUtils;

import com.icesoft.core.common.util.RegexUtils;
import com.icesoft.core.web.suppose.validation.IdCard;

public class IdCardValidator implements ConstraintValidator<IdCard, String> {

	@Override
	public void initialize(IdCard constraintAnnotation) {
		// 初始化，得到注解数据
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (StringUtils.isEmpty(value)) {
			return true;
		}
		if (value.endsWith("x")) {
			return false;
		}
		return RegexUtils.isIdCard(value);
	}
}
