package com.icesoft.core.web.base;

import com.icesoft.core.common.util.ReflectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.annotation.Annotation;

public abstract class BaseAnnotationInterceptor<T extends Annotation> {
	protected final Logger log = LoggerFactory.getLogger(this.getClass());
	private Class<T> annotationClass;

	@SuppressWarnings("unchecked")
	@PostConstruct
	protected void PostConstruct() {
		annotationClass = (Class<T>) ReflectionUtils.getGenricType(this.getClass(), 0);
		if (log.isInfoEnabled()) {
			log.info("注解{}，过滤器{}", annotationClass.getName(), this.getClass().getName());
		}
	}

	public Class<T> getAnnotationClass() {
		return annotationClass;
	}

	public abstract boolean preHandle(HttpServletRequest request, HttpServletResponse response,
			HandlerMethod handlerMethod, T annotation);

	public void postHandle(HttpServletRequest request, HttpServletResponse response, HandlerMethod handler,
			ModelAndView modelAndView, T annotation) {
	}

	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, HandlerMethod handler, Exception ex,
			T annotation) {
	}

}
