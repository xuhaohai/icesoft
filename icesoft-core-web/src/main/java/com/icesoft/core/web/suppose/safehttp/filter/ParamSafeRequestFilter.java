package com.icesoft.core.web.suppose.safehttp.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.TreeMap;

public interface ParamSafeRequestFilter  {
    boolean paramFilter(HttpServletRequest request, HttpServletResponse response, TreeMap<String, String> paramMap);

}
