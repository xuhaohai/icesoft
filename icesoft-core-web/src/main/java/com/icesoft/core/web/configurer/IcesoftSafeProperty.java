package com.icesoft.core.web.configurer;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("icesoft.safe")
@Data
public class IcesoftSafeProperty {
    /**
     * 管理端点访问白名单IP
     */
    private String whiteIp = "";
    /**
     * host头过滤地址
     */
    private String hostPath;
    /**
     * 是否使用国密进行接口加解密
     */
    private boolean smCryptoEnable = false;
}
