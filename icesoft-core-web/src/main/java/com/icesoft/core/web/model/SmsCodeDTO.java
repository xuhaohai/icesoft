package com.icesoft.core.web.model;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class SmsCodeDTO implements Serializable {
	private String code;
	private String phone;
	private long createTime;

}
