package com.icesoft.core.web.suppose.upload.base;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

@Data
public class ChunkFileDTO {

    /**
     * 文件md5
     */
    @NotEmpty(message = "md5不能为空")
    @Length(min = 32, max = 32, message = "md5长度错误")
    String md5;
    /**
     * 文件名
     */
    @NotEmpty(message = "文件名不能为空")
    String fileName;
    /**
     * 分片文件序号，从1开始
     */
    @Min(value = 1, message = "分片文件序号，从1开始")
    int chunk;
    /**
     * 文件总片数
     */
    @Min(value = 1, message = "分片文件序号，从1开始")
    int chunks;
    /**
     * 每一片大小
     */
    @Max(value = 5 * 1024 * 1024, message = "分片文件最大5M")
    int chunkSize;


    public boolean isEndChunk() {
        return chunk == chunks;
    }
}
