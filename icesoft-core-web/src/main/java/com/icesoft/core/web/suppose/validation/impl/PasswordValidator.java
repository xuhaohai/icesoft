package com.icesoft.core.web.suppose.validation.impl;

import com.icesoft.core.common.util.RegexUtils;
import com.icesoft.core.web.suppose.validation.SafePassword;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordValidator implements ConstraintValidator<SafePassword, String> {

    @Override
    public void initialize(SafePassword constraintAnnotation) {
        //初始化，得到注解数据
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return RegexUtils.isSafePassword(value);
    }
}  
