package com.icesoft.test.qdoc;

import com.icesoft.base.manager.config.UrlConstant;
import com.icesoft.core.common.util.JsonUtil;
import com.power.doc.builder.ApiDataBuilder;
import com.power.doc.model.*;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.StringUtils;

import java.util.LinkedHashMap;
import java.util.Map;

class ApiDocCreateTest {
    @Test
    void testCreate() {
        ApiConfig config = new ApiConfig();
        config.setSourceCodePaths(
                SourceCodePath.builder().setDesc("static/coder/admin").setPath("G:\\project\\ideaWork\\icesoft\\icesoft-base-admin\\src\\main\\java"),
                SourceCodePath.builder().setDesc("manager").setPath("G:\\project\\ideaWork\\icesoft\\icesoft-base-manager\\src\\main\\java"),
                SourceCodePath.builder().setDesc("web").setPath("G:\\project\\ideaWork\\icesoft\\icesoft-core-web\\src\\main\\java"));
        ApiAllData apiAllData = ApiDataBuilder.getApiData(config);
        Map<String, String> map = new LinkedHashMap<>();
        for (ApiDoc apiDoc : apiAllData.getApiDocList()) {
            for (ApiMethodDoc apiMethodDoc : apiDoc.getList()) {
                System.out.println(String.format("%s:%s", apiMethodDoc.getDesc(), apiMethodDoc.getUrl()));
                if (StringUtils.isBlank(apiMethodDoc.getDesc())) {
                    continue;
                }
                String url = apiMethodDoc.getUrl().replace("UrlConstant.LOAD", UrlConstant.LOAD);
                url = url.replace("UrlConstant.QUERY", UrlConstant.QUERY);
                url = url.replace("UrlConstant.CREATE", UrlConstant.CREATE);
                url = url.replace("UrlConstant.UPDATE", UrlConstant.UPDATE);
                url = url.replace("UrlConstant.DELETE", UrlConstant.DELETE);
                url = url.replace("UrlConstant.EXPORT", UrlConstant.EXPORT);
                url = url.replace("UrlConstant.IMP", UrlConstant.IMP);
                map.put(url, apiMethodDoc.getDesc());
            }
        }
        System.out.println(JsonUtil.toJson(map));
    }
}
