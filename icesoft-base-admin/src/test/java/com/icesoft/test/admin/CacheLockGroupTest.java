package com.icesoft.test.admin;

import com.icesoft.core.common.helper.lock.base.ICacheLockGroup;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Repeat;

import java.util.List;
import java.util.Vector;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

@EnableAutoConfiguration
@SpringBootTest
public class CacheLockGroupTest {
	@Autowired
	private ICacheLockGroup cacheLockGroup;

	@Test
	@Repeat(3)
	public void testLoadOrCreateIfNullThread() {
		int count = 100;
		CountDownLatch latch = new CountDownLatch(count);
		List<Integer> tokens = new Vector<>();
		AtomicInteger atomicInteger = new AtomicInteger();
		CyclicBarrier barrier = new CyclicBarrier(count);
		Runnable runnable = () -> {
			try {
				barrier.await();
			} catch (InterruptedException | BrokenBarrierException e1) {
				e1.printStackTrace();
			}
			Supplier<Integer> loadCallable = () -> {
				if (atomicInteger.get() == 0) {
					return null;
				}
				return (Integer) atomicInteger.get();
			};
			Supplier<Integer> createCallable = () -> {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				return atomicInteger.incrementAndGet();
			};
			Integer tep = cacheLockGroup.getGroup("fasdf").loadOrCreateIfNull("aaa", loadCallable, createCallable);
			tokens.add(tep);
			latch.countDown();
		};
		for (int i = 0; i < count; i++) {
			Thread thread = new Thread(runnable);
			thread.start();
		}
		try {
			latch.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		for (Integer token : tokens) {
			Assertions.assertEquals((Integer) 1, token);
		}

	}

	@Test
	public void testCacheLockGroupThread() {
		int count = 100;
		CountDownLatch latch = new CountDownLatch(count);
		List<Integer> tokens = new Vector<>();
		AtomicInteger atomicInteger = new AtomicInteger();
		CyclicBarrier barrier = new CyclicBarrier(count);
		Runnable runnable = () -> {
			try {
				barrier.await();
			} catch (InterruptedException | BrokenBarrierException e1) {
				e1.printStackTrace();
			}
			cacheLockGroup.getGroup("fasdf").runIfLocked("adfs", () -> {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				atomicInteger.incrementAndGet();
			}, true);
			tokens.add(atomicInteger.get());
			latch.countDown();
		};
		for (int i = 0; i < count; i++) {
			Thread thread = new Thread(runnable);
			thread.start();
		}
		try {
			latch.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		for (Integer token : tokens) {
			Assertions.assertEquals((Integer) 1, token);
		}

	}
}
