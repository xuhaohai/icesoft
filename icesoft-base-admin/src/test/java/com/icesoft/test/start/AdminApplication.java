package com.icesoft.test.start;

import com.icesoft.Application;

/**
 * @author XHH
 */
public class AdminApplication {

	public static void main(String[] args) {
		System.setProperty("spring.profiles.active","dev");
		Application.main(args);
	}
}