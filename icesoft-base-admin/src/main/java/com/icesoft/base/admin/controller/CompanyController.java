package com.icesoft.base.admin.controller;

import com.icesoft.base.manager.config.UrlConstant;
import com.icesoft.base.manager.helper.PageParam;
import com.icesoft.base.manager.helper.PageResp;
import com.icesoft.base.manager.helper.QueryBuilderUtils;
import com.icesoft.core.dao.criteria.Page;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.icesoft.core.dao.criteria.QueryBuilder;
import com.icesoft.core.common.helper.Resp;
import com.icesoft.base.admin.entity.Company;
import com.icesoft.base.admin.service.CompanyService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * 公司管理接口
 */
@RequestMapping("security/company")
@RestController
@AllArgsConstructor
public class CompanyController {

	private CompanyService companyService;
	/**
	 * 通过id查询
	 */
	@GetMapping(value = UrlConstant.LOAD)
	public Resp load(int id) {
		return Resp.success(companyService.load(id));
	}
	/**
	 * 修改公司
	 */
	@PostMapping(value = UrlConstant.UPDATE)
	public Resp update(Company queryParam) {
		Company company = companyService.load(queryParam.getId());
		if (company != null && !company.getId().equals(queryParam.getId())) {
			return Resp.error("公司已存在");
		}
		companyService.updateNotNullProperties(queryParam);
		return Resp.success();
	}
	/**
	 * 公司列表查询
	 */
	@GetMapping(UrlConstant.QUERY)
	public PageResp query(HttpServletRequest request, Company queryParam, String search, PageParam pageParam) {
		QueryBuilder queryBuilder = QueryBuilder.get();
		QueryBuilderUtils.addRequestParam(queryBuilder, request, queryParam);
		QueryBuilderUtils.addSearchParam(queryBuilder, search, new String[]{"name"});
		Page<Company> page = companyService.page(queryBuilder, pageParam);
		return new PageResp<>(page);
	}
	/**
	 * 创建公司
	 */
	@PostMapping(value = UrlConstant.CREATE)
	public Resp<Integer> create(@Valid Company company) {
		companyService.create(company);
		return Resp.success(company.getId());
	}
	/**
	 * 删除公司
	 */
	@PostMapping(value = UrlConstant.DELETE)
	public Resp<?> delete(int[] ids) {
		companyService.delete(ids);
		return Resp.success();
	}

}
