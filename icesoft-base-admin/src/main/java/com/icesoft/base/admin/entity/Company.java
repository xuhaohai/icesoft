package com.icesoft.base.admin.entity;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.icesoft.core.dao.base.BaseModel;
import com.icesoft.core.web.suppose.validation.ChineseOrEnglishOrNumber;
import com.icesoft.core.dao.suppose.validation.UniqueCheck;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Table(name = "sys_company")
@Entity
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Cacheable
public final class Company extends BaseModel {
    @Column(unique = true, length = 50, nullable = false)
    @NotNull(message = "公司名不能为空")
    @Size(min = 1, max = 50, message = "公司名长度不符合要求")
    @UniqueCheck(message = "公司名已存在")
    @ChineseOrEnglishOrNumber
    private String name;

	public Company() {
	}

	public Company(@NotNull(message = "公司名不能为空") @Size(min = 1, max = 50, message = "公司名长度不符合要求") String name) {
		this.name = name;
	}
}
