package com.icesoft.base.admin.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.icesoft.base.manager.base.BaseUser;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.util.Date;

@Table(name = "SYS_USER", indexes = {@Index(name = "idx_SysUser_companyId", columnList = "companyId")})
@Entity
@Cacheable
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class SysUser extends BaseUser {

    private boolean locked;
    @JsonIgnore
    private Date unlockTime;
    private boolean disable;
    @JsonIgnore
    private Date expiredTime;
    @JsonIgnore
    private Date passwordUpdateTime;

    @PreUpdate
    @PrePersist
    public void phoneCheck() {
        if (!StringUtils.hasText(phone)) {
            phone = null;
        }
    }

    public boolean isAccountNonExpired() {
        return expiredTime == null || expiredTime.after(new Date());
    }

    public boolean isAccountNonLocked() {
        return BooleanUtils.isNotTrue(locked);
    }

    public boolean isEnabled() {
        return BooleanUtils.isNotTrue(disable);
    }
}
