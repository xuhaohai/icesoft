package com.icesoft.base.admin.listener;

import com.icesoft.base.admin.entity.Company;
import com.icesoft.base.admin.service.SysUserService;
import com.icesoft.base.manager.service.system.SysOrgService;
import com.icesoft.core.common.exception.CheckException;
import com.icesoft.core.dao.suppose.BaseModelListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CompanyListener extends BaseModelListener<Company> {

    @Autowired(required = false)
    private SysOrgService orgService;
    @Autowired
    private SysUserService sysUserService;

    @Override
    public void preRemove(Company company) {
        if (orgService.checkCompanyCanDelete(company.getId())) {
            throw new CheckException("公司下存在部门");
        }
        if (sysUserService.checkCompanyCanDelete(company.getId())) {
            throw new CheckException("公司下存在系统用户");
        }
        super.preRemove(company);
    }
}
