package com.icesoft.base.admin.entity.dto;

import com.icesoft.base.admin.entity.SysUser;
import lombok.Data;

import java.util.Date;

@Data
public class SysUserDTO {
    private int id;
    private Date createTime;
    private Date updateTime;
    private String headImgUrl;
    private String email;
    private int orgId;
    private String username; // 用户名
    private String phone;
    private String name; // 姓名
    private int companyId;
    private String sex;
    private boolean locked;
    private boolean disable;
    private int onlineSize;

    private String orgName;
    private String roleNames;
    private String roleIds;

    public SysUserDTO() {
    }

    public SysUserDTO(SysUser sysUser) {
        this.id = sysUser.getId();
        this.createTime = sysUser.getCreateTime();
        this.updateTime = sysUser.getUpdateTime();
        this.headImgUrl = sysUser.getHeadImgUrl();
        this.email = sysUser.getEmail();
        this.orgId = sysUser.getOrgId();
        this.username = sysUser.getUsername();
        this.phone = sysUser.getPhone();
        this.name = sysUser.getName();
        this.companyId = sysUser.getCompanyId();
        this.sex = sysUser.getSex();
        this.locked = sysUser.isLocked();
        this.disable = sysUser.isDisable();
    }
}
