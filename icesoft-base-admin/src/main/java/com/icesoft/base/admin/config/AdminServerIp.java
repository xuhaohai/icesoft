package com.icesoft.base.admin.config;

import com.icesoft.base.manager.security.suppose.IWhiteIpService;
import com.icesoft.core.web.configurer.filter.AdminServerIpHelper;
import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
@ConditionalOnProperty(name = AdminServerIp.ADMIN_SERVER_URL_PROPERTY)
@ConditionalOnClass(name = "de.codecentric.boot.admin.client.config.ClientProperties")
@AllArgsConstructor
public class AdminServerIp implements IWhiteIpService {
	static final String ADMIN_SERVER_URL_PROPERTY = "spring.boot.admin.client.url";
	private final AdminServerIpHelper adminServerIpHelper;

	@Override
	public Set<String> getWhiteIps() {
		return adminServerIpHelper.getAdminServerIps();
	}

}
