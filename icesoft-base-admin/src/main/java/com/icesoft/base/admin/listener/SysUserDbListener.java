package com.icesoft.base.admin.listener;

import com.icesoft.base.admin.entity.SysUser;
import com.icesoft.base.manager.service.security.RelSysUserRoleService;
import com.icesoft.core.dao.suppose.BaseModelListener;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class SysUserDbListener extends BaseModelListener<SysUser> {
    private RelSysUserRoleService relSysUserRoleService;

    @Override
    public void preRemove(SysUser model) {
        relSysUserRoleService.deleteByUserId(model.getId());
    }
}
