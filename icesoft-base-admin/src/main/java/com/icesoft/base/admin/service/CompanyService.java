package com.icesoft.base.admin.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import com.icesoft.base.manager.base.BaseService;
import com.icesoft.base.admin.entity.Company;

@Service
@AllArgsConstructor
public class CompanyService extends BaseService<Company> {
    @Override
    public void delete(int id) {
        super.delete(id);
    }
}
