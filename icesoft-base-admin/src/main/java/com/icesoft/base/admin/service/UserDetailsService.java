package com.icesoft.base.admin.service;

import com.icesoft.base.admin.entity.SysUser;
import com.icesoft.base.manager.entity.security.SysRole;
import com.icesoft.base.manager.entity.system.SysOrg;
import com.icesoft.base.manager.helper.AuthUser;
import com.icesoft.base.manager.helper.UserPermission;
import com.icesoft.base.manager.security.config.ApiMatchAuthority;
import com.icesoft.base.manager.security.suppose.IUserDetailsService;
import com.icesoft.base.manager.service.security.SysApiAuthorityService;
import com.icesoft.base.manager.service.security.SysRoleService;
import com.icesoft.base.manager.service.system.SysOrgService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
@AllArgsConstructor
class UserDetailsService implements IUserDetailsService {
    private final SysUserService sysUserService;
    private final SysRoleService sysRoleService;
    private final SysApiAuthorityService sysApiAuthorityService;
    private final SysOrgService sysOrgService;

    @Override
    public UserDetails loadUserByPhone(String phone) throws UsernameNotFoundException {
        return toUserDetails(sysUserService.findByPhone(phone));
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return toUserDetails(sysUserService.findByUsername(s));
    }

    private UserDetails toUserDetails(SysUser sysUser) {
        if (sysUser == null) {
            throw new UsernameNotFoundException("用户不存在");
        }
        Collection<ApiMatchAuthority> authorities = new HashSet<>();
        Set<Integer> orgIdList;
        List<SysRole> sysRoleList = sysRoleService.findByUserId(sysUser.getId());
        AuthUser user;
        if (sysUser.getId() == 1) {
            authorities.add(new ApiMatchAuthority("superAdmin", Collections.singletonList("/**")));
            orgIdList = sysOrgService.findAll().stream().map(SysOrg::getId).collect(Collectors.toSet());
            user = new AuthUser(sysUser.getId(), sysUser.getName(), sysUser.getOrgId(), sysUser.getCompanyId(),
                    sysUser.getUsername(), sysUser.getPassword(),
                    true, true, true, true,
                    authorities);
        } else {
            authorities.addAll(sysApiAuthorityService.findUrlsByUserId(sysUser.getId()));
            orgIdList = sysOrgService.findOrgIdsByUserId(sysUser.getId(), sysUser.getOrgId());
            user = new AuthUser(sysUser.getId(), sysUser.getName(), sysUser.getOrgId(), sysUser.getCompanyId(),
                    sysUser.getUsername(), sysUser.getPassword(),
                    sysUser.isEnabled(), sysUser.isAccountNonExpired(), sysUser.isAccountNonExpired(), sysUser.isAccountNonLocked(),
                    authorities);
        }
        user.setPermission(new UserPermission(sysRoleList, orgIdList));
        user.setHeadImgUrl(sysUser.getHeadImgUrl());
        return user;
    }

}
