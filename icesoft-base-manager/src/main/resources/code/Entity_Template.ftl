package ${modelClass.rootPackageName}.entity.${modelClass.moduleName};

import com.icesoft.core.dao.base.BaseModel;
import javax.persistence.*;
[#if extra.hasDate]import java.util.Date;[/#if]
import lombok.*;


/**
* ${modelClass.label}
* @author generator
* @description auto generate code
*/
@Table(name = "tb_${modelClass.simpleClassName}")
@Entity
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class ${modelClass.simpleClassName} extends BaseModel{
[#list fields as field ]
    /**
    *${field.label}
    */
    [#if field.className=='String']@Column(length = 100)[/#if]
    private ${field.className} ${field.name};
[/#list]
}
