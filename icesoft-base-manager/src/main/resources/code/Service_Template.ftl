package ${modelClass.rootPackageName}.service.${modelClass.moduleName};

import ${modelClass.rootPackageName}.entity.${modelClass.moduleName}.${modelClass.simpleClassName};
import com.icesoft.base.manager.base.BaseService;
import org.springframework.stereotype.Service;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class ${modelClass.simpleClassName}Service extends BaseService<${modelClass.simpleClassName}>{

}

