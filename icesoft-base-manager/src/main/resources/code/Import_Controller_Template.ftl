package ${modelClass.rootPackageName}.controller.${modelClass.moduleName};

import com.icesoft.base.manager.base.BaseExcelController;
import com.icesoft.base.manager.base.BaseImportAction;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

import ${modelClass.rootPackageName}.entity.${modelClass.moduleName}.${modelClass.simpleClassName};
import ${modelClass.rootPackageName}.service.${modelClass.moduleName}.${modelClass.simpleClassName}Service;

@Controller
@RequestMapping("${modelClass.valName}/import")
public class ${modelClass.simpleClassName}ImportController extends BaseExcelController<${modelClass.simpleClassName}, ${modelClass.simpleClassName}Service> {

    @Override
    public BaseImportAction<${modelClass.simpleClassName}> getImportAction(HttpServletRequest request) {
        return new ${modelClass.simpleClassName}ImportAction(request);
    }
    
    /**
     * 模板文件必须存在，以.xls结尾，模板文件路径：文件上传目录/excel/
     */
    @Override
    public String getTemplateFileName() {
        return "${modelClass.label}导入(模板文件)";
    }
}
