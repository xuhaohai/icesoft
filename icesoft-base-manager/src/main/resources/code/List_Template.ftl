<!DOCTYPE html>
<html  lang="zh-cn">
<head>
    <base href="../../"/>
    <meta charset="utf-8">
    <script src="js/commonHead.js"></script>
</head>
<body>
[#if extra.hasTree]
<div class="page-aside" >
    <div class="page-header">
        <h1 class="page-title">组织机构</h1>
        <div class="form-group">
            <input id="keyword" type="text" class="form-control" name="" placeholder="请输入关键词...">
        </div>
    </div>
    <div class="page-content">
        <ul id="org_click_tree" class="ztree"></ul>
    </div>
</div>
[/#if]
<div class="page-main">
    <div class="collapse" id="collapseFilter" aria-expanded="true">
        <div class="panel">
            <div class="panel-body">
                <form class="form-inline" id="searchForm">
                    <form-search>
                    </form-search>
                </form>
            </div>
        </div>
    </div>
    <div class="page-content">
        <h1 class="page-title">列表</h1>
        <div class="panel">
            <div class="panel-heading" id="tableToolbar">
                <tool-bar edit-page="${modelClass.valName}-edit.html"></tool-bar>
                [#if modelClass.impEnable]
                <button id="impQx" type="button" class="btn btn-dark"  onclick="dialog(preUrl + 'toUpload')">
                    <i class="icon wb-upload" aria-hidden="true"></i> excel导入
                </button>
                [/#if]
                [#if modelClass.expEnable]
                <button id="expQx" type="button" class="btn btn-info" onclick="table.explore()">
                    <i class="icon wb-download" aria-hidden="true"></i> 导出excel
                </button>
                [/#if]
                <button type="button" class="btn btn-icon btn-info collapsed" data-toggle="collapse" data-target="#collapseFilter" aria-expanded="false" aria-controls="collapseFilter">
                    <i class="icon fa-filter"></i>条件筛选
                </button>
            </div>
            <div class="panel-body">
                <table class="table table-bordered table-hover table-striped width-full text-nowrap" id="table">
                    <thead>
                    <th data-field="state" data-checkbox="true"></th>
                    <th data-formatter="<row-index/>">序号</th>
                    [#list fields as field]
                        [#if field.showInTable]
                    <th data-field="${field.name}">${field.label}</th>
                        [/#if]
                    [/#list]
                    <th data-formatter="<row-tool/>">操作</th>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
<script src="lib/require/require.js"></script>
<script src="js/require-config.js"></script>
<script type="text/javascript">
    const preUrl = "${modelClass.moduleName}/${modelClass.valName}/";
    require(['TableCurdTemplate', 'SearchFormTemplate'], function (TableCurdTemplate, SearchFormTemplate) {
        let template = new TableCurdTemplate({
            preUrl: preUrl,
            tableId: "table"
        });
        template.initTable();
        let searchFormTemplate = new SearchFormTemplate();
        searchFormTemplate.initForm({
            formId: "searchForm",
            search: function (fields) {
                let params = [];
                for (let field of fields) {
                    params.push(field.name + "=" + field.value);
                }
                template.table.refresh(params.join("&"));
            }
        });
        [#if extra.hasOrgTree]
        require(["ajaxUtils","fuzzySearch"],function (ajaxUtils) {
            const setting = {
                check : {
                    enable : false,
                },
                data : {
                    simpleData : {
                        enable : true
                    }
                },
                callback : {
                    onClick : function(e, treeId, treeNode) {
                        template.table.refresh("orgId=" + treeNode.id);
                    }
                }
            };
            ajaxUtils.get("system/org/tree").then(resp => {
                $.fn.zTree.init($("#org_click_tree"), setting, resp.data);
                fuzzySearch('org_click_tree', "#keyword", true, true);
            });
        });
        [/#if]
    });
</script>

</html>
