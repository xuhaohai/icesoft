package ${modelClass.rootPackageName}.controller.${modelClass.moduleName};

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.icesoft.base.manager.base.BaseExportAction;
import com.icesoft.base.manager.base.BaseExportController;
import ${modelClass.rootPackageName}.entity.${modelClass.moduleName}.${modelClass.simpleClassName};
import ${modelClass.rootPackageName}.service.${modelClass.moduleName}.${modelClass.simpleClassName}Service;

/** 
 * 类名称：${modelClass.simpleClassName}ExportController
 * @author generator
 * @description auto generate code
 */
@RestController
@RequestMapping(value="${modelClass.valName}/export")
public class ${modelClass.simpleClassName}ExportController extends BaseExportController<${modelClass.simpleClassName}> {
    @Autowired
    ${modelClass.simpleClassName}Service ${modelClass.valName}Service;
    
    @Override
    public BaseExportAction<${modelClass.simpleClassName}> getExportAction() {
        return new ${modelClass.simpleClassName}ExportAction();
    }

    @Override
    public List<${modelClass.simpleClassName}> findSelect(HttpServletRequest request) {
        return ${modelClass.valName}Service.findAll();
    }
}
