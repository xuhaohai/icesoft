package ${modelClass.rootPackageName}.controller.${modelClass.moduleName};

import com.icesoft.base.manager.base.BaseImportAction;
import com.icesoft.core.web.helper.RequestHold;
import ${modelClass.rootPackageName}.entity.${modelClass.moduleName}.${modelClass.simpleClassName};
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.propertyeditors.CustomDateEditor;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.HashMap;
import java.util.Map;

@NoArgsConstructor
@AllArgsConstructor
public class ${modelClass.simpleClassName}ImportAction extends BaseImportAction<${modelClass.simpleClassName}> {
    /** 用于接收从页面传过来的额外参数 */
    private HttpServletRequest request;

    @Override
    protected ${modelClass.simpleClassName} convert(Map<String, String> model) {
        Map<String, String> map = RequestHold.getRequestParamMap(request);
        ${modelClass.simpleClassName} rowModel = new ${modelClass.simpleClassName}();
        /**此处初始化对象**/
        //表头：【[#list fields as field]"[#if field.label!='']${field.label}[/#if]"[#sep],[/#sep][/#list]】
        Map<String,Object> beanMap = new HashMap<>();
            [#list fields as f]
            beanMap.put("${f.name}",[#if f.label!='']model.get("${f.label}")[/#if]);
            [/#list]
        BeanWrapper bw = new BeanWrapperImpl(${modelClass.simpleClassName}.class);
        //日期字符串转换
        DateFormat formatter = new SimpleDateFormat("YYYY-MM-DD HH:mm:ss");
        bw.registerCustomEditor(java.util.Date.class, new CustomDateEditor(formatter, true));
        bw.setPropertyValues(beanMap);
        ${modelClass.simpleClassName} wBean = (${modelClass.simpleClassName}) bw.getWrappedInstance();
        log.info("${modelClass.simpleClassName}Bean:{}", wBean);
        return wBean;
    }
}
