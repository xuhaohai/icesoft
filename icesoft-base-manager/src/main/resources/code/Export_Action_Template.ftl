package ${modelClass.rootPackageName}.controller.${modelClass.moduleName};

import com.icesoft.base.manager.base.BaseExportAction;
import ${modelClass.rootPackageName}.entity.${modelClass.moduleName}.${modelClass.simpleClassName};

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: ${modelClass.simpleClassName}ExportAction
 * @author generator
 * @description auto generate code
 */
public class ${modelClass.simpleClassName}ExportAction extends BaseExportAction<${modelClass.simpleClassName}> {

    @Override
    protected String[] getExcelHead() {
        return new String[]{[#list fields as field][#if field.label!=''] "${field.label}"[/#if][#sep],[/#sep][/#list]};
    }

    @Override
    protected String[] convert(${modelClass.simpleClassName} model) {
        List<String> data = new ArrayList<>();
        [#list fields as f]
            data.add(model.[#if f.className=="boolean"]is[#else]get[/#if]${f.name?cap_first}() + "");
        [/#list]
        return data.toArray(new String[data.size()]);
    }

}
