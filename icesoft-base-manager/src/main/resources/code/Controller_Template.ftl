package ${modelClass.rootPackageName}.controller.${modelClass.moduleName};

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
[#if extra.hasDate]
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import java.util.Date;
import java.text.SimpleDateFormat;
[/#if]
import com.icesoft.base.manager.config.UrlConstant;
import com.icesoft.base.manager.helper.PageParam;
import com.icesoft.base.manager.helper.PageResp;
import com.icesoft.base.manager.helper.QueryBuilderUtils;
import com.icesoft.core.common.helper.ObjectUtil;
import com.icesoft.core.common.helper.Resp;
import com.icesoft.core.dao.criteria.Page;
import com.icesoft.core.dao.criteria.QueryBuilder;

import ${modelClass.rootPackageName}.entity.${modelClass.moduleName}.${modelClass.simpleClassName};
import ${modelClass.rootPackageName}.service.${modelClass.moduleName}.${modelClass.simpleClassName}Service;
import lombok.AllArgsConstructor;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * ${modelClass.label}后台管理接口
 * 负责人：
 * 创建时间：${.now?string("yyyy-MM-dd")}
 */
@RestController
@RequestMapping(value="${modelClass.moduleName}/${modelClass.valName}")
@AllArgsConstructor
public class ${modelClass.simpleClassName}Controller {

	private final ${modelClass.simpleClassName}Service ${modelClass.valName}Service;
	[#if extra.hasDate]
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"), true));
	}
	[/#if]
	/**
	 * 根据id查询${modelClass.label}接口
	 */
	@GetMapping(value = UrlConstant.LOAD)
	public Resp<${modelClass.simpleClassName}> load(int id) {
		return Resp.success(${modelClass.valName}Service.load(id));
	}

	/**
	 * 修改${modelClass.label}接口
	 */
	@PostMapping(value = UrlConstant.UPDATE)
	public Resp<Integer> update(${modelClass.simpleClassName} queryParam) {
		${modelClass.valName}Service.updateNotNullProperties(queryParam);
		return Resp.success(queryParam.getId());
	}

	/**
	 * 查询${modelClass.label}列表接口
	 */
	@GetMapping(UrlConstant.QUERY)
	public PageResp<${modelClass.simpleClassName}> query(HttpServletRequest request, ${modelClass.simpleClassName} queryParam, String search, PageParam pageParam) {
		QueryBuilder queryBuilder = QueryBuilder.get();
		QueryBuilderUtils.addRequestParam(queryBuilder, request, queryParam);
		String[] searchFields = new String[]{"id"[#list fields as field][#if field.className=='String'], "${field.name}"[/#if][/#list]};
		QueryBuilderUtils.addSearchParam(queryBuilder, search, searchFields);
		Page<${modelClass.simpleClassName}> page = ${modelClass.valName}Service.page(queryBuilder, pageParam);
		return new PageResp<>(page);
	}

	/**
	 * 新建${modelClass.label}数据接口
	 */
	@PostMapping(value = UrlConstant.CREATE)
	public Resp<Integer> create(@Valid ${modelClass.simpleClassName} model) {
		${modelClass.valName}Service.create(model);
		return Resp.success(model.getId());
	}

	/**
	 * 根据id删除${modelClass.label}接口
	 */
	@PostMapping(value = UrlConstant.DELETE)
	public Resp<String> delete(int[] ids) {
		${modelClass.valName}Service.delete(ids);
		return Resp.success("删除成功");
	}
}
