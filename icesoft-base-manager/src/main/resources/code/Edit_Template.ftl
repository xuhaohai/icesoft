<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<base href="../../"/>
		<meta charset="utf-8">
		<script src="js/commonHead.js"></script>
	</head>
	<body>
		<div class="col-md-offset-2 col-md-8 tab-content padding-top-20">
			<form id="form" class="form-horizontal col-sm-10 col-md-10">
				<input type="hidden" name="id" value="0">
				[#list fields as field]
				<div class="form-group">
					[#assign required="${field.required?string('required', '')}"/]
					<label class="col-sm-3 control-label">[#if field.required]<span class="required">*</span>[/#if]${field.label}：</label>
					<div class="col-sm-9">
						[#if field.inputType == 'IMAGE']
						<input class="form-control" type="hidden" name="${field.name}" id="${field.name}" ${required}/>
						<figure class="icesoft-img overlay overlay-hover animation-hover height-100 width-100">
							<img class="caption-figure" id="${field.name}Img" src="images/no.jpg">
							<figcaption class="icesoft-img overlay-panel overlay-background overlay-fade text-center vertical-align">
								<button type="button" class="icesoft-img btn btn-outline btn-default project-button" onclick="${field.name}Edit()">选择图片</button>
							</figcaption>
						</figure>
						[#elseif field.inputType == 'FILE']
						<div class="input-group">
                            <span class="input-group-addon" onclick="${field.name}Edit()"> <i class="icon fa-upload" aria-hidden="true"></i></span>
							<input class="form-control"  readonly="readonly" type="text" name="${field.name}" id="${field.name}" ${required}/>
						</div>
						[#elseif field.inputType == 'NUMBER']
						<input type="number" class="form-control" name="${field.name}" id="${field.name}" ${required}/>
						[#elseif field.inputType == 'SELECT']
						<select class="form-control" data-plugin="selectpicker" data-live-search="true" name="${field.name}" id="${field.name}" ${required}>
                           </select>
						[#elseif field.inputType == 'DATE']
						<input type="text" class="form-control" name="${field.name}" id="${field.name}" readonly="readonly" data-plugin="laydate" data-type="${field.options!'date'}" ${required}/>
						[#elseif field.inputType == 'CHECKBOX' || field.inputType == 'RADIO']
						[#assign type="${(field.inputType=='CHECKBOX')?string('checkbox', 'radio')}"/]
						<ul class="list-unstyled list-inline input-group" data-name="${field.name}">
							[#if field.options?? && field.options?contains(',')]
							[#list field.options?split(",") as op]
							<li>
								<div class="${type}-custom ${type}-primary">
									<input type="${type}" name="${field.name}" id="${field.name}${op?index}" value="${op}" [#if op?index==0]${required}[/#if]/>
									<label for="${field.name}${op?index}">${op}</label>
								</div>
							</li>
							[/#list]
						    [/#if]
						</ul>
						[#elseif field.inputType == 'TEXTAREA']
						<textarea class="form-control" name="${field.name}" id="${field.name}" rows="3" ${required}></textarea>
						[#elseif field.inputType == 'TREE']
						<input type="hidden" name="${field.name}" id="${field.name}" ${required}/>
						<input type="text" placeholder="点击选择" class="form-control" name="${field.name}Name" id="${field.name}Name" readonly="readonly" onclick="${field.name}Tree()"/>
						[#elseif field.inputType == 'NONE']
						[#else]
						<input type="text" name="${field.name}" id="${field.name}" class="form-control" ${required}/>
						[/#if]
					</div>
				</div>
				[/#list]
				<div class="form-group margin-top-25 modal-footer col-md-12">
					<a class="btn btn-default margin-right-10" onclick="alertUtils.closeDialog()">取消</a>
					<button class="btn btn-primary" type="submit">保存</button>
				</div>
			</form>
		</div>
	</body>
	<script src="lib/require/require.js"></script>
	<script src="js/require-config.js"></script>
	<script type="text/javascript">
		const preUrl = "${modelClass.moduleName}/${modelClass.valName}/";
		const $form = $("#form");
		const params = getParam();
		require(['EditTemplate','ajaxUtils','laydate'], function (EditTemplate,ajaxUtils) {
			let template = new EditTemplate({
				preUrl: preUrl,
				formId: "form",
				initForm: function (resolve) {
					Promise.all([[#list remoteFields as field]${field.name}List()[#sep], [/#sep][/#list]]).then(()=> {
						resolve();
					});
				}
			});
			template.initEdit();

	[#list fields as field]
		[#if field.inputType == 'SELECT']
			function ${field.name}List() {
				return new Promise(function (resolve) {
					ajaxUtils.get("${field.options}").then(resp=>{
						let options = '';
						for(let item of resp.data){
							options += `<option value="${r"${item.value}"}">${r"${item.name}"}</option>`;
						}
						$form.find("select[name='${field.name}']").html(options);
						resolve();
					});
				});
			}
		[#elseif (field.inputType == 'CHECKBOX' || field.inputType == 'RADIO') && field.options?? && field.options?contains('/')]
		[#assign type="${(field.inputType=='CHECKBOX')?string('checkbox', 'radio')}" required="${field.required?string('required', '')}"/]
			function ${field.name}List() {
				return new Promise(function (resolve) {
					ajaxUtils.get("${field.options}").then(resp=>{
						let options = '';
						for(let item of resp.data){
							let required = [#if field.required]idx==0?'required':[/#if]'';
							options += `<li><div class="${type}-custom ${type}-primary">
							  <input type="${type}" name="${field.name}" id="${field.name}${field?counter}" value="${r"${item.value}"}" ${r"${required}"}/>
							  <label for="${field.name}${field?counter}">${r"${item.name}"}</label></div>
						 	</li>`;
						}
						$form.find("ul[data-name='${field.name}']").html(options)
						resolve();
					});
				});
			}
		[/#if]
	[/#list]
		});
		[#list fields as field]
		[#if field.inputType == 'IMAGE']
		function ${field.name}Edit() {
			alertUtils.toUploadImg(function (path) {
				$("#${field.name}Img").attr("src",path);
				$("#${field.name}").val(path);
				$("#${field.name}").trigger("input");
			});
		}
		[#elseif field.inputType == 'FILE']
		function ${field.name}Edit() {
			alertUtils.toUpload(function (path) {
				$("#${field.name}").val(path);
				$("#${field.name}").trigger("input");
			});
		}
		[#elseif field.inputType == 'TREE']
		function ${field.name}Tree() {
			const url = "${field.options!'system/org/tree'}";
			alertUtils.openTree(url, function (id, name) {
				$("#${field.name}").val(id);
				$("#${field.name}Name").val(name);
				$("#${field.name}").trigger("input");
			});
		}
		[/#if]
		[/#list]
	</script>
</html>
