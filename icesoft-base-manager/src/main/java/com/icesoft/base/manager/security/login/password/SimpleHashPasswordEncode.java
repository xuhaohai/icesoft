package com.icesoft.base.manager.security.login.password;

import org.springframework.security.crypto.password.PasswordEncoder;

@Deprecated
public class SimpleHashPasswordEncode implements PasswordEncoder {

    @Override
    public String encode(CharSequence charSequence) {
        String text = "this is password salt,(QQ982604405)";
        return new SimpleHash("SHA-256", charSequence.toString(), text).toString(); // 密码加密;
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return encode(rawPassword).equals(encodedPassword);
    }
}
