package com.icesoft.base.manager.helper;

import com.icesoft.core.common.helper.Resp;
import com.icesoft.core.dao.criteria.Page;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class PageResp<T> extends Resp<List<T>> {
    private int total = 0;

    public PageResp(Page<T> page) {
        this.setData(page.getData());
        this.setTotal(page.getTotal());
    }

    public PageResp(List<T> data, int total) {
        this.setData(data);
        this.setTotal(total);
    }

}
