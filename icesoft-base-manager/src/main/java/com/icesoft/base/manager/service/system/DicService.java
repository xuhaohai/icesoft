package com.icesoft.base.manager.service.system;

import com.icesoft.base.manager.base.BaseService;
import com.icesoft.base.manager.entity.system.Dic;
import com.icesoft.core.common.helper.Resp;
import com.icesoft.core.dao.criteria.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DicService extends BaseService<Dic> {

    @Autowired
    DicItemService dicItemService;

    /**
     * 通过字典码查找
     *
     * @param code 字典码
     * @return 查找结果 or null
     */
    public Dic findByCode(String code) {
        return unique(QueryBuilder.get("code", code));
    }

    /**
     * @param ids
     * @return Resp<?>
     * @Description 级联删除
     */
    public Resp<?> deleteItem(Integer id) {
        if (!isExist(QueryBuilder.get("id", id))) {
            return Resp.error("没有访问权限");
        }
        //级联删除
        dicItemService.delete(dicItemService.findByDicId(id));
        delete(id);
        return Resp.success();
    }


}
