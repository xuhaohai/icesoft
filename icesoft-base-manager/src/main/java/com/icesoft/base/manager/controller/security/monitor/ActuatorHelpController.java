package com.icesoft.base.manager.controller.security.monitor;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.actuate.autoconfigure.endpoint.web.WebEndpointProperties;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 监控端点接口
 */
@RestController
@RequestMapping("security/actuator")
@AllArgsConstructor
@Slf4j
public class ActuatorHelpController {

    private WebEndpointProperties webEndpointProperties;

    /**
     * 监控端点基础路径
     * */
    @GetMapping("basePath")
    public String logLevel() {
        return webEndpointProperties.getBasePath();
    }


}
