package com.icesoft.base.manager.controller.manager;

import com.icesoft.base.manager.config.UrlConstant;
import com.icesoft.base.manager.entity.manager.SysSetting;
import com.icesoft.base.manager.helper.PageParam;
import com.icesoft.base.manager.helper.PageResp;
import com.icesoft.base.manager.helper.QueryBuilderUtils;
import com.icesoft.base.manager.service.manager.SysSettingService;
import com.icesoft.core.common.helper.Resp;
import com.icesoft.core.dao.criteria.Page;
import com.icesoft.core.dao.criteria.QueryBuilder;
import com.icesoft.core.web.suppose.setting.BasePropertySetting;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * 参数设置管理接口
 */
@RestController
@RequestMapping("manager/setting")
@Slf4j
@AllArgsConstructor
public class SysSettingController {
    private SysSettingService sysSettingService;
    private List<BasePropertySetting<?>> settings;

    /**
     * 获取编辑数据
     *
     * @param id
     * @return Resp
     * @Description
     */
    @GetMapping(value = UrlConstant.LOAD)
    public Resp load(int id) {
        return Resp.success(sysSettingService.load(id));
    }

    /**
     * 删除
     *
     * @param ids
     * @return Resp<?>
     * @Description
     */
    @PostMapping(value = UrlConstant.DELETE)
    public Resp<?> delete(int[] ids) {
        sysSettingService.delete(ids);
        return Resp.success();
    }

    /**
     * 更新非空字段
     */
    @PostMapping(value = UrlConstant.UPDATE)
    public Resp update(SysSetting sysSetting) {
        sysSettingService.updateNotNullProperties(sysSetting);
        return Resp.success();
    }

    /**
     * 查询
     *
     * @param request
     * @param queryParam 实体属性查询条件
     * @param search     筛选查询条件
     * @param pageParam  分页查询条件
     * @return PageResp 分页数据
     * @Description
     */
    @GetMapping(UrlConstant.QUERY)
    public PageResp query(HttpServletRequest request, SysSetting queryParam, String search, PageParam pageParam) {
        QueryBuilder queryBuilder = QueryBuilder.get();
        QueryBuilderUtils.addRequestParam(queryBuilder, request, queryParam);
        QueryBuilderUtils.addSearchParam(queryBuilder, search, new String[]{"groupName", "keyName"});
        Page<SysSetting> page = sysSettingService.page(queryBuilder, pageParam);
        return new PageResp<>(page);
    }

    /**
     * 查询配置组
     *
     * @return Resp<List < String>>
     * @Description
     */
    @GetMapping("findGroups")
    public Resp<List<String>> findGroups() {
        Resp<List<String>> resp = new Resp<>();
        resp.setData(sysSettingService.findGroups());
        return resp;
    }

    /**
     * 刷新配置组/项
     *
     * @return Resp<?>
     * @Description
     */
    @GetMapping("checkAllProperty")
    public Resp<?> checkAllProperty() {
        int count = 0;
        int delCount = 0;
        List<String> groups = new ArrayList<>(sysSettingService.findGroups());
        for (BasePropertySetting<?> basePropertySetting : settings) {
            count += basePropertySetting.checkEveryPropertyRegister();
            delCount += basePropertySetting.clearProperties();
            groups.remove(basePropertySetting.getGroupName());
        }
        for (String name : groups) {
            log.info("删除配置组：{}", name);
            delCount += sysSettingService.remove(name);
        }
        Resp<?> resp = new Resp<>();
        resp.setMsg("新生成了" + count + "条配置项，清理了" + delCount + "条无用配置项");
        return resp;
    }

}
