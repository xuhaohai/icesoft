package com.icesoft.base.manager.controller.common;

import com.icesoft.base.manager.config.SystemPropertySetting;
import com.icesoft.base.manager.entity.security.SysMenu;
import com.icesoft.base.manager.helper.AuthUser;
import com.icesoft.base.manager.helper.SecurityUtils;
import com.icesoft.base.manager.model.MenuVo;
import com.icesoft.base.manager.security.config.ApiMatchAuthority;
import com.icesoft.base.manager.security.config.LoginPropertySetting;
import com.icesoft.base.manager.security.config.SecurityProperty;
import com.icesoft.base.manager.security.config.WebSecurityConfig;
import com.icesoft.base.manager.service.security.SysApiAuthorityService;
import com.icesoft.base.manager.service.security.SysMenuService;
import com.icesoft.core.common.helper.Resp;
import com.icesoft.core.common.util.DateUtils;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.stream.Collectors;


/**
 * 首页接口
 */
@RestController
@RequestMapping("system/common")
@Slf4j
@AllArgsConstructor
public class ManagerCommonSystemController {
    private final SysMenuService sysMenuService;
    private final SystemPropertySetting systemPropertySetting;
    private final SysApiAuthorityService sysApiAuthorityService;
    private final SecurityProperty securityProperty;
    private final LoginPropertySetting loginPropertySetting;

    /**
     * 首页信息查询
     */
    @GetMapping(value = "index.json")
    public Resp indexInit(AuthUser authUser) {
        Map<String, Object> map = new HashMap<>(16);
        Map<String, Object> home = new HashMap<>(16);
        boolean isAdmin = SecurityUtils.isSuperAdmin();
        List<SysMenu> sysMenus;
        if (isAdmin) {
            sysMenus = sysMenuService.findAll();
        } else {
            sysMenus = sysMenuService.findByUserId(authUser.getId());
        }
        List<MenuVo> menuInfo = sysMenuService.hierarchy(sysMenus);
        home.put("title", "首页");
        home.put("href", systemPropertySetting.getHomePageUrl());

        map.put("loginSuccessUrl", loginPropertySetting.getSuccessUrl());
        map.put("menuInfo", menuInfo);
        map.put("homeInfo", home);
        map.put("authUser", authUser);
        return Resp.success(map);
    }

    /**
     * 获取当前用户权限
     */
    @GetMapping("whiteUrls")
    public Resp<Set<String>> whiteUrls(AuthUser authUser) {
        Set<String> urls = sysApiAuthorityService.findUrlsByUserId(authUser.getId()).stream().map(ApiMatchAuthority::getAuthority).collect(Collectors.toSet());
        urls.addAll(Arrays.asList(securityProperty.getIgnoreUrls().split(",")));
        urls.addAll(Arrays.asList(securityProperty.getDefaultIgnoreUrls().split(",")));
        urls.addAll(Arrays.asList(WebSecurityConfig.authenticatedUrls));
        return Resp.success(urls);
    }

    /**
     * session保持接口
     */
    @GetMapping("session/sessionKeeper")
    public Resp<?> sessionKeeper(AuthUser user) {
        String onLineTime = DateUtils.toTotalTime((System.currentTimeMillis() - user.getLoginTime().getTime()) / 1000).toString(true);
        log.info("当前用户：{}，在线时间：{}，账号：{}", user.getName(), onLineTime, user.getUsername());
        return Resp.success(onLineTime);
    }
}