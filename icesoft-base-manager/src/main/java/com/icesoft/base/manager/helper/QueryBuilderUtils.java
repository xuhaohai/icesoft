package com.icesoft.base.manager.helper;

import com.icesoft.core.common.util.DateUtils;
import com.icesoft.core.dao.base.BaseModel;
import com.icesoft.core.dao.criteria.Operator;
import com.icesoft.core.dao.criteria.QueryBuilder;
import com.icesoft.core.dao.criteria.Sequence;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.PropertyAccessorFactory;
import org.springframework.beans.propertyeditors.CustomDateEditor;

import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Enumeration;

public abstract class QueryBuilderUtils {


    /**
     * 绑定request中参数匹配的属性条件，默认使用EQ，例：<br/>
     * <br/>
     * createTime_GE: queryBuilder.and("createTime", Operator.GE ,createTime_GE);<br/>
     * createTime_LE: queryBuilder.and("createTime", Operator.LE, createTime_LE);<br/>
     * id_ORDER=DESC: queryBuilder.orderBy("id", Sequence.DESC);<br/>
     */
    public static <T extends BaseModel> void addRequestParam(QueryBuilder queryBuilder, HttpServletRequest request,
                                                             T queryParam) {
        BeanWrapper beanWrapper = PropertyAccessorFactory.forBeanPropertyAccess(queryParam);
        beanWrapper.setAutoGrowNestedPaths(true);
        //日期字符串转换
        DateFormat formatter = new SimpleDateFormat(DateUtils.DATE_TIME_FORMAT);
        beanWrapper.registerCustomEditor(java.util.Date.class, new CustomDateEditor(formatter, true));
        Enumeration<?> pars = request.getParameterNames();
        while (pars.hasMoreElements()) {
            String par = (String) pars.nextElement();
            String param = par;
            Operator operator = Operator.EQ;
            if (param.contains("_")) {
                String[] params = param.split("_");
                if (params.length != 2) {
                    continue;
                }
                param = params[0];
                operator = Operator.get(params[1]);
                if (operator == null && "ORDER".equals(params[1])) {
                    Sequence sequence = Sequence.get(request.getParameter(par));
                    if (sequence != null && beanWrapper.isReadableProperty(param)) {
                        queryBuilder.orderBy(param, sequence);
                    }
                    continue;
                }
                beanWrapper.setPropertyValue(param, request.getParameter(par));
            }
            if (beanWrapper.isReadableProperty(param)) {
                Object value = beanWrapper.getPropertyValue(param);
                if (value != null) {
                    queryBuilder.and(param, operator, value);
                }
            }
        }
    }

    public static void addSearchParam(QueryBuilder queryBuilder, String search, String[] searchFieldNames) {
        if (searchFieldNames != null && StringUtils.isNotBlank(search)) {
            queryBuilder.andSub();
            search = search.trim();
            String searchValue = search + "%";
            for (String name : searchFieldNames) {
                if (name.equals("id") && StringUtils.isNumeric(search)) {
                    queryBuilder.orEq(name, search);
                } else {
                    if (name.startsWith("%")) {
                        name = name.substring(1);
                        searchValue = "%" + searchValue;
                    }
                    queryBuilder.orLike(name, searchValue);
                }
            }
            queryBuilder.andSub();
        }
    }

}
