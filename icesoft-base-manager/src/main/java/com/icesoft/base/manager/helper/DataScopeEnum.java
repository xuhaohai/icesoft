package com.icesoft.base.manager.helper;

import com.icesoft.base.manager.dao.IDataScopeFilter;
import com.icesoft.base.manager.entity.security.SysRole;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author <p>
 * 数据权限类型
 */
@Getter
@AllArgsConstructor
public enum DataScopeEnum {
    /**
     * 本部门
     */
    OWN(1, "本部门"),

    /**
     * 本部门及下一级
     */
    OWN_CHILD_ONE(1 << 1, "本部门及下一级"),
    /**
     * 同一层级
     */
    OWN_LEVEL(1 << 2, "同一层级"),
    /**
     * 本公司
     */
    OWN_COMPANY(3, "本公司"),
    /**
     * 查询全部数据
     */
    ALL(1 << 4, "全部数据"),

    /**
     * 跟随部门自定义（默认所有子级）
     */
    ORG_OWNER_CUSTOM(1 << 5, "跟随部门自定义"),
    /**
     * 自定义
     */
    CUSTOM(1 << 6, "自定义");
    /**
     * 类型
     */
    private final int code;
    /**
     * 描述
     */
    private final String description;

    public static Map<String, String> toMap() {
        Map<String, String> map = new HashMap<>();
        for (DataScopeEnum dataScopeEnum : values()) {
            map.put(dataScopeEnum.name(), dataScopeEnum.description);
        }
        return map;
    }

    public static void runScopeFilter(List<SysRole> sysRoles, IDataScopeFilter dataScopeFilter) {
        if (!dataScopeFilter.suppose()) {
            return;
        }
        int sumCode = sysRoles.stream().map(SysRole::getDataScopeEnum).distinct().mapToInt(DataScopeEnum::getCode).sum();
        if ((sumCode & DataScopeEnum.ALL.code) == DataScopeEnum.ALL.code) {
            dataScopeFilter.scopeAll();
            return;
        }
        if ((sumCode & DataScopeEnum.OWN_COMPANY.code) == DataScopeEnum.OWN_COMPANY.code) {
            dataScopeFilter.scopeOwnCompany();
            return;
        }
        List<Integer> customRoleIds = sysRoles.stream().filter(role -> DataScopeEnum.CUSTOM.equals(role.getDataScopeEnum()))
                .map(SysRole::getId).collect(Collectors.toList());
        if ((sumCode & DataScopeEnum.CUSTOM.code) == DataScopeEnum.CUSTOM.code) {
            dataScopeFilter.scopeCustom(customRoleIds);
        }
        if ((sumCode & DataScopeEnum.ORG_OWNER_CUSTOM.code) == DataScopeEnum.ORG_OWNER_CUSTOM.code) {
            dataScopeFilter.scopeOrgOwnerCustom();
        }
        boolean isContainOwn = false;
        if ((sumCode & DataScopeEnum.OWN_CHILD_ONE.code) == DataScopeEnum.OWN_CHILD_ONE.code) {
            dataScopeFilter.scopeOwnChildOne();
            isContainOwn = true;
        }
        if ((sumCode & DataScopeEnum.OWN_LEVEL.code) == DataScopeEnum.OWN_LEVEL.code) {
            dataScopeFilter.scopeOwnLevel();
            isContainOwn = true;
        }
        if (!isContainOwn && (sumCode & DataScopeEnum.OWN.code) == DataScopeEnum.OWN.code) {
            dataScopeFilter.scopeOwn();
        }
    }
}
