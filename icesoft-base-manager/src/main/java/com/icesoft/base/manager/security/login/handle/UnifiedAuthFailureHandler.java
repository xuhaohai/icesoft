package com.icesoft.base.manager.security.login.handle;

import com.icesoft.core.common.helper.Resp;
import com.icesoft.core.web.helper.ResponseUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
@Slf4j
public class UnifiedAuthFailureHandler implements AuthenticationFailureHandler {

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
                                        AuthenticationException exception) {
        log.info("登陆请求失败：{}", exception.getMessage());
        String msg = exception.getMessage();
        if (msg == null) {
            msg = "用户名或密码错误";
        }
        ResponseUtils.writeJson(response, Resp.error(msg));
    }

}
