package com.icesoft.base.manager.controller.security.monitor;

import com.icesoft.core.common.log.ILogMessageListener;
import com.icesoft.core.common.log.LogMessage;
import lombok.AllArgsConstructor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class WebsocketLogMsgSender implements ILogMessageListener {
    public static final String CONSOLE_LOG_TOPIC = "/topic/consoleLog";
    private SimpMessagingTemplate template;

    @Override
    public void handle(LogMessage logMessage) {
        template.convertAndSendToUser("admin", CONSOLE_LOG_TOPIC, logMessage);
    }
}