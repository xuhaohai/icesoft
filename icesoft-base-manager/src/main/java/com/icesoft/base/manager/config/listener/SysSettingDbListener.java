package com.icesoft.base.manager.config.listener;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Component;

import com.icesoft.core.dao.suppose.BaseModelListener;
import com.icesoft.base.manager.entity.manager.SysSetting;
import com.icesoft.core.web.suppose.setting.service.ISettingRefreshEvent;
import com.icesoft.base.manager.service.manager.SysSettingService;

@Component
public class SysSettingDbListener extends BaseModelListener<SysSetting> {
	@Autowired(required = false)
	List<ISettingRefreshEvent<?>> refreshEvents;
	@Autowired
	SysSettingService sysSettingService;

	@Override
	@Caching(evict = { @CacheEvict(cacheNames = SysSettingService.CACHE_GROUP, key = "#model.groupName"),
			@CacheEvict(cacheNames = SysSettingService.CACHE_GROUP, key = SysSettingService.KEY_FIND_GROUPS) })
	public void postPersist(SysSetting model) {
	}

	@Override
	@Caching(evict = { @CacheEvict(cacheNames = SysSettingService.CACHE_GROUP, key = "#model.groupName"),
			@CacheEvict(cacheNames = SysSettingService.CACHE_GROUP, key = SysSettingService.KEY_FIND_GROUPS) })
	public void postUpdate(SysSetting model) {
		refreshEvents.forEach(event -> {
			if (event.getSetting().getGroupName().equals(model.getGroupName())) {
				event.refresh(model.getKeyName(), model.getVal());
			}
		});
	}

	@Caching(evict = { @CacheEvict(cacheNames = SysSettingService.CACHE_GROUP, key = "#model.groupName"),
			@CacheEvict(cacheNames = SysSettingService.CACHE_GROUP, key = SysSettingService.KEY_FIND_GROUPS) })
	@Override
	public void postRemove(SysSetting model) {
		super.postRemove(model);
	}

}
