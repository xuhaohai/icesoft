package com.icesoft.base.manager.config.resolver;

import com.icesoft.base.manager.base.BaseUser;
import com.icesoft.core.web.suppose.token.IAuthToken;

public interface ITokenUserService {
    BaseUser loadUserByAuthToken(IAuthToken authToken);
}
