package com.icesoft.base.manager.entity.system;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.icesoft.base.manager.suppose.validation.SensitiveWordForbidden;
import com.icesoft.core.dao.base.BaseModel;
import com.icesoft.core.web.suppose.validation.ChineseOrEnglishOrNumber;
import com.icesoft.core.dao.suppose.validation.UniqueCheck;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Table(name = "sys_dic")
@Entity
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Cacheable
public class Dic extends BaseModel {
	@Column(length = 50, nullable = false)
	@NotEmpty(message = "字典名称不能为空")
	@Size(min = 1, max = 20, message = "字典名称太长")
	@SensitiveWordForbidden(wordGroups = "安全")
	private String name;

	@Column(unique = true, length = 20, nullable = false)
	@NotEmpty(message = "字典码不能为空")
	@Size(min = 1, max = 20, message = "字典码太长")
	@UniqueCheck(message = "字典码已存在")
	@ChineseOrEnglishOrNumber
	private String code;

	@Column(length = 100)
	private String detail;
}
