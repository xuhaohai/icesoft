/**
* @Title: DicItemExportController.java
* @Package com.icesoft.base.manager.controller.system
* @Description: TODO
* @author hou
* @date 2020年11月23日
* @version V1.0
*/
package com.icesoft.base.manager.controller.manager;

import com.icesoft.base.manager.base.BaseExportAction;
import com.icesoft.base.manager.base.BaseExportController;
import com.icesoft.base.manager.entity.system.DicItem;
import com.icesoft.base.manager.entity.manager.RelSysOrgOwner;
import com.icesoft.base.manager.helper.AuthUser;
import com.icesoft.base.manager.helper.SecurityUtils;
import com.icesoft.base.manager.service.system.DicItemService;
import com.icesoft.core.dao.criteria.FromBuilder;
import com.icesoft.core.dao.criteria.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @ClassName: DicItemExportController
 * @Description: TODO
 * @author hou 2020年11月23日
 */
@RequestMapping("security/dicItem/export")
public class DicItemExportController extends BaseExportController<DicItem> {
	@Autowired
	DicItemService dicItemService;
	
	@Override
	public BaseExportAction<DicItem> getExportAction() {
		return new DicItemExportAction();
	}

	@Override
	public List<DicItem> findSelect(HttpServletRequest request) {
		AuthUser sysUser = SecurityUtils.getUser();
        int orgId = sysUser.getOrgId();
        FromBuilder fb = FromBuilder.get();
        fb.join(RelSysOrgOwner.class, "orgOwner").on("orgOwner.ownerOrgId", "orgId")
                .where().andEq("orgOwner.orgId", orgId).end();
        QueryBuilder qb = QueryBuilder.get();
		return dicItemService.findAll();
	}

}
