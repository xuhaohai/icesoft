package com.icesoft.base.manager.entity.manager;

import com.icesoft.base.manager.suppose.NotUpdatePart;
import com.icesoft.core.dao.base.BaseModel;
import com.icesoft.core.web.suppose.validation.ChineseOrEnglishOrNumber;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;

@Table(name = "sys_setting", uniqueConstraints = @UniqueConstraint(columnNames = {"groupName", "keyName"}))
@Entity
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Cacheable
public class SysSetting extends BaseModel {

    @NotUpdatePart
    @Column(length = 20, nullable = false, updatable = false)
    @ChineseOrEnglishOrNumber
    private String groupName;
    private String des;
    @Column(length = 100, nullable = false)
    @NotUpdatePart
    @ChineseOrEnglishOrNumber
    private String keyName;
    @Column(nullable = false)
    private String val;
}
