package com.icesoft.base.manager.config.listener;

import com.icesoft.base.manager.entity.security.SysApiAuthorityGroup;
import com.icesoft.base.manager.service.security.SysApiAuthorityService;
import com.icesoft.core.dao.suppose.BaseModelListener;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class SysApiAuthorityGroupDbListener extends BaseModelListener<SysApiAuthorityGroup> {
    private SysApiAuthorityService sysApiAuthorityService;

    @Override
    public void preRemove(SysApiAuthorityGroup model) {
        sysApiAuthorityService.deleteByGroupId(model.getId());
    }
}
