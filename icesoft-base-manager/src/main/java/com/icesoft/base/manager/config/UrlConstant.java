package com.icesoft.base.manager.config;

import java.util.HashMap;
import java.util.Map;

public abstract class UrlConstant {
    public static final String QUERY = "query";
    public static final String CREATE = "create";
    public static final String UPDATE = "update";
    public static final String DELETE = "delete";
    public static final String LOAD = "load";
    public static final String EXPORT = "export.action";
    public static final String IMP = "imp.action";
    public static final String[] BASE_URL_ARRAY = new String[]{QUERY, CREATE, UPDATE, DELETE, EXPORT, IMP};

    public static Map<String, String> getUrlNameMap() {
        Map<String, String> map = new HashMap<>(BASE_URL_ARRAY.length);
        map.put(CREATE, "创建");
        map.put(UPDATE, "修改");
        map.put(DELETE, "删除");
        map.put(QUERY, "查询");
        map.put(IMP, "导入");
        map.put(EXPORT, "导出");
        return map;
    }
}
