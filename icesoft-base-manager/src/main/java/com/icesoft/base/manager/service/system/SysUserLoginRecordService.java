package com.icesoft.base.manager.service.system;

import com.icesoft.base.manager.base.BaseService;
import com.icesoft.base.manager.entity.system.SysUserLoginRecord;
import com.icesoft.core.dao.criteria.QueryBuilder;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class SysUserLoginRecordService extends BaseService<SysUserLoginRecord> {

    public int errorCountByUserIdAndGeCreateTime(int userId, Date createTime) {
        return count(QueryBuilder.get("userId", userId).andEq("loginSuccess", false).andGe("createTime", createTime));
    }

    public int errorCountByUsernameAndGeCreateTime(String username, Date createTime) {
        return count(QueryBuilder.get("username", username).andEq("loginSuccess", false).andGe("createTime", createTime));
    }

    public SysUserLoginRecord loadLatestLoginFailureRecord(String username) {
        return unique(QueryBuilder.get("username", username).andEq("loginSuccess", false).desc("createTime").limit(1));
    }
}
