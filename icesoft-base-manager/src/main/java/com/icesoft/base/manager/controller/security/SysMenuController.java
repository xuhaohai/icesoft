package com.icesoft.base.manager.controller.security;

import com.icesoft.base.manager.config.UrlConstant;
import com.icesoft.base.manager.config.register.MenuRegister;
import com.icesoft.base.manager.entity.security.SysMenu;
import com.icesoft.base.manager.model.MenuVo;
import com.icesoft.base.manager.service.security.SysMenuService;
import com.icesoft.core.common.helper.Resp;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * 菜单管理接口
 * */
@RestController
@RequestMapping("security/menu/")
@AllArgsConstructor
public class SysMenuController {
    private MenuRegister menuRegister;
    private SysMenuService sysMenuService;

    /**
     * 通过id查询菜单
     * */
    @GetMapping(value = UrlConstant.LOAD)
    public Resp load(int id) {
        return Resp.success(sysMenuService.load(id));
    }
    /**
     * 修改菜单
     * */
    @PostMapping(value = UrlConstant.UPDATE)
    public Resp update(SysMenu queryParam) {
        SysMenu sysMenu = sysMenuService.load(queryParam.getId());
        if (sysMenu != null && !sysMenu.getId().equals(queryParam.getId())) {
            return Resp.error("菜单名已存在");
        }
        sysMenuService.updateNotNullProperties(queryParam);
        return Resp.success();
    }
    /**
     * 查询所有菜单
     * */
    @GetMapping(UrlConstant.QUERY)
    public Resp<List<MenuVo>> query() {
        return Resp.success(sysMenuService.hierarchy(sysMenuService.findAll()));
    }
    /**
     * 创建菜单
     * */
    @PostMapping(value = UrlConstant.CREATE)
    public Resp<Integer> create(@Valid SysMenu sysMenu) {
        sysMenuService.create(sysMenu);
        return Resp.success(sysMenu.getId());
    }
    /**
     * 删除菜单
     * */
    @PostMapping(value = UrlConstant.DELETE)
    public Resp<?> delete(int[] ids) {
        sysMenuService.delete(ids);
        return Resp.success();
    }
    /**
     * 检查未生成的菜单
     * */
    @PostMapping(value = "/checkRegisterMenu")
    public Resp<Integer> checkRegisterMenu() {
        return Resp.success(menuRegister.checkCreateMenu());
    }

}
