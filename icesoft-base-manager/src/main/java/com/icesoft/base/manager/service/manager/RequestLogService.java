package com.icesoft.base.manager.service.manager;

import com.icesoft.base.manager.base.BaseService;
import org.springframework.stereotype.Service;

import com.icesoft.base.manager.entity.manager.RequestLog;

@Service
public class RequestLogService extends BaseService<RequestLog> {

}
