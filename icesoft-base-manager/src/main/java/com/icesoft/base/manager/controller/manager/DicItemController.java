package com.icesoft.base.manager.controller.manager;

import com.icesoft.base.manager.config.UrlConstant;
import com.icesoft.base.manager.entity.system.DicItem;
import com.icesoft.base.manager.helper.PageParam;
import com.icesoft.base.manager.helper.PageResp;
import com.icesoft.base.manager.helper.QueryBuilderUtils;
import com.icesoft.base.manager.service.system.DicItemService;
import com.icesoft.core.common.helper.Resp;
import com.icesoft.core.dao.criteria.Page;
import com.icesoft.core.dao.criteria.QueryBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * 字典项管理
 * */
@RestController
@RequestMapping("manager/dicItem")
@AllArgsConstructor
@Slf4j
public class DicItemController {
    private DicItemService dicItemService;

    /**
     * 获取编辑数据
     *
     * @param id
     * @return Resp
     * @Description
     */
    @GetMapping(value = UrlConstant.LOAD)
    public Resp load(int id) {
        return Resp.success(dicItemService.load(id));
    }

    /**
     * 新增
     *
     * @param dicItem
     * @param request
     * @return Resp<?>
     * @Description
     */
    @PostMapping(value = UrlConstant.CREATE)
    public Resp<?> create(@Valid DicItem dicItem, HttpServletRequest request) {
        dicItemService.create(dicItem);
        return Resp.success();
    }

    /**
     * 删除
     *
     * @param ids
     * @return Resp<?>
     * @Description
     */
    @PostMapping(value = UrlConstant.DELETE)
    public Resp<?> delete(int[] ids) {
        dicItemService.delete(ids);
        return Resp.success();
    }

    /**
     * 更新非空字段
     *
     * @param request
     * @param dicItem void
     * @Description
     */
    @PostMapping(value = UrlConstant.UPDATE)
    public Resp<?> update(HttpServletRequest request, DicItem dicItem) {
        dicItemService.updateNotNullProperties(dicItem);
        return Resp.success();
    }

    /**
     * 查询
     *
     * @param request
     * @param queryParam 实体属性查询条件
     * @param search     筛选查询条件
     * @param pageParam  分页查询条件
     * @return PageResp 分页数据
     * @Description
     */
    @GetMapping(UrlConstant.QUERY)
    public PageResp query(HttpServletRequest request, DicItem queryParam, String search, PageParam pageParam) {
        log.debug("queryParam:{}", queryParam);
        QueryBuilder queryBuilder = QueryBuilder.get();
        QueryBuilderUtils.addRequestParam(queryBuilder, request, queryParam);
        QueryBuilderUtils.addSearchParam(queryBuilder, search, new String[]{"name", "code"});
        Page<DicItem> page = dicItemService.page(queryBuilder, pageParam);
        return new PageResp<>(page);
    }

}
