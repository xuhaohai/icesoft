package com.icesoft.base.manager.suppose.validation;

import com.icesoft.base.manager.suppose.validation.impl.SensitiveWordForbiddenValidator;
import org.springframework.core.annotation.AliasFor;

import javax.validation.Constraint;
import javax.validation.Payload;  
import java.lang.annotation.Documented;  
import java.lang.annotation.Retention;  
import java.lang.annotation.Target;  
import static java.lang.annotation.ElementType.*;  
import static java.lang.annotation.RetentionPolicy.*;  

@Target({ FIELD, METHOD, PARAMETER, ANNOTATION_TYPE })  
@Retention(RUNTIME)  
//指定验证器  
@Constraint(validatedBy = SensitiveWordForbiddenValidator.class)
@Documented  
public @interface SensitiveWordForbidden {
    @AliasFor("wordGroups")
    String[] value() default {};
    //默认错误消息
    String message() default "您输入的数据中有非法关键词";
    @AliasFor("value")
    String[] wordGroups() default { };
    Class<?>[] groups() default { };
    //分组

    //负载  
    Class<? extends Payload>[] payload() default { };  
  
    //指定多个时使用
    @Target({ FIELD, METHOD, PARAMETER, ANNOTATION_TYPE })
    @Retention(RUNTIME)
    @Documented
    @interface List {
        SensitiveWordForbidden[] value();
    }
}
