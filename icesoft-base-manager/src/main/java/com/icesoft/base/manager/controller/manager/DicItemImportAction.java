package com.icesoft.base.manager.controller.manager;

import com.icesoft.base.manager.base.BaseImportAction;
import com.icesoft.base.manager.entity.system.DicItem;
import com.icesoft.core.web.helper.RequestHold;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@NoArgsConstructor
@AllArgsConstructor
public class DicItemImportAction extends BaseImportAction<DicItem> {
	private HttpServletRequest req;

	@Override
	protected DicItem convert(Map<String, String> model) {
		Map<String, String> map = RequestHold.getRequestParamMap(req);
		String dicIdStr = map.get("dicId");
		log.debug("dicIdStr:{}", dicIdStr);
		DicItem dicItem = new DicItem();
		Integer dId = (StringUtils.isEmpty(dicIdStr)) ? 0 : Integer.parseInt(dicIdStr);
		dicItem.setDicId(dId);
		String iname = model.get("字典项名");
		String ival = model.get("值");
		dicItem.setName(iname);
		dicItem.setValue(ival);
		return dicItem;
	}
}
