package com.icesoft.base.manager.controller.manager;

import com.icesoft.base.manager.base.BaseExportAction;
import com.icesoft.base.manager.entity.system.DicItem;

/**
 * @ClassName: DicExportAction
 * @Description: TODO
 * @author hou 2020年11月23日
 */
public class DicItemExportAction extends BaseExportAction<DicItem> {

	@Override
	protected String[] getExcelHead() {
		return new String[]{"字典项名","值"};
	}

	@Override
	protected String[] convert(DicItem model) {
		// TODO Auto-generated method stub
		return new String[] { model.getName(), model.getValue() };
	}

}
