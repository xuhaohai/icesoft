package com.icesoft.base.manager.controller.security.monitor;

import com.icesoft.base.manager.config.UrlConstant;
import com.icesoft.base.manager.entity.manager.RequestLogConfig;
import com.icesoft.base.manager.helper.PageParam;
import com.icesoft.base.manager.helper.PageResp;
import com.icesoft.base.manager.helper.QueryBuilderUtils;
import com.icesoft.base.manager.service.manager.RequestLogConfigService;
import com.icesoft.core.common.helper.Resp;
import com.icesoft.core.dao.criteria.Page;
import com.icesoft.core.dao.criteria.QueryBuilder;
import com.icesoft.core.web.helper.RequestHold;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * 接口日志配置
 * */
@RestController
@RequestMapping(value = "security/requestLogConfig")
@AllArgsConstructor
@Slf4j
public class RequestLogConfigController {
	private RequestLogConfigService requestLogConfigService;

	/**
	 * 通过id查询
	 * 
	 */
	@GetMapping(value = UrlConstant.LOAD)
	public Resp load(int id) {
		return Resp.success(requestLogConfigService.load(id));
	}

	/**
	 * 新增
	 * 
	 */
	@PostMapping(value = UrlConstant.CREATE)
	public Resp<?> create(@Valid RequestLogConfig requestLogConfig, HttpServletRequest request) {
		requestLogConfigService.create(requestLogConfig);
		return Resp.success();
	}

	/**
	 * 删除
	 */
	@PostMapping(value = UrlConstant.DELETE)
	public Resp<?> delete(int[] ids) {
		requestLogConfigService.delete(ids);
		return Resp.success();
	}

	/**
	 * 更新非空字段
	 */
	@PostMapping(value = UrlConstant.UPDATE)
	public Resp<?> update(HttpServletRequest request, RequestLogConfig requestLogConfig) {
		String logLevel = RequestHold.getString(request, "logLevel");
		log.debug("logLevel:{}", logLevel);
		requestLogConfigService.updateNotNullProperties(requestLogConfig);
		return Resp.success();
	}

	/**
	 * 查询
	 * 
	 * @param queryParam 实体属性查询条件
	 * @param search     筛选查询条件
	 * @param pageParam  分页查询条件
	 * @return PageResp 分页数据
	 */
	@GetMapping(UrlConstant.QUERY)
	public PageResp query(HttpServletRequest request, RequestLogConfig queryParam, String search, PageParam pageParam) {
		QueryBuilder queryBuilder = QueryBuilder.get();
		QueryBuilderUtils.addRequestParam(queryBuilder, request, queryParam);
		QueryBuilderUtils.addSearchParam(queryBuilder, search, new String[] { "logLevel", "url" });
		Page<RequestLogConfig> page = requestLogConfigService.page(queryBuilder, pageParam);
		return new PageResp<>(page);
	}
}
