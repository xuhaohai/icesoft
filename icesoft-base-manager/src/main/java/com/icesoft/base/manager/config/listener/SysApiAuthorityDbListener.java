package com.icesoft.base.manager.config.listener;

import com.icesoft.base.manager.entity.security.SysApiAuthority;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Component;

import com.icesoft.core.dao.suppose.BaseModelListener;
import com.icesoft.base.manager.service.security.RelSysRoleAuthorityService;
import com.icesoft.base.manager.service.security.SysApiAuthorityService;

@Component
public class SysApiAuthorityDbListener extends BaseModelListener<SysApiAuthority> {
	@Autowired
	private RelSysRoleAuthorityService relSysRoleAuthorityService;

	@Override
	public void preRemove(SysApiAuthority model) {
		relSysRoleAuthorityService.deleteByAuthorityId(model.getId());
	}

	@Override
	@CacheEvict(cacheNames = SysApiAuthorityService.CACHE_GROUP, key = SysApiAuthorityService.KEY_FIND_ALL_AUTHORITY)
	public void postPersist(SysApiAuthority model) {
	}

	@Override
	@CacheEvict(cacheNames = SysApiAuthorityService.CACHE_GROUP, key = SysApiAuthorityService.KEY_FIND_ALL_AUTHORITY)
	public void postRemove(SysApiAuthority model) {

	}

	@Override
	@CacheEvict(cacheNames = SysApiAuthorityService.CACHE_GROUP, key = SysApiAuthorityService.KEY_FIND_ALL_AUTHORITY)
	public void postUpdate(SysApiAuthority model) {
	}

}
