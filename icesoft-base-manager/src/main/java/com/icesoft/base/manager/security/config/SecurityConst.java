package com.icesoft.base.manager.security.config;

public interface SecurityConst {

    String LOGIN_PROCESS_API = "/oauth/login_login";
    String PHONE_LOGIN_PROCESS_API = "/oauth/phone_login";
    String LOGOUT_API = "/oauth/logout";

    /**
     * 系统验证码
     */
    String SESSION_CAPTCHA = "cn.hutool.captcha";
}
