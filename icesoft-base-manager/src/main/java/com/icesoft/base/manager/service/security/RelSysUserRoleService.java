package com.icesoft.base.manager.service.security;

import com.icesoft.base.manager.entity.security.RelSysUserRole;
import com.icesoft.core.dao.base.BaseDao;
import com.icesoft.core.dao.criteria.QueryBuilder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RelSysUserRoleService extends BaseDao<RelSysUserRole> {

    public void deleteByUserId(int userId) {
        delete(findByUserId(userId));
    }

    public void deleteByRoleId(int roleId) {
        delete(findByRoleId(roleId));
    }

    public List<RelSysUserRole> findByRoleId(int roleId) {
        return find(QueryBuilder.get("roleId", roleId));
    }

    public List<RelSysUserRole> findByUserId(int userId) {
        return find(QueryBuilder.get("userId", userId));
    }

}
