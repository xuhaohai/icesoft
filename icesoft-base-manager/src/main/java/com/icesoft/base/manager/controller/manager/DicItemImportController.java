package com.icesoft.base.manager.controller.manager;

import com.icesoft.base.manager.base.BaseExcelController;
import com.icesoft.base.manager.base.BaseImportAction;
import com.icesoft.base.manager.entity.system.DicItem;
import com.icesoft.base.manager.service.system.DicItemService;
import com.icesoft.core.web.helper.RequestHold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;


/**
 * 数据字典导入接口
 */
@Controller
@RequestMapping("manager/dicItem/import")
public class DicItemImportController extends BaseExcelController<DicItem, DicItemService> {

    @Override
    public BaseImportAction<DicItem> getImportAction(HttpServletRequest req) {
        Map<String, String> paramMap = RequestHold.getRequestParamMap(req);
        String praramString = paramMap.get("dicId");
        log.debug("praramString:{}", praramString);
        String dicIdStr = req.getParameter("dicId");
        log.debug("dicIdStr:{}", dicIdStr);
        return new DicItemImportAction(req);
    }

    @Override
    public String getTemplateFileName() {
        return "数据字典导入(模板文件)";
    }
}
