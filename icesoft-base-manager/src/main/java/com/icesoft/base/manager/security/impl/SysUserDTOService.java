package com.icesoft.base.manager.security.impl;

import com.icesoft.base.manager.security.suppose.IUserDetailsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@Slf4j
public class SysUserDTOService implements IUserDetailsService {

    @Override
    public UserDetails loadUserByPhone(String phone) throws UsernameNotFoundException {
        throw new UsernameNotFoundException("无用户实现");
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        throw new UsernameNotFoundException("无用户实现");
    }
}
