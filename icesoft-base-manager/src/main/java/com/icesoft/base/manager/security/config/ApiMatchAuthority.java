package com.icesoft.base.manager.security.config;

import com.icesoft.core.web.helper.PathUtil;
import lombok.Getter;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;

@ToString
public class ApiMatchAuthority implements GrantedAuthority {

    private final String role;
    @Getter
    private final Set<String> patterns;

    public ApiMatchAuthority(String role, Collection<String> patterns) {
        this.role = role;
        this.patterns = patterns.stream().map(PathUtil::appendPathStart).collect(Collectors.toSet());
    }

    @Override
    public String getAuthority() {
        return role;
    }

}
