package com.icesoft.base.manager.controller.security.monitor;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.socket.config.WebSocketMessageBrokerStats;

import java.security.Principal;

/**
 * websocket连接测试
 */
@RestController
@AllArgsConstructor
@Slf4j
public class ConsoleLogController {
    private WebSocketMessageBrokerStats webSocketMessageBrokerStats;

    @MessageMapping("/info/test")
    public void info(String message, Principal principal) {
        log.info("收到消息[{}]，websocket状态：{}", message, principal);
    }

    @SubscribeMapping({"/sub/test"})
    public void subscribe() {
        log.info("subscribe成功，websocket状态：{}", webSocketMessageBrokerStats);
    }
}
