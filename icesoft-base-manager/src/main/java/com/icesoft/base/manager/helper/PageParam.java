package com.icesoft.base.manager.helper;

import lombok.Data;

@Data
public class PageParam {

    /**
     * 上一次查询的最后一条记录对应的偏移量 offset+limit
     */
    private Integer lastEndOffset;
    /**
     * 上一次查询的最后一条记录的id
     */
    private Integer lastEndId;

    /**
     * 当前的offset
     */
    private int offset;
    
    private int page;
    /**
     * 当前的limit
     */
    private int limit;
}
