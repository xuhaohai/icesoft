package com.icesoft.base.manager.controller.security.monitor;

import com.icesoft.base.manager.config.UrlConstant;
import com.icesoft.base.manager.entity.manager.RequestLog;
import com.icesoft.base.manager.helper.PageParam;
import com.icesoft.base.manager.helper.PageResp;
import com.icesoft.base.manager.helper.QueryBuilderUtils;
import com.icesoft.base.manager.service.manager.RequestLogService;
import com.icesoft.core.common.helper.Resp;
import com.icesoft.core.dao.criteria.Page;
import com.icesoft.core.dao.criteria.QueryBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 接口日志管理
 * */
@RequestMapping("security/requestLog")
@RestController
@AllArgsConstructor
@Slf4j
public class RequestLogController {
	private RequestLogService requestLogService;

	/**
	 * 删除
	 */
	@PostMapping(value = UrlConstant.DELETE)
	public Resp<?> delete(int[] ids) {
		requestLogService.delete(ids);
		return Resp.success();
	}

	/**
	 * 查询
	 * @param queryParam 实体属性查询条件
	 * @param search     筛选查询条件
	 * @param pageParam  分页查询条件
	 * @return PageResp 分页数据
	 */
	@GetMapping(UrlConstant.QUERY)
	public PageResp query(HttpServletRequest request, RequestLog queryParam, String search, PageParam pageParam) {
		log.debug("queryParam:{}",queryParam);
		QueryBuilder queryBuilder = QueryBuilder.get();
		QueryBuilderUtils.addRequestParam(queryBuilder, request, queryParam);
		QueryBuilderUtils.addSearchParam(queryBuilder, search, new String[] { "userName" });
		Page<RequestLog> page = requestLogService.page(queryBuilder, pageParam);
		return new PageResp<>(page);
	}

}
