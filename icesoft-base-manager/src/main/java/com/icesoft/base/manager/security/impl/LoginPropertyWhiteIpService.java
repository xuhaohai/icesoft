package com.icesoft.base.manager.security.impl;

import java.util.Set;

import com.icesoft.base.manager.security.config.LoginPropertySetting;
import com.icesoft.base.manager.security.suppose.IWhiteIpService;
import org.apache.commons.collections4.SetUtils;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@Component
public class LoginPropertyWhiteIpService implements IWhiteIpService {
	private LoginPropertySetting loginPropertySetting;

	@Override
	public Set<String> getWhiteIps() {
		return SetUtils.hashSet(loginPropertySetting.getBasicAuthWhiteIps());
	}


}
