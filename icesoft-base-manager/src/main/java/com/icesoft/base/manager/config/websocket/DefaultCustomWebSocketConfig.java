package com.icesoft.base.manager.config.websocket;

import com.icesoft.core.web.configurer.test.condition.ConditionOnNotMockTestCase;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@ConditionOnNotMockTestCase
@Configuration
@EnableWebSocketMessageBroker
@Slf4j
public class DefaultCustomWebSocketConfig implements WebSocketMessageBrokerConfigurer {

    /**
     * 配置 WebSocket 进入点，及开启使用 SockJS，这些配置主要用配置连接端点，用于 WebSocket 连接
     *
     * @param registry STOMP 端点
     */
    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/websocket")
                .addInterceptors(new DefaultCustomHandshakeInterceptor())
                .withSockJS();
        registry.addEndpoint("/adminWebsocket")
                .addInterceptors(new AdminHandshakeInterceptor())
                .withSockJS();
    }

    /**
     * 配置消息代理选项
     *
     * @param registry 消息代理注册配置
     */
    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        //这里注册两个，主要是目的是将广播和队列分开。
        //registry.enableStompBrokerRelay().setRelayHost().setRelayPort() 其他消息代理，rabbitmq或者其他支持stomp协议的mq
        //这里使用的是内存模式，生产环境可以使用rabbitmq或者其他mq。
        //服务器消息发送的路由前缀，必须以这些为前缀消息（toUser时还要加上/user最前缀），服务器才会发送，服务器发送时（@SendTo,convertAndSend）需要自己加上,@MessageMapping返回时会自动带上
        registry.enableSimpleBroker("/topic", "/queue");
        //服务器给客户端发送消息的名称前缀，服务器发送时(convertAndSendToUser,@SendToUser)会自动带上，客户端需要自己加上
        registry.setUserDestinationPrefix("/user");
        //客户端往服务器发送消息的名称前缀，服务器接收时(@MessageMapping,@SubscribeMapping)会自动带上，客户端发送时需要自己加上
        registry.setApplicationDestinationPrefixes("/app");
    }

}
