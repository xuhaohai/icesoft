package com.icesoft.base.manager.entity.system;

import com.icesoft.core.dao.base.BaseModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@ToString(callSuper = true)
@Table(name = "SYS_UserLoginRecord", indexes = { @Index(name = "idx_SysUserLoginRecord_userId", columnList = "userId"),
		@Index(name = "idx_SysUserLoginRecord_username", columnList = "username"),
		@Index(name = "idx_SysUserLoginRecord_createTime", columnList = "createTime") })
@Entity
public class SysUserLoginRecord extends BaseModel {

	private int userId;
	@Column(length = 40, nullable = false, updatable = false)
	private String username;
	@Column(nullable = false, updatable = false)
	@CreatedDate
	private Date loginTime;
	private Date logoutTime;
	@Column(length = 128, nullable = false, updatable = false)
	private String loginIp;
	@Column(nullable = false, updatable = false)
	private Boolean loginSuccess;
	private String errorMsg;
}
