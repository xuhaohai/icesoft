package com.icesoft.base.manager.security.suppose;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * @author xhh
 */
public interface IUserDetailsService extends UserDetailsService {

    /**
     * 手机验证码登录
     *
     * @param phone 手机号
     * @return UserDetails
     * @throws UsernameNotFoundException
     */
    UserDetails loadUserByPhone(String phone) throws UsernameNotFoundException;

}
