package com.icesoft.base.manager.service.manager;

import com.icesoft.base.manager.base.BaseService;
import com.icesoft.core.dao.criteria.QueryBuilder;
import com.icesoft.base.manager.entity.manager.RequestLogConfig;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
@CacheConfig(cacheNames = RequestLogConfigService.CACHE_GROUP)
public class RequestLogConfigService extends BaseService<RequestLogConfig> {
    public static final String CACHE_GROUP = "requestLogConfigService";

    @Cacheable(key = "#url")
    public RequestLogConfig findByUrl(String url) {
        return unique(QueryBuilder.get("url", url));
    }

}
