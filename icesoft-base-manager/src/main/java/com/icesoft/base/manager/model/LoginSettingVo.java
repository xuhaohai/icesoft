package com.icesoft.base.manager.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoginSettingVo {
    private boolean enableSysCode;
    private boolean enableSmsCode;
    private String successUrl;
}
