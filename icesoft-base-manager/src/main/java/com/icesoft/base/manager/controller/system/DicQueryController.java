package com.icesoft.base.manager.controller.system;

import com.icesoft.base.manager.service.system.DicItemService;
import com.icesoft.core.common.helper.Resp;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 系统数据字典接口
 */
@RestController
@RequestMapping("system/dic")
@AllArgsConstructor
public class DicQueryController {
    private DicItemService dicItemService;

    /**
     * 数据字典查询
     */
    @GetMapping(value = "items")
    public Resp<?> create(@RequestParam String code) {
        return Resp.success(dicItemService.findByDicCode(code));
    }
}
