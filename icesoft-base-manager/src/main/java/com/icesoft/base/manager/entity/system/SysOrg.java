package com.icesoft.base.manager.entity.system;

import com.icesoft.base.manager.suppose.NotUpdatePart;
import com.icesoft.core.dao.base.BaseModel;
import com.icesoft.core.web.suppose.validation.ChineseOrEnglishOrNumber;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Table(name = "SYS_Org", uniqueConstraints = @UniqueConstraint(columnNames = {"pid", "name"}))
@Entity
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Cacheable
public class SysOrg extends BaseModel {

    /**
     * 部门名称
     */
    @Column(nullable = false, length = 50)
    @Length(min = 1, max = 50)
    @ChineseOrEnglishOrNumber
    @NotNull(message = "部门名称不能为空")
    protected String name;
    /**
     * 父id
     */
    @NotUpdatePart
    @Column(nullable = false, updatable = false)
    protected int pid;
    /**
     * 公司id
     */
    @NotUpdatePart
    @Column(nullable = false, updatable = false)
    protected int companyId;

}
