package com.icesoft.base.manager.entity.manager;

import com.icesoft.core.dao.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

@Table(name = "log_RequestLogConfig")
@Entity
@ToString(callSuper = true)
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Cacheable
public class RequestLogConfig extends BaseModel {
    @Enumerated(EnumType.STRING)
    @Column(length = 10)
    private LogLevel logLevel;
    @Column(unique = true, nullable = false)
    @NotEmpty
    private String url;

    public enum LogLevel {
        off, trace, debug, info, warn, error, db;
        private String name;
        @Override
        public String toString() {
            return this.name;
        }
    }
}
