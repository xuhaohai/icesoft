package com.icesoft.base.manager.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
@ConfigurationProperties("icesoft.copyright")
@Data
public class CopyrightProperty {
    private String title = "后台管理系统";
    private String copyright = "新疆乌鲁木齐";
    private String copyrightLinkUrl = "http://www.xxx.com";
    private String copyrightIcp = "icp 2021";

    public Map<String, String> toMap() {
        Map<String, String> map = new HashMap<>();
        map.put("title", title);
        map.put("copyright", copyright);
        map.put("copyrightLinkUrl", copyrightLinkUrl);
        map.put("copyrightIcp", copyrightIcp);
        return map;
    }
}
