package com.icesoft.base.manager.model;

import com.fasterxml.jackson.annotation.*;
import com.icesoft.core.common.helper.tree.BaseTree;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MenuVo implements BaseTree<MenuVo> {
    private int id;

    private int pid;

    private String title;

    private String icon;

    private String href;

    private String target;

    private List<MenuVo> children;

    @Override
    public boolean isParent(MenuVo parent) {
        return parent.getId() == pid;
    }

    @Override
    public List<MenuVo> getChildren() {
        return children;
    }

    @Override
    public void setChildren(List<MenuVo> children) {
        this.children = children;
    }
}
