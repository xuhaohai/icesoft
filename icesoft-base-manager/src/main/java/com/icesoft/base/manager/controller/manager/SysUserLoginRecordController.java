package com.icesoft.base.manager.controller.manager;

import com.icesoft.base.manager.config.UrlConstant;
import com.icesoft.base.manager.entity.system.SysUserLoginRecord;
import com.icesoft.base.manager.helper.PageParam;
import com.icesoft.base.manager.helper.PageResp;
import com.icesoft.base.manager.helper.QueryBuilderUtils;
import com.icesoft.base.manager.service.system.SysUserLoginRecordService;
import com.icesoft.core.dao.criteria.Page;
import com.icesoft.core.dao.criteria.QueryBuilder;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 登录记录
 */
@RestController
@RequestMapping(value = "manager/sysUserLoginRecord")
@AllArgsConstructor
public class SysUserLoginRecordController {
    private SysUserLoginRecordService sysUserLoginRecordService;

    /**
     * 全部登录记录
     *
     * @param queryParam 实体属性查询条件
     * @param search     筛选查询条件
     * @param pageParam  分页查询条件
     * @return PageResp 分页数据
     */
    @GetMapping(UrlConstant.QUERY)
    public PageResp<SysUserLoginRecord> query(HttpServletRequest request, SysUserLoginRecord queryParam, String search, PageParam pageParam) {
        QueryBuilder queryBuilder = QueryBuilder.get();
        QueryBuilderUtils.addRequestParam(queryBuilder, request, queryParam);
        QueryBuilderUtils.addSearchParam(queryBuilder, search, new String[]{"username", "loginIp"});
        Page<SysUserLoginRecord> page = sysUserLoginRecordService.page(queryBuilder, pageParam);
        return new PageResp<>(page);
    }
}
