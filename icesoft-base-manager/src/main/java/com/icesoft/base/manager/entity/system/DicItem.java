package com.icesoft.base.manager.entity.system;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.icesoft.core.dao.base.BaseModel;
import com.icesoft.base.manager.suppose.NotUpdatePart;
import com.icesoft.core.web.suppose.validation.ChineseOrEnglishOrNumber;
import com.icesoft.core.dao.suppose.validation.UniqueCheck;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Table(name = "sys_dicItem", indexes = { @Index(name = "dicIdIndex", columnList = "dicId"),
		@Index(name = "nameIndex", columnList = "name"),
		@Index(name = "dicIdNameIndex", columnList = "dicId,name", unique = true) })
@Entity
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Cacheable
public class DicItem extends BaseModel {
	/** 字典项名 */
	@Column(length = 20, nullable = false)
	@NotNull(message = "字典项不能为空")
	@Size(min = 1, max = 20, message = "字典项太长")
	@ChineseOrEnglishOrNumber
	@UniqueCheck(cascade = "dicId")
	private String name;
	/** 值 */
	@Column(length = 50, nullable = false)
	@NotNull(message = "字典值不能为空")
	@Size(min = 1, max = 50, message = "字典值太长")
	private String value;
	@NotUpdatePart
	private int dicId;

	private int sort;
}
