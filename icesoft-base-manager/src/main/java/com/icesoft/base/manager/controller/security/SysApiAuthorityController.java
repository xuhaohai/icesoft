package com.icesoft.base.manager.controller.security;

import com.icesoft.base.manager.config.UrlConstant;
import com.icesoft.base.manager.entity.security.SysApiAuthority;
import com.icesoft.base.manager.service.security.SysApiAuthorityService;
import com.icesoft.core.common.helper.Resp;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 接口权限管理
 */
@RestController
@RequestMapping("security/simpleAuthority")
@AllArgsConstructor
public class SysApiAuthorityController {

    private SysApiAuthorityService sysApiAuthorityService;

    /**
     * 通过id查询接口权限
     */
    @GetMapping(value = UrlConstant.LOAD)
    public Resp load(int id) {
        return Resp.success(sysApiAuthorityService.load(id));
    }

    /**
     * 修改接口权限
     */
    @PostMapping(value = UrlConstant.UPDATE)
    public Resp update(SysApiAuthority queryParam) {
        SysApiAuthority sysApiAuthority = sysApiAuthorityService.load(queryParam.getId());
        if (sysApiAuthority != null && !sysApiAuthority.getId().equals(queryParam.getId())) {
            return Resp.error("菜单名已存在");
        }
        sysApiAuthorityService.updateNotNullProperties(queryParam);
        return Resp.success();
    }

    /**
     * 创建接口权限
     */
    @PostMapping(value = UrlConstant.CREATE)
    public Resp<Integer> create(@Valid SysApiAuthority sysApiAuthority) {
        sysApiAuthorityService.create(sysApiAuthority);
        return Resp.success(sysApiAuthority.getId());
    }

    /**
     * 删除接口
     */
    @PostMapping(value = UrlConstant.DELETE)
    public Resp<?> delete(int[] ids) {
        sysApiAuthorityService.delete(ids);
        return Resp.success();
    }
}
