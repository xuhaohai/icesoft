package com.icesoft.base.manager.entity.security;

import com.icesoft.core.dao.base.BaseModel;
import com.icesoft.core.dao.suppose.validation.UniqueCheck;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.util.List;

@Table(name = "qx_ApiAuthorityGroup")
@Entity
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Cacheable
public class SysApiAuthorityGroup extends BaseModel {

    @Column(nullable = false, length = 100)
    @NotEmpty(message = "preUrl不能为空")
    @Pattern(regexp = "^/(.+/)*", message = "请求前缀格式错误，正例：/user/、/system/sysuser/")
    private String preUrl;
    @Column(nullable = false, length = 20, unique = true)
    @NotEmpty(message = "分组名不能为空")
    @UniqueCheck(message = "分组名已经存在")
    private String name;
    @Transient
    private List<SysApiAuthority> authorities;

    public SysApiAuthorityGroup() {
    }

    public SysApiAuthorityGroup(String preUrl, String name) {
        this.preUrl = preUrl;
        this.name = name;
    }

    public List<SysApiAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(List<SysApiAuthority> authorities) {
        this.authorities = authorities;
    }

    public String getPreUrl() {
        return preUrl;
    }

    public void setPreUrl(String preUrl) {
        this.preUrl = preUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
