package com.icesoft.base.manager.service.security;

import com.icesoft.base.manager.entity.security.RelSysRoleMenu;
import com.icesoft.core.dao.base.BaseDao;
import com.icesoft.core.dao.criteria.QueryBuilder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RelSysRoleMenuService extends BaseDao<RelSysRoleMenu> {

	public void deleteByRoleId(int roleId) {
		QueryBuilder qb = QueryBuilder.get("roleId", roleId);
		delete(qb);
	}
	public void deleteByMenuId(int menuId) {
		QueryBuilder qb = QueryBuilder.get("menuId", menuId);
		delete(qb);
	}
	public List<RelSysRoleMenu> findByRoleId(int roleId) {
		QueryBuilder qb = QueryBuilder.get("roleId", roleId);
		return find(qb);
	}
}
