package com.icesoft.base.manager.model;

import lombok.Data;

@Data
public class SysApiAuthorityDTO {
    private String groupName;
    private String name;
    private String authority;
}
