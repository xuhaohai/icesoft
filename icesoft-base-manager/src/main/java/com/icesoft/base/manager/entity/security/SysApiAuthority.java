package com.icesoft.base.manager.entity.security;

import com.icesoft.core.dao.base.BaseModel;
import com.icesoft.core.dao.suppose.validation.UniqueCheck;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;

@Table(name = "qx_ApiAuthority", uniqueConstraints = @UniqueConstraint(columnNames = {"groupId", "name"}))
@Entity
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Cacheable
public class SysApiAuthority extends BaseModel {

    @Column(nullable = false, length = 50)
    @UniqueCheck(cascade = "groupId", message = "权限名已存在")
    private String name;
    @Min(1)
    private int groupId;
    @Pattern(regexp = "^/.+", message = "请求路径错误，正例：/user/query、/system/sysuser/test")
    private String authority;

    public SysApiAuthority() {
    }

    public SysApiAuthority(String name, int groupId, String authority) {
        this.name = name;
        this.groupId = groupId;
        this.authority = authority;
    }
}
