package com.icesoft.base.manager.entity.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.icesoft.core.dao.base.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Table(name = "qx_UserRole", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"roleId", "userId"})}, indexes = {
        @Index(name = "idx_UserRole_roleId", columnList = "roleId"),
        @Index(name = "idx_UserRole_userId", columnList = "userId")})
@Entity
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Cacheable
public class RelSysUserRole extends Model {

    @Column(nullable = false)
    @JsonIgnore
    @NotNull
    @Min(1)
    private int roleId; // 角色对象
    @Column(nullable = false)
    @JsonIgnore
    @NotNull
    @Min(1)
    private int userId;
    @Column(length = 40, nullable = false)
    private String username;
    public RelSysUserRole() {
    }

    public RelSysUserRole(@NotNull int roleId, @NotNull int userId,@NotNull String username) {
        this.roleId = roleId;
        this.userId = userId;
        this.username = username;
    }
}
