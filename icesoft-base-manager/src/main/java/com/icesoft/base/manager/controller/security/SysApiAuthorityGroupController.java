package com.icesoft.base.manager.controller.security;

import com.fasterxml.jackson.core.type.TypeReference;
import com.icesoft.base.manager.config.UrlConstant;
import com.icesoft.base.manager.entity.security.SysApiAuthorityGroup;
import com.icesoft.base.manager.helper.PageParam;
import com.icesoft.base.manager.helper.PageResp;
import com.icesoft.base.manager.helper.QueryBuilderUtils;
import com.icesoft.base.manager.model.SysApiAuthorityDTO;
import com.icesoft.base.manager.service.security.SysApiAuthorityGroupService;
import com.icesoft.base.manager.service.security.SysApiAuthorityService;
import com.icesoft.core.common.helper.Resp;
import com.icesoft.core.common.util.JsonUtil;
import com.icesoft.core.dao.criteria.Page;
import com.icesoft.core.dao.criteria.QueryBuilder;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * 权限分组接口
 */
@Slf4j
@RestController
@RequestMapping("security/authorityGroup")
@AllArgsConstructor
public class SysApiAuthorityGroupController {

    private final SysApiAuthorityGroupService authorityGroupService;
    private final SysApiAuthorityService sysApiAuthorityService;

    /**
     * 通过id查询分组
     */
    @GetMapping(value = UrlConstant.LOAD)
    public Resp<SysApiAuthorityGroup> load(int id) {
        return Resp.success(authorityGroupService.load(id));
    }

    /**
     * 修改分组
     */
    @PostMapping(value = UrlConstant.UPDATE)
    public Resp<?> update(SysApiAuthorityGroup queryParam) {
        SysApiAuthorityGroup sysApiAuthorityGroup = authorityGroupService.findByName(queryParam.getName());
        if (sysApiAuthorityGroup != null && !sysApiAuthorityGroup.getId().equals(queryParam.getId())) {
            return Resp.error("角色名已存在");
        }
        authorityGroupService.updateNotNullProperties(queryParam);
        return Resp.success();
    }

    /**
     * 查询所有权限树
     */
    @GetMapping(UrlConstant.QUERY)
    public PageResp<SysApiAuthorityGroup> query(HttpServletRequest request, SysApiAuthorityGroup queryParam, String search, PageParam pageParam) {
        QueryBuilder queryBuilder = QueryBuilder.get();
        QueryBuilderUtils.addRequestParam(queryBuilder, request, queryParam);
        QueryBuilderUtils.addSearchParam(queryBuilder, search, new String[]{"name"});
        Page<SysApiAuthorityGroup> page = authorityGroupService.page(queryBuilder, pageParam);
        List<SysApiAuthorityGroup> list = page.getData();
        list.forEach(group -> group.setAuthorities(sysApiAuthorityService.findByGroupId(group.getId())));
        return new PageResp<>(page);
    }

    /**
     * 创建分组
     */
    @PostMapping(value = UrlConstant.CREATE)
    public Resp<Integer> create(@Valid SysApiAuthorityGroup sysApiAuthorityGroup) {
        authorityGroupService.create(sysApiAuthorityGroup);
        return Resp.success(sysApiAuthorityGroup.getId());
    }

    /**
     * 删除分组
     */
    @PostMapping(value = UrlConstant.DELETE)
    public Resp<?> delete(int[] ids) {
        authorityGroupService.delete(ids);
        return Resp.success();
    }

    /**
     * 指定角色权限分组树
     */
    @RequestMapping("tree")
    public Resp<?> tree(@RequestParam int roleId) {
        return new Resp<>().data(authorityGroupService.tree(roleId));
    }

    /**
     * 查询权限生成树
     */
    @GetMapping(value = "/menuAuthUrlTree")
    public Resp<?> urlTree() {
        return Resp.success(authorityGroupService.urlTree());
    }

    /**
     * 权限生成接口
     */
    @PostMapping(value = "/createByGroup")
    public Resp<Integer> createByGroup(@RequestParam String apiAuthorityJson) {
        List<SysApiAuthorityDTO> list = JsonUtil.toObject(apiAuthorityJson, new TypeReference<List<SysApiAuthorityDTO>>() {
        });
        assert list != null;
        return Resp.success(authorityGroupService.createByGroup(list).size());
    }


}
