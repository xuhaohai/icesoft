package com.icesoft.base.manager.entity.security;

import com.icesoft.core.dao.base.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;

@Table(name = "qx_RoleOrg",
        uniqueConstraints = @UniqueConstraint(columnNames = {"roleId", "orgId"}),
        indexes = {@Index(name = "idx_SysRoleOrg_roleId", columnList = "roleId")}
)
@Entity
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Cacheable
public class RelSysRoleOrg extends Model {
    private int roleId;
    private int orgId;
}
