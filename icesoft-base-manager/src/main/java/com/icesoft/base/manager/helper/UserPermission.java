package com.icesoft.base.manager.helper;

import com.icesoft.base.manager.entity.security.SysRole;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

@AllArgsConstructor
public class UserPermission implements Serializable {
    @Getter
    private List<SysRole> sysRoleList;
    @Getter
    private Set<Integer> orgIds;
}
