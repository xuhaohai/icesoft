package com.icesoft.base.manager.dao;

import java.util.List;

public interface IDataScopeFilter {
    default boolean suppose() {
        return true;
    }

    default void scopeAll() {

    }

    void scopeCustom(List<Integer> customRoleIds);

    void scopeOrgOwnerCustom();

    void scopeOwnChildOne();

    void scopeOwnLevel();

    void scopeOwnCompany();

    void scopeOwn();
}
