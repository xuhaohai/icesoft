package com.icesoft.base.manager.service.system;

import com.icesoft.base.manager.base.BaseService;
import com.icesoft.base.manager.entity.system.Dic;
import com.icesoft.base.manager.entity.system.DicItem;
import com.icesoft.core.dao.criteria.QueryBuilder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DicItemService extends BaseService<DicItem> {
	@Override
    public void delete(QueryBuilder qb) {
		delete(qb.getFromBuilder());
	}

	public DicItem findByDicCodeAndName(String code, String name) {
		return unique(QueryBuilder.get("name", name).joinOn(Dic.class, "dic", "id", "dicId").andEq("dic.code", code));
	}

	public List<DicItem> findByDicCode(String code) {
		return find(QueryBuilder.get().joinOn(Dic.class, "dic", "id", "dicId").andEq("dic.code", code));
	}

	public List<DicItem> findByDicId(int dicId) {
		return find(QueryBuilder.get("dicId", dicId));
	}

}
