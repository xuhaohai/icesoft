package com.icesoft.base.manager.config;

import com.icesoft.core.web.suppose.setting.BasePropertySetting;
import com.icesoft.core.web.suppose.setting.BaseSettingEnum;
import org.springframework.stereotype.Component;

@Component
public class SystemPropertySetting extends BasePropertySetting<SystemPropertySetting.SettingEnum>{
    private static final String SYSTEM_SETTING_GROUP_NAME = "系统配置";

    @Override
    public String getGroupName() {
        return SYSTEM_SETTING_GROUP_NAME;
    }

    public String getHomePageUrl() {
        return getProperty(SettingEnum.system_homePageUrl);
    }

    public enum SettingEnum implements BaseSettingEnum {
        /**
         * 后台首页路径
         */
        system_homePageUrl("首页路径", "system/common/home/home.html");

        private String des;
        private String defaultValue;

        SettingEnum(String des, String defaultValue) {
            this.des = des;
            this.defaultValue = defaultValue;
        }

        @Override
        public String getDes() {
            return this.des;
        }

        @Override
        public String getDefaultValue() {
            return this.defaultValue;
        }
    }
}
