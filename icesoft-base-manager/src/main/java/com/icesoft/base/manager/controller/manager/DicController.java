package com.icesoft.base.manager.controller.manager;

import com.icesoft.base.manager.config.UrlConstant;
import com.icesoft.base.manager.entity.system.Dic;
import com.icesoft.base.manager.helper.PageParam;
import com.icesoft.base.manager.helper.PageResp;
import com.icesoft.base.manager.helper.QueryBuilderUtils;
import com.icesoft.base.manager.service.system.DicService;
import com.icesoft.core.common.helper.Resp;
import com.icesoft.core.dao.criteria.Page;
import com.icesoft.core.dao.criteria.QueryBuilder;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * 数据字典管理
 * */
@RestController
@RequestMapping("manager/dic")
@AllArgsConstructor
public class DicController {
    private DicService dicService;

    /**
     * 获取编辑数据
     *
     * @param id
     * @return Resp
     * @Description
     */
    @GetMapping(value = UrlConstant.LOAD)
    public Resp load(int id) {
        return Resp.success(dicService.load(id));
    }

    /**
     * 新增
     *
     * @param dic
     * @param request
     * @return Resp<?>
     * @Description
     */
    @PostMapping(value = UrlConstant.CREATE)
    public Resp<?> create(@Valid Dic dic, HttpServletRequest request) {
        dicService.create(dic);
        return Resp.success(dic.getId());
    }

    /**
     * 删除数据字典，级联删除对应数据字典项
     *
     * @param ids 数据字典id
     * @return Resp<?>
     */
    @PostMapping(value = UrlConstant.DELETE)
    public Resp<?> delete(Integer[] ids) {
        if (ids.length != 1) {
            return new Resp<>("只能单个删除");
        }
        // 做了级联删除
        return dicService.deleteItem(ids[0]);
    }

    /**
     * 更新非空字段
     *
     * @param request
     * @param dic     void
     * @Description
     */
    @PostMapping(value = UrlConstant.UPDATE)
    public Resp<?> update(HttpServletRequest request, Dic dic) {
        dicService.updateNotNullProperties(dic);
        return Resp.success();
    }

    /**
     * 查询
     *
     * @param request
     * @param queryParam 实体属性查询条件
     * @param search     筛选查询条件
     * @param pageParam  分页查询条件
     * @return PageResp 分页数据
     * @Description
     */
    @GetMapping(UrlConstant.QUERY)
    public PageResp query(HttpServletRequest request, Dic queryParam, String search, PageParam pageParam) {
        QueryBuilder queryBuilder = QueryBuilder.get();
        QueryBuilderUtils.addRequestParam(queryBuilder, request, queryParam);
        QueryBuilderUtils.addSearchParam(queryBuilder, search, new String[]{"name", "code", "dicId"});
        Page<Dic> page = dicService.page(queryBuilder, pageParam);
        return new PageResp<>(page);
    }

    /**
     * code检验
     *
     * @param id   id
     * @param code code
     * @return ResponseEntity<?>
     * @Description
     */
    @GetMapping("validCode")
    public Resp<Boolean> validCode(@RequestParam int id, @RequestParam String code) {
        Dic dic = dicService.findByCode(code);
        if (dic == null || dic.getId() == id) {
            return Resp.success(true);
        }
        return Resp.success(false);
    }

}
