package com.icesoft.base.manager.config.log;

import com.icesoft.base.manager.entity.manager.RequestLog;
import com.icesoft.base.manager.helper.AuthUser;
import com.icesoft.base.manager.helper.SecurityUtils;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class UserDTOBindRequestLogInfo extends BindRequestLogInfo {

    @Override
    public void editRequestLog(HttpServletRequest request, RequestLog requestLog) {
        AuthUser user = SecurityUtils.getUser();
        if (user != null) {
            requestLog.setUserId(user.getId());
            requestLog.setUserType(user.getClass().getSimpleName());
            requestLog.setUserName(user.getName());
        }
    }

}
