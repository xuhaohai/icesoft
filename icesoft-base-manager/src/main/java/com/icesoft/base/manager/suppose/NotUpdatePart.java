package com.icesoft.base.manager.suppose;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;


/**
 * 标记不进行通用更新的字段
 * */
@Target({FIELD})  
@Retention(RUNTIME)  
@Documented
public @interface NotUpdatePart {
}
