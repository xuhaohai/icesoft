package com.icesoft.base.manager.security.suppose;

import java.util.Set;

public interface IWhiteIpService {

	Set<String> getWhiteIps();

}
