package com.icesoft.base.manager.config.listener;

import com.icesoft.base.manager.service.system.DicItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.icesoft.core.dao.suppose.BaseModelListener;
import com.icesoft.core.dao.criteria.QueryBuilder;
import com.icesoft.base.manager.entity.system.Dic;

@Component
public class DicDbListener extends BaseModelListener<Dic> {
	@Autowired
	private DicItemService dicItemService;

	@Override
	public void preRemove(Dic model) {
		dicItemService.delete(QueryBuilder.get().andEq("dicId", model.getId()));
	}

}
