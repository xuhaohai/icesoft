package com.icesoft.base.manager.config.listener;

import com.icesoft.base.manager.service.manager.RequestLogConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Component;

import com.icesoft.core.dao.suppose.BaseModelListener;
import com.icesoft.base.manager.entity.manager.RequestLogConfig;

@Component
public class RequestMappingLogInfoDbListener extends BaseModelListener<RequestLogConfig> {

	@Autowired
	RequestLogConfigService requestLogConfigService;

	@Override
	@CacheEvict(value = RequestLogConfigService.CACHE_GROUP, key = "#model.url")
	public void postUpdate(RequestLogConfig model) {
	}

	@Override
	@CacheEvict(value = RequestLogConfigService.CACHE_GROUP, key = "#model.url")
	public void postRemove(RequestLogConfig model) {
	}

}
