package com.icesoft.base.manager.security.suppose;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.Authentication;

public interface IAccessRule {

	String urlPattern();

	boolean hasPermission(HttpServletRequest request, Authentication authentication);
}
