package com.icesoft.base.manager.entity.security;

import com.icesoft.core.dao.base.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;

@Table(name = "qx_RoleMenu", uniqueConstraints = @UniqueConstraint(columnNames = { "roleId", "menuId" }), indexes = {
		@Index(name = "idx_RoleMenu_roleId", columnList = "roleId"), @Index(name = "idx_RoleMenu_menuId", columnList = "menuId") })
@Entity
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Cacheable
public class RelSysRoleMenu extends Model {

	@Column(length = 40, nullable = false)
	private int roleId;

	@Column(length = 40, nullable = false)
	private int menuId;
}
