package com.icesoft.base.manager.entity.security;

import com.icesoft.core.dao.base.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;

@Table(name = "qx_RoleAuthority", uniqueConstraints = @UniqueConstraint(columnNames = { "roleId",
		"authorityId" }), indexes = { @Index(name = "idx_RoleAuthority_roleId", columnList = "roleId"),
				@Index(name = "idx_RoleAuthority_authorityId", columnList = "authorityId") })
@Entity
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Cacheable
public class RelSysRoleAuthority extends Model {

	@Column(length = 40, nullable = false)
	private int authorityId;

	@Column(length = 40, nullable = false)
	private int roleId;

}
