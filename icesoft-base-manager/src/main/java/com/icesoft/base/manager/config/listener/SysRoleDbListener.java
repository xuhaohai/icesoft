package com.icesoft.base.manager.config.listener;

import com.icesoft.base.manager.entity.security.SysRole;
import com.icesoft.base.manager.service.security.RelSysRoleAuthorityService;
import com.icesoft.base.manager.service.security.RelSysRoleMenuService;
import com.icesoft.base.manager.service.security.RelSysRoleOrgService;
import com.icesoft.base.manager.service.security.RelSysUserRoleService;
import com.icesoft.core.dao.suppose.BaseModelListener;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class SysRoleDbListener extends BaseModelListener<SysRole> {
    private RelSysUserRoleService relSysUserRoleService;
    private RelSysRoleAuthorityService relSysRoleAuthorityService;
    private RelSysRoleMenuService relSysRoleMenuService;
    private RelSysRoleOrgService relSysRoleOrgService;

    @Override
    public void preRemove(SysRole model) {
        relSysUserRoleService.deleteByRoleId(model.getId());
        relSysRoleAuthorityService.deleteByRoleId(model.getId());
        relSysRoleMenuService.deleteByRoleId(model.getId());
        relSysRoleOrgService.deleteRoleRelation(model.getId());
    }
}
