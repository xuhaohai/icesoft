package com.icesoft.base.manager.config.listener;

import com.icesoft.base.manager.entity.security.SysMenu;
import com.icesoft.base.manager.service.security.RelSysRoleMenuService;
import com.icesoft.core.dao.suppose.BaseModelListener;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@AllArgsConstructor
@Component
public class SysMenuDbListener extends BaseModelListener<SysMenu> {
    private RelSysRoleMenuService relSysRoleMenuService;

    @Override
    public void preRemove(SysMenu model) {
        relSysRoleMenuService.deleteByMenuId(model.getId());
    }
}
