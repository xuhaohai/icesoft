package com.icesoft.base.manager.base;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.icesoft.base.manager.suppose.NotUpdatePart;
import com.icesoft.core.dao.base.BaseModel;
import com.icesoft.core.dao.suppose.validation.UniqueCheck;
import com.icesoft.core.web.suppose.validation.Phone;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

/**
 * 用户父类
 *
 * @author XHH
 * @date 2021/1/28 17:02
 */
@MappedSuperclass
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public abstract class BaseUser extends BaseModel {
    protected String headImgUrl;
    @Email
    @Column(length = 50)
    protected String email;
    @NotUpdatePart
    protected int orgId;
    @Column(unique = true, nullable = false, length = 30, updatable = false)
    @Pattern(regexp = "[A-Za-z0-9]{3,30}", message = "账号只能包含字母和数字，长度只允许在3至30位")
    @UniqueCheck(message = "账号已存在")
    @NotUpdatePart
    protected String username; // 用户名
    @Column(unique = true, length = 11)
    @UniqueCheck(message = "手机号已存在")
    @Phone
    protected String phone;
    @Column(nullable = false, length = 128)
    @NotEmpty(message = "密码不能为空")
    @NotUpdatePart
    @JsonIgnore
    protected String password; // 密码
    @Column(length = 20)
    @Length(min = 1, max = 20, message = "姓名长度不能超过20")
    protected String name; // 姓名
    @NotUpdatePart
    protected int companyId;
    @Column(unique = true, length = 40)
    @NotUpdatePart
    protected String userToken;
    @Column(length = 10)
    protected String sex;
}
