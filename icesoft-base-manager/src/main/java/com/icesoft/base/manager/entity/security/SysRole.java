package com.icesoft.base.manager.entity.security;

import com.icesoft.base.manager.helper.DataScopeEnum;
import com.icesoft.core.dao.base.BaseModel;
import com.icesoft.core.dao.suppose.validation.UniqueCheck;
import com.icesoft.core.web.suppose.validation.ChineseOrEnglishOrNumber;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Table(name = "qx_Role")
@Entity
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Cacheable
/**
 * 角色信息
 * @apiNote 角色信息
 * */
public class SysRole extends BaseModel {

    @Column(nullable = false, length = 15, unique = true)
    @Length(max = 15, message = "角色名长度不能超过15")
    @UniqueCheck(message = "角色名已存在")
    @ChineseOrEnglishOrNumber
    /**
     * 角色名
     * */
    private String name;
    /**
     * 数据权限
     */
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private DataScopeEnum dataScopeEnum = DataScopeEnum.OWN_CHILD_ONE;
    /**
     * 备注
     */
    @Column(length = 50)
    private String remarks;

}
