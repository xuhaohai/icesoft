package com.icesoft.base.manager.entity.manager;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.icesoft.core.dao.base.BaseModel;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Table(name = "log_RequestLog")
@Entity
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class RequestLog extends BaseModel {

    private int userId;
    private String userName;

    private String userType;
    private String address;

    private String requestURL;
    @Column(length = 4096)
    private String parameter;
    private String method;
    @Column(length = 4096)
    private String result;

    private long executeTimeMillis;

    public void setRequestLog(String address, String requestURL, String parameter, String method) {
        this.address = address;
        this.requestURL = requestURL;
        this.parameter = parameter;
        this.method = method;
    }

}
