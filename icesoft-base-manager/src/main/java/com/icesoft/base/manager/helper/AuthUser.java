package com.icesoft.base.manager.helper;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

/**
 * @author XHH
 */
public class AuthUser extends User implements Serializable {
    @Getter
    private int id;
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private String headImgUrl;
    @Getter
    private int orgId;
    @Getter
    private int companyId;
    @Getter
    private Date loginTime;
    @Getter
    @Setter
    @JsonIgnore
    private UserPermission permission;

    public AuthUser(int id, String name, int orgId, int companyId, String username, String password
            , Collection<? extends GrantedAuthority> authorities) {
        this(id, name, orgId, companyId, username, password, true, true, true, true, authorities);
    }

    public AuthUser(int id, String name, int orgId, int companyId, String username, String password,
                    boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked
            , Collection<? extends GrantedAuthority> authorities) {
        super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
        this.id = id;
        this.name = name;
        this.orgId = orgId;
        this.companyId = companyId;
        this.loginTime = new Date();
    }

}