package com.icesoft.base.manager.security.config;

import com.icesoft.base.manager.model.LoginSettingVo;
import com.icesoft.core.web.suppose.setting.BasePropertySetting;
import com.icesoft.core.web.suppose.setting.BaseSettingEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class LoginPropertySetting extends BasePropertySetting<LoginPropertySetting.SettingEnum> {
    public static final String LOGIN_SETTING_GROUP_NAME = "登录配置";

    public String getSuccessUrl() {
        return getProperty(SettingEnum.login_successUrl);
    }

    @Override
    public String getGroupName() {
        return LOGIN_SETTING_GROUP_NAME;
    }

    public boolean isEnableLoginCode() {
        return !"false".equals(getProperty(SettingEnum.login_code));
    }

    public boolean isEnableSmsCode() {
        return "true".equals(getProperty(SettingEnum.login_smsCode));
    }

    public boolean isEnablePasswordExpired() {
        return "true".equals(getProperty(SettingEnum.login_passwordExpired));
    }

    public int getPasswordExpiredDayCount() {
        return getIntProperty(SettingEnum.login_passwordExpiredDayCount);
    }

    public int getMaxLimitCount() {
        return getIntProperty(SettingEnum.login_maxLimitCount);
    }
    public int geMaxLimitCountTimeMinute() {
        return getIntProperty(SettingEnum.login_maxLimitCountTimeMinute);
    }
    public int geLimitIntervalMinute() {
        return getIntProperty(SettingEnum.login_limitIntervalTimeMinute);
    }

    public int getMaxLockCount() {
        return getIntProperty(SettingEnum.login_maxLockCount);
    }

    public int getMaxLockCountTimeMinute() {
        return getIntProperty(SettingEnum.login_maxLockCountTimeMinute);
    }

    public String[] getBasicAuthWhiteIps() {
        return StringUtils.split(getProperty(SettingEnum.login_basicAuthWhiteIps), ",");
    }

    public LoginSettingVo getLoginSetting() {
        return new LoginSettingVo(isEnableLoginCode(), isEnableSmsCode(), getSuccessUrl());
    }

    public enum SettingEnum implements BaseSettingEnum {

        login_basicAuthWhiteIps("允许访问endpoint端口的ip白名单", "127.0.0.1"),

        login_code("登录验证码", "true"),
        login_smsCode("是否开启短信登录", "true"),

        login_successUrl("登录成功后跳转页面", "system/index.html"),

        login_passwordExpired("密码过期校验", "false"),
        login_passwordExpiredDayCount("密码过期天数，单位：天", "30"),

        login_maxLimitCount("登录错误次数，限制登录", "5"),
        login_maxLimitCountTimeMinute("登录达到限制次数的时间，单位：分钟", "10"),

        login_limitIntervalTimeMinute("限制登录间隔时间，单位：分钟", "2"),

        login_maxLockCount("登录错误次数，锁定账号", "10"),
        login_maxLockCountTimeMinute("登录达到锁定次数的时间，单位：分钟", "30");

        private final String des;
        private final String defaultValue;

        SettingEnum(String des, String defaultValue) {
            this.des = des;
            this.defaultValue = defaultValue;
        }

        @Override
        public String getDes() {
            return this.des;
        }

        @Override
        public String getDefaultValue() {
            return this.defaultValue;
        }
    }
}
