package com.icesoft.base.manager.security.config;

import com.icesoft.core.web.helper.PathUtil;
import lombok.Data;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


/**
 * 安全相关配置
 */
@Configuration
@ConfigurationProperties(prefix = "icesoft.security")
@Data
public class SecurityProperty implements InitializingBean {
    /**
     * 是否启用单一登录
     */
    private boolean singleLogin = false;
    /**
     * 开放url路径，逗号分隔
     */
    private String ignoreUrls = "";
    /**
     * 登录页面路径
     */
    private String loginPage = "/system/login.html";
    /**
     * 默认开放路径
     */
    private String defaultIgnoreUrls = "/uploadFiles/**,/public/**,/api/**";
    /**
     * 跨域请求域名
     */
    private String corsUrl;

    @Override
    public void afterPropertiesSet() throws Exception {
        PathUtil.appendPathStart(loginPage);
    }
}
