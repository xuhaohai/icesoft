package com.icesoft.base.manager.entity.manager;

import com.icesoft.core.dao.base.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;

@Table(name = "sys_OrgOwner", uniqueConstraints = @UniqueConstraint(columnNames = {"orgId",
        "ownerOrgId"}), indexes = {@Index(name = "orgIdIndex", columnList = "orgId"),
        @Index(name = "ownerOrgIdIndex", columnList = "ownerOrgId")})
@Entity
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@FilterDef(name = "orderOwnerFilter", parameters = {@ParamDef(name = "ownerIds", type = "long")})
@Filters({@Filter(name = "orderOwnerFiler", condition = "ownder in (:ownerIds)")})
@Cacheable
public class RelSysOrgOwner extends Model {
    private int orgId;
    private int ownerOrgId;
}
