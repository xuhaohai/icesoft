package com.icesoft.base.manager.service.security;

import com.icesoft.base.manager.entity.security.RelSysRoleAuthority;
import com.icesoft.core.dao.base.BaseDao;
import com.icesoft.core.dao.criteria.QueryBuilder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RelSysRoleAuthorityService extends BaseDao<RelSysRoleAuthority> {

    public List<RelSysRoleAuthority> findByRoleId(Integer roleId) {
        if (roleId == null || roleId == 0) {
            return new ArrayList<>();
        }

        return find(QueryBuilder.get("roleId", roleId));
    }

    public void deleteByAuthorityId(int authorityId) {
        delete(QueryBuilder.get("authorityId", authorityId));
    }

    public void deleteByRoleId(int roleId) {
        delete(QueryBuilder.get("roleId", roleId));
    }
}
