package com.icesoft.base.manager.controller.upload;

import com.icesoft.base.manager.helper.SecurityUtils;
import com.icesoft.core.common.helper.Resp;
import com.icesoft.core.web.helper.PathUtil;
import com.icesoft.core.web.suppose.upload.base.BaseFileUploadTemplate;
import com.icesoft.core.web.suppose.upload.base.BaseUploadPathConfig;
import com.icesoft.core.web.suppose.upload.base.ChunkFileDTO;
import com.icesoft.core.web.suppose.upload.config.FileUploadPropertySetting;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Base64;

/**
 * 文件上传接口
 */
@RestController
@RequestMapping("/system/common/file/upload/")
public class FileUploadController extends BaseFileUploadTemplate {

    private FileUploadPropertySetting fileUploadPropertySetting;
    private BaseUploadPathConfig uploadPathConfig;

    public FileUploadController(FileUploadPropertySetting fileUploadPropertySetting, ObjectProvider<BaseUploadPathConfig> baseUploadPathConfigObjectProvider) {
        this.fileUploadPropertySetting = fileUploadPropertySetting;
        this.uploadPathConfig = baseUploadPathConfigObjectProvider.getIfAvailable(() -> new BaseUploadPathConfig() {
        });
    }

    /**
     * 文件base64上传
     */
    @PostMapping("base64File")
    public Resp<String> base64File(HttpServletRequest request, @RequestParam String base64File,
                                   @Valid ChunkFileDTO chunkFileDTO) {
        if (notUpload(request, chunkFileDTO.getFileName())) {
            return Resp.error("非法文件");
        }
        log.trace("base64File:{}", base64File);
        byte[] fileBytes = Base64.getMimeDecoder().decode(base64File);
        String filePath = writeToFile(chunkFileDTO, fileBytes);
        return Resp.success(PathUtil.getNetworkPath(filePath));
    }

    /**
     * 文件流上传
     */
    @PostMapping("multipartFile")
    public Resp<String> upload(HttpServletRequest request, @RequestParam MultipartFile file,
                               @Valid ChunkFileDTO chunkFileDTO) throws IOException {
        if (notUpload(request, chunkFileDTO.getFileName())) {
            return Resp.error("非法文件");
        }
        String filePath = writeToFile(chunkFileDTO, file.getInputStream());
        return Resp.success(PathUtil.getNetworkPath(filePath));
    }

    private boolean notUpload(HttpServletRequest request, String fileName) {
        if (SecurityUtils.isSuperAdmin()) {
            return false;
        }
        String referrer = request.getHeader("Referer");
        if (referrer == null) {
            return true;
        }
        return !fileUploadPropertySetting.isWhiteExt(referrer, fileName);
    }


    @Override
    protected BaseUploadPathConfig getPathConfig() {
        return uploadPathConfig;
    }
}
