package com.icesoft.test.suppose;

import com.fasterxml.jackson.core.type.TypeReference;
import com.github.jsonzou.jmockdata.JMockData;
import com.icesoft.base.manager.helper.AuthUser;
import com.icesoft.core.common.helper.Resp;
import com.icesoft.core.common.helper.RespCode;
import com.icesoft.mock.builder.MockBuilder;
import com.icesoft.mock.service.MockService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class SysSettingControllerTest {
	@Autowired
	private MockService mockService;

	@Test
	public void test() {
		AuthUser user = JMockData.mock(AuthUser.class);
		Resp<List<String>> resp = mockService.http(MockBuilder.httpGet("/system/setting/findGroups"),
				new TypeReference<Resp<List<String>>>() {
				});
		Assertions.assertEquals(RespCode.SUCCESS, resp.getCode());
	}
}
