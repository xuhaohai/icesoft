package com.icesoft.test.start;

import com.icesoft.Application;

/**
 * @author XHH
 */
public class ManagerApplication {

	public static void main(String[] args) {
		System.setProperty("spring.config.location","classpath:application.properties");
		Application.main(args);
	}
}
