//package com.icesoft.base.manager.service.security;
//
//import com.github.jsonzou.jmockdata.JMockData;
//import com.icesoft.base.manager.entity.security.AuthorityGroup;
//import com.icesoft.base.manager.entity.security.Role;
//import com.icesoft.base.manager.entity.security.RoleAuthority;
//import com.icesoft.base.manager.entity.security.SimpleAuthority;
//import com.icesoft.base.manager.security.config.RoleEnum;
//import com.icesoft.core.common.helper.TreeBean;
//import org.junit.After;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.security.test.context.support.WithMockUser;
//import org.springframework.test.annotation.Rollback;
//import org.springframework.test.context.ActiveProfiles;
//import org.springframework.test.context.junit4.SpringRunner;
//
//import javax.transaction.Transactional;
//
//import java.util.List;
//
//import static org.hamcrest.CoreMatchers.*;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest
//@AutoConfigureMockMvc
//@WithMockUser(roles = RoleEnum.SUPER_ADMIN)
//@ActiveProfiles("test")
//@Transactional
//public class AuthorityGroupServiceTest {
//
//    @Autowired
//    private AuthorityGroupService authorityGroupService;
//    @Autowired
//    private RoleAuthorityService roleAuthorityService;
//    @Autowired
//    private RoleService roleService;
//    @Autowired
//    private SimpleAuthorityService simpleAuthorityService;
//
//    private AuthorityGroup authorityGroup;
//    private Role role;
//    private RoleAuthority roleAuthority;
//
//    @Before
//    public void setUp() throws Exception {
//        authorityGroup = JMockData.mock(AuthorityGroup.class);
//        authorityGroupService.create(authorityGroup);
//        role = new Role();
//        role.setName("默认角色");
//        roleService.create(role);
//        roleAuthority = new RoleAuthority();
//        roleAuthority.setRoleId(role.getId());
//        roleAuthority.setAuthorityId(authorityGroup.getId());
//        roleAuthorityService.create(roleAuthority);
//        SimpleAuthority query = new SimpleAuthority("查询", authorityGroup.getId(), "/security/role/query");
//        simpleAuthorityService.create(query);
//        SimpleAuthority update = new SimpleAuthority("修改", authorityGroup.getId(), "/security/role/update");
//        simpleAuthorityService.create(update);
//        SimpleAuthority create = new SimpleAuthority("创建", authorityGroup.getId(), "/security/role/create");
//        simpleAuthorityService.create(create);
//        SimpleAuthority delete = new SimpleAuthority("删除", authorityGroup.getId(), "/security/role/delete");
//        simpleAuthorityService.create(delete);
//    }
//
//    @After
//    public void tearDown() throws Exception {
//    }
//
//    @Test
//    @Rollback
//    public void findByName() {
//        AuthorityGroup authorityGroup = authorityGroupService.findByName("系统角色");
//        Assert.assertThat(authorityGroup.getPreUrl(), is("/security/role/"));
//    }
//
//    @Test
//    @Rollback
//    public void tree() {
//        List<TreeBean> treeBeans = authorityGroupService.tree(role.getId());
//        Assert.assertThat(treeBeans, anything());
//        System.out.println("------------treeBeans:" + treeBeans);
//    }
//
//    @Test
//    @Rollback
//    public void urlTree() {
//    }
//
//    @Test
//    @Rollback
//    public void createByGroup() {
//    }
//}
