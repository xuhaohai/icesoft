package com.icesoft.core.dao.criteria;

public enum Sequence {
    DESC("desc"), ASC("asc"), DESC_NULL_LAST("desc nulls last"), ASC_NULL_LAST("asc nulls last"), DESC_NULL_FIRST("desc nulls first"), ASC_NULL_FIRST("asc nulls first");

    Sequence(String value) {
        this.value = value;
    }

    private String value;

    public String getValue() {
        return this.value;
    }

    public static Sequence get(String op) {
        return get(op, null);
    }

    public static Sequence get(String op, Sequence defaultOperator) {
        try {
            return Sequence.valueOf(op);
        } catch (IllegalArgumentException e) {
            return defaultOperator;
        }
    }
}