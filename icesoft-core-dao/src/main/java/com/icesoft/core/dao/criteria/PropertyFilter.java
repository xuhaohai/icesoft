package com.icesoft.core.dao.criteria;

import java.util.Collection;

public class PropertyFilter extends AndPropertyFilter {
    PropertyFilter(FromBuilder fromBuilder) {
        super(fromBuilder);
    }

    @Override
    public PropertyFilter andEq(String name, Object value) {
        return (PropertyFilter) super.andEq(name, value);
    }

    @Override
    public PropertyFilter copy(FromBuilder fromBuilder) {
        PropertyFilter propertyFilter = new PropertyFilter(fromBuilder);
        propertyFilter.propertys.addAll(propertys);
        return propertyFilter;
    }

    @Override
    public PropertyFilter andGe(String name, Object value) {
        return (PropertyFilter) super.andGe(name, value);
    }

    @Override
    public PropertyFilter andLe(String name, Object value) {
        return (PropertyFilter) super.andLe(name, value);
    }

    @Override
    public PropertyFilter andNe(String name, Object value) {
        return (PropertyFilter) super.andNe(name, value);
    }

    @Override
    public PropertyFilter andGt(String name, Object value) {
        return (PropertyFilter) super.andGt(name, value);
    }

    @Override
    public PropertyFilter andLt(String name, Object value) {
        return (PropertyFilter) super.andLt(name, value);
    }

    @Override
    public PropertyFilter andIn(String name, SelectBuilder selectBuilder) {
        return (PropertyFilter) super.andIn(name, selectBuilder);
    }

    @Override
    public PropertyFilter andNotIn(String name, SelectBuilder selectBuilder) {
        return (PropertyFilter) super.andNotIn(name, selectBuilder);
    }

    @Override
    public PropertyFilter andIn(String name, Collection<?> value) {
        return (PropertyFilter) super.andIn(name, value);
    }

    @Override
    public PropertyFilter andNotIn(String name, Collection<?> value) {
        return (PropertyFilter) super.andNotIn(name, value);
    }

    @Override
    public PropertyFilter andLike(String name, Object value) {
        return (PropertyFilter) super.andLike(name, value);
    }

    @Override
    public PropertyFilter andIsNotNull(String name) {
        return (PropertyFilter) super.andIsNotNull(name);
    }

    @Override
    public PropertyFilter andIsNull(String name) {
        return (PropertyFilter) super.andIsNull(name);
    }

    // or查询
    public PropertyFilter orEq(String name, Object value) {
        return or(name, Operator.EQ, value);
    }

    public PropertyFilter orGe(String name, Object value) {
        return or(name, Operator.GE, value);
    }

    public PropertyFilter orLe(String name, Object value) {
        return or(name, Operator.LE, value);
    }

    public PropertyFilter orNe(String name, Object value) {
        return or(name, Operator.NE, value);
    }

    public PropertyFilter orGt(String name, Object value) {
        return or(name, Operator.GT, value);
    }

    public PropertyFilter orLt(String name, Object value) {
        return or(name, Operator.LT, value);
    }

    public PropertyFilter orIn(String name, SelectBuilder selectBuilder) {
        return or(name, Operator.IN, selectBuilder);
    }

    public PropertyFilter orNotIn(String name, SelectBuilder selectBuilder) {
        return or(name, Operator.NOT_IN, selectBuilder);
    }

    public PropertyFilter orIn(String name, Collection<?> value) {
        if (value == null || value.isEmpty()) {
            return (PropertyFilter) add(Criteria.OR, "1", Operator.EQ, "0", true);
        }
        if (value.size() == 1) {
            return or(name, Operator.EQ, value.iterator().next());
        }
        return or(name, Operator.IN, value);
    }

    public PropertyFilter orNotIn(String name, Collection<?> value) {
        if (value == null || value.isEmpty()) {
            return (PropertyFilter) add(Criteria.OR, "1", Operator.EQ, "1", true);
        }
        if (value.size() == 1) {
            return or(name, Operator.NE, value.iterator().next());
        }
        return or(name, Operator.NOT_IN, value);
    }

    public PropertyFilter orLike(String name, Object value) {
        return or(name, Operator.LIKE, value);
    }

    public PropertyFilter orIsNotNull(String name) {
        return or(name, Operator.IS, "not null");
    }

    public PropertyFilter orIsNull(String name) {
        return or(name, Operator.IS, "null");
    }


    PropertyFilter or(String name, Operator operator, Object value) {
        return (PropertyFilter) add(Criteria.OR, name, operator, value);
    }

}
