package com.icesoft.core.dao.suppose.validation;


import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.RUNTIME;


@Target({FIELD})
@Retention(RUNTIME)
//指定验证器
@Documented
public @interface UniqueCheck {
    //默认错误消息
    String message() default "数据已存在";

    //级联字段
    String[] cascade() default {};
}
