package com.icesoft.core.dao.suppose;

import com.icesoft.core.dao.base.BaseModel;
import com.icesoft.core.dao.base.RawType;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation=Propagation.REQUIRES_NEW)
public abstract class BaseModelListener<T extends BaseModel> extends RawType<T> {

	public void prePersist(T model) {
	}

	public void postPersist(T model) {
	}

	/**
	 * 通过jpql语句删除的将不会触发
	 */
	public void preRemove(T model) {
	}

	/**
	 * 通过jpql语句删除的将不会触发
	 */
	public void postRemove(T model) {
	}

	public void preUpdate(T model) {
	}

	public void postUpdate(T model) {
	}

}
