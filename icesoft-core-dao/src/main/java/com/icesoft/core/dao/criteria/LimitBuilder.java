package com.icesoft.core.dao.criteria;

public class LimitBuilder extends BaseBuilder {
    LimitBuilder copy(FromBuilder fromBuilder) {
        LimitBuilder builder = new LimitBuilder(fromBuilder);
        builder.offset = offset;
        builder.limit = limit;
        return builder;
    }

    public LimitBuilder limit(int limit) {
        this.limit = limit;
        return this;
    }

    public LimitBuilder offset(int offset) {
        this.offset = offset;
        return this;
    }

    public LimitBuilder page(int page, int size) {
        if (page == 0) {
            return this;
        }
        offset = (page - 1) * size;
        limit = size;
        return this;
    }

    public int getOffset() {
        return offset;
    }

    public int getLimit() {
        return limit;
    }

    int limit = 0;
    int offset = 0;

    LimitBuilder(FromBuilder fromBuilder) {
        super(fromBuilder);
    }

    @Override
    public String toString() {
        return (limit > 0 ? "limit " + limit : "") + (offset > 0 ? " offset " + offset : "");
    }

}
