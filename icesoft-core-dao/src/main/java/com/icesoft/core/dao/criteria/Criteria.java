package com.icesoft.core.dao.criteria;

public enum Criteria {
	AND("and"),OR("or");
	private Criteria(String symbol){
		this.symbol=symbol;
	}
	
	private String symbol;
	
	public String getSymbol() {
		return symbol;
	}
}