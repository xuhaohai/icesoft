package com.icesoft.core.dao.cache;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.env.EnvironmentPostProcessor;
import org.springframework.boot.env.PropertiesPropertySourceLoader;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.PropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public class HibernateCacheEnvironmentPostProcessor implements EnvironmentPostProcessor {
	private static final String hibernatePath = "config/hibernate-cache.properties";

	@Override
	public void postProcessEnvironment(ConfigurableEnvironment environment, SpringApplication application) {
		PropertySource<?> propertySource = load(hibernatePath);
		System.out.println("加载hibernate-cache配置：" + propertySource.getSource());
		environment.getPropertySources().addLast(propertySource);
	}

	private PropertySource<?> load(String path) {
		Resource resource = new ClassPathResource(path);
		if (!resource.exists()) {
			throw new IllegalArgumentException("Resource " + path + " does not exist");
		}
		PropertiesPropertySourceLoader loader = new PropertiesPropertySourceLoader();
		try {
			return loader.load("classpath:" + path, resource).get(0);
		} catch (IOException ex) {
			throw new IllegalStateException("Failed to load yaml configuration from " + path, ex);
		}
	}
}
