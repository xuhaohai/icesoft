package com.icesoft.core.dao.cache;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.cache.jcache.ConfigSettings;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.orm.jpa.HibernatePropertiesCustomizer;
import org.springframework.cache.jcache.JCacheCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.cache.configuration.CompleteConfiguration;
import javax.cache.configuration.MutableConfiguration;
import javax.cache.expiry.Duration;
import javax.cache.expiry.TouchedExpiryPolicy;
import java.util.concurrent.TimeUnit;

@Slf4j
@Configuration(proxyBeanMethods = false)
public class CustomizeCacheBeanAutoConfiguration {
    private static final long DEFAULT_EXPIRE_TIME = 1800;

    @Bean
    @ConditionalOnMissingBean
    public static CompleteConfiguration defaultCacheConfiguration() {
        MutableConfiguration defaultCacheConfiguration = new MutableConfiguration<>();
        defaultCacheConfiguration.setStatisticsEnabled(true);
        defaultCacheConfiguration
                .setExpiryPolicyFactory(TouchedExpiryPolicy.factoryOf(new Duration(TimeUnit.SECONDS, DEFAULT_EXPIRE_TIME)));
        log.info("默认缓存有效期{}秒", DEFAULT_EXPIRE_TIME);
        return defaultCacheConfiguration;
    }

    static {
        //保证hibernate使用的cacheManager与spring自动注入的是同一个,方便统一管理
        //Caching.setDefaultClassLoader(Thread.currentThread().getContextClassLoader());
    }

    @Bean
    public HibernatePropertiesCustomizer
    hibernateSecondLevelCacheCustomizer(JCacheCacheManager cacheManager) {
        return (properties) -> properties.put(ConfigSettings.CACHE_MANAGER,
                cacheManager.getCacheManager());
    }
}
