package com.icesoft.core.dao.criteria;

import java.util.ArrayList;
import java.util.List;

public final class Page<T> {

	private int total = 0;
	private List<T> data = new ArrayList<>();

	public Page() {

	}

	public Page(List<T> data) {
		this.data = data;
		total = data.size();
	}

	public int getTotal() {
		if (total == 0) {
			return data.size();
		}
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> rows) {
		this.data = rows;
	}

	@Override
	public String toString() {
		return "{total:" + total + ",\"data\":" + data + "}";
	}

}