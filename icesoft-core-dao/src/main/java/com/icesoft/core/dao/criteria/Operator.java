package com.icesoft.core.dao.criteria;

public enum Operator {
    GE(">="), GT(">"), LE("<="), LT("<"), EQ("="), NE("!="), LIKE("like"), IS("is"), IN("in"), NOT_IN("not in");

    Operator(String symbol) {
        this.symbol = symbol;
    }

    private String symbol;

    public String getSymbol() {
        return symbol;
    }

    public static Operator get(String op) {
        return get(op, null);
    }

    public static Operator get(String op, Operator defaultOperator) {
        try {
            return Operator.valueOf(op);
        } catch (IllegalArgumentException e) {
            return defaultOperator;
        }
    }
}