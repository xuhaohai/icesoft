package com.icesoft.core.dao.cache;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.cache.Cache;
import org.springframework.cache.interceptor.CacheOperationInvocationContext;
import org.springframework.cache.interceptor.CacheResolver;
import org.springframework.cache.interceptor.SimpleCacheResolver;
import org.springframework.cache.jcache.JCacheCacheManager;
import org.springframework.cache.jcache.config.JCacheConfigurerSupport;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.cache.configuration.MutableConfiguration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

@Slf4j
@Component
public class JCacheConfigAutoCreateSupport extends JCacheConfigurerSupport {
    private final JCacheCacheManager jCacheCacheManager;
    private final javax.cache.configuration.Configuration<?, ?> defaultCacheConfiguration;

    public JCacheConfigAutoCreateSupport(JCacheCacheManager jCacheCacheManager, ObjectProvider<javax.cache.configuration.Configuration<?, ?>> defaultCacheConfiguration) {
        this.jCacheCacheManager = jCacheCacheManager;
        this.defaultCacheConfiguration = defaultCacheConfiguration.getIfAvailable();
    }

    @Override
    public CacheResolver cacheResolver() {
        return new SimpleCacheResolver(jCacheCacheManager) {
            @Override
            public Collection<? extends Cache> resolveCaches(CacheOperationInvocationContext<?> context) {
                Collection<String> cacheNames = getCacheNames(context);
                if (cacheNames == null) {
                    return Collections.emptyList();
                }
                Collection<Cache> result = new ArrayList<>(cacheNames.size());
                for (String cacheName : cacheNames) {
                    Cache cache = getCacheManager().getCache(cacheName);
                    if (cache == null) {
                        synchronized (JCacheConfigAutoCreateSupport.class) {
                            cache = getCacheManager().getCache(cacheName);
                            if (cache == null) {
                                log.info("自动创建缓存：{}", cacheName);
                                jCacheCacheManager.getCacheManager().createCache(cacheName, getDefaultCacheConfiguration());
                                cache = getCacheManager().getCache(cacheName);
                            }
                        }

                    }
                    result.add(cache);
                }
                return result;
            }
        };
    }

    private javax.cache.configuration.Configuration<?, ?> getDefaultCacheConfiguration() {
        if (this.defaultCacheConfiguration != null) {
            return this.defaultCacheConfiguration;
        }
        return new MutableConfiguration<>();
    }
}
