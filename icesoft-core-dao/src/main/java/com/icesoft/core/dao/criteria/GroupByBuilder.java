package com.icesoft.core.dao.criteria;


import org.apache.commons.lang3.StringUtils;

import java.util.LinkedHashSet;

public class GroupByBuilder extends BaseBuilder {
    GroupByBuilder(FromBuilder fromBuilder) {
        super(fromBuilder);
    }

    LinkedHashSet<String> groups = new LinkedHashSet<>(5);

    GroupByBuilder copy(FromBuilder fromBuilder) {
        GroupByBuilder builder = new GroupByBuilder(fromBuilder);
        builder.groups.addAll(groups);
        return builder;
    }

    public GroupByBuilder by(String propertyName) {
        propertyName = fromBuilder.appendNickName(propertyName);
        groups.add(propertyName);
        return this;
    }

    public String getHql() {
        if (groups.isEmpty()) {
            return "";
        }
        return "group by " + StringUtils.join(groups, ",");
    }

    @Override
    public String toString() {
        return getHql();
    }
}
