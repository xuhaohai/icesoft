package com.icesoft.core.dao.base;

import com.icesoft.core.dao.suppose.IDaoInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
@Slf4j
class EntityManagerDaoInterceptorBeanConfig {

    EntityManagerDaoInterceptorBeanConfig(DaoFacade daoFacade, ObjectProvider<Collection<IDaoInterceptor>> objectProvider) {
        Collection<IDaoInterceptor> interceptors = objectProvider.getIfAvailable();
        if (interceptors != null) {
            daoFacade.getDaoInterceptors().addAll(interceptors);
            log.info("interceptors size:{}", interceptors.size());
        }
    }
}
