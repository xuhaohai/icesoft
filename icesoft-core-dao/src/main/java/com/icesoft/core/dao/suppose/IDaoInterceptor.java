package com.icesoft.core.dao.suppose;

import com.icesoft.core.dao.base.Model;
import com.icesoft.core.dao.criteria.SelectBuilder;

public interface IDaoInterceptor {
    default void onLoad(Model model) {
    }

    default void onCreate(Model model) {
    }

    default void onUpdate(Model model) {
    }

    default void onDelete(Model model) {
    }

    default void onQuery(SelectBuilder selectBuilder) {
    }

}
