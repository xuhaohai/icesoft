package com.icesoft.core.dao.criteria;

abstract class BaseBuilder {
	FromBuilder fromBuilder;

	BaseBuilder(FromBuilder fromBuilder) {
		this.fromBuilder = fromBuilder;
	}

	public FromBuilder end() {
		return fromBuilder;
	}

}
