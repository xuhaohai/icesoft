package com.icesoft.core.dao.criteria;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface FieldRef {

	Class<?> clazz();// 关联实体，也就是join的表

	String name();// 关联实体字段名

	String func() default "";//对该字段使用的函数名，为空表示不使用，默认不使用
}
