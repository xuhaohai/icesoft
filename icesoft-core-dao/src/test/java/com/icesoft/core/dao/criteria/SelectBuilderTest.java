package com.icesoft.core.dao.criteria;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class SelectBuilderTest {

    @Test
    public void testTable() {
        FromBuilder from = FromBuilder.get(Object.class, "current").join(Integer.class, "int").on("int.id", "id")
                .join(Float.class, "float").on("float.intId", "int.id").where().andEq("float.intId", 2).andEq("id", 3)
                .andIn("a", new ArrayList<>()).orFilter().andEq("float.intId", 1).andEq("aid", 2).end().order()
                .desc("id").asc("name").end().group().by("aa").end().page(2, 5);
        QuerySupport querySupport = new QuerySupport(SelectBuilder.select("count", "id", "float.d").builder(from));
        Assertions.assertEquals(
                "select count(current.id,float.d) from Object current join Integer int on int.id = current.id join Float float on float.intId = int.id where (float.intId = 2 AND current.id = 3 AND 1 = 0) OR (float.intId = 1 AND current.aid = 2) group by current.aa",
                querySupport.toString());
        Assertions.assertEquals(
                "select count(current.id,float.d) from Object current join Integer int on int.id = current.id join Float float on float.intId = int.id where (float.intId = :float_intId_1 AND current.id = :current_id_2 AND 1 = 0) OR (float.intId = :float_intId_4 AND current.aid = :current_aid_5) group by current.aa",
                querySupport.getHql());

    }

    @Test
    public void test() {
        FromBuilder from = FromBuilder.get(Object.class, "o").where().andIsNull("id").orIsNotNull("id").andFilter()
                .andLike("id", "a").orLike("id", "a").orLike("1", "1").orEq("id", 1).andIn("id", Collections.emptyList())
                .andNotIn("id", Collections.emptyList()).andNotIn("id", Arrays.asList(1)).orIn("id", Collections.emptyList()).orNotIn("id", Collections.emptyList())
                .orNotIn("id", Arrays.asList(1)).end().offsetLimit(1, 1).order().ascNullFirst("id").descNullLast("id")
                .end();
        QueryBuilder queryBuilder = new QueryBuilder(from);
        QuerySupport querySupport = new QuerySupport(SelectBuilder.select("count", "id").builder(from));
        QuerySupport querySupport2 = new QuerySupport(SelectBuilder.select("count", "id").builder(queryBuilder.getFromBuilder()));
        Assertions.assertEquals(querySupport2.toString(), querySupport.toString());
        Assertions.assertEquals(querySupport2.getHql(), querySupport.getHql());
    }

    @Test
    public void testQueryBuilderSub() {
        QueryBuilder queryBuilder = QueryBuilder.get("name", "1").andSub().orEq("id", 1).orEq("id", 2)
                .andEq("age", 1);
        FromBuilder from = FromBuilder.get().where().andEq("name", "1").andFilter().orEq("id", 1).orEq("id", 2)
                .andEq("age", 1).end();
        QuerySupport querySupport = new QuerySupport(SelectBuilder.selectFrom(from));
        Assertions.assertEquals(queryBuilder.toString(), querySupport.toString());
        System.err.println(queryBuilder);
    }

    @Test
    public void testQueryBuilder() {
        FromBuilder from = FromBuilder.get(Object.class, "o").where().andEq("id", "1").andIsNull("id").orIsNotNull("id")
                .end().offsetLimit(1, 1).order().asc("id").desc("name").end();
        QueryBuilder queryBuilder = QueryBuilder.get(FromBuilder.get(Object.class, "o")).andEq("id", "1").andIsNull("id").orIsNotNull("id").offset(1)
                .limit(1).asc("id").desc("name");
        QuerySupport querySupport = new QuerySupport(SelectBuilder.select(Object.class).builder(from));
        QuerySupport querySupport2 = new QuerySupport(SelectBuilder.select(Object.class).builder(queryBuilder.getFromBuilder()));
        Assertions.assertEquals(querySupport2.toString(), querySupport.toString());
        Assertions.assertEquals(querySupport2.getHql(), querySupport.getHql());
        Assertions.assertEquals(
                "select o from Object o where o.id = :o_id_1 AND o.id is null OR o.id is not null order by o.id asc,o.name desc",
                querySupport2.getHql());
    }

    @Test
    public void testSubSelectBuilder() {
        SelectBuilder subSelectBuilder = SelectBuilder.select("id").builder(FromBuilder.get(Object.class, "o").where().andEq("id", 1).end());
        FromBuilder from = FromBuilder.get(Object.class, "o").where().andIn("id", subSelectBuilder).andEq("id", "2").andIsNull("id").orIsNotNull("id")
                .end().offsetLimit(1, 1).order().asc("id").desc("name").end();
        QueryBuilder queryBuilder = QueryBuilder.get(FromBuilder.get(Object.class, "o")).andIn("id", subSelectBuilder).andEq("id", "2").andIsNull("id").orIsNotNull("id").offset(1)
                .limit(1).asc("id").desc("name");
        QuerySupport querySupport = new QuerySupport(SelectBuilder.select(Object.class).builder(from));
        QuerySupport querySupport2 = new QuerySupport(SelectBuilder.select(Object.class).builder(queryBuilder.getFromBuilder()));
        Assertions.assertEquals(querySupport2.toString(), querySupport.toString());
        Assertions.assertEquals(querySupport2.getHql(), querySupport.getHql());
        Assertions.assertEquals(
                "select o from Object o where o.id in (select o1.id from Object o1 where o1.id = :o1_id_1_1) AND o.id = :o_id_2 AND o.id is null OR o.id is not null order by o.id asc,o.name desc",
                querySupport2.getHql());
    }

    @Test
    public void testFilter() {
        SelectBuilder subSelectBuilder = SelectBuilder.select("id").builder(FromBuilder.get().where().andEq("id", 1).end());
        FromBuilder from = FromBuilder.get().where().andIn("id", subSelectBuilder)
                .andEq("id", "2").andIsNull("id").orIsNotNull("id")
                .filter().andEq("id", 2)
                .end().offsetLimit(1, 1).order().asc("id").desc("name").end();
        QuerySupport querySupport = new QuerySupport(SelectBuilder.select(Object.class).builder(from));
        Assertions.assertEquals(
                "select f from Object f where (f.id in (select f1.id from Object f1 where f1.id = :f1_id_1_1) AND f.id = :f_id_2 AND f.id is null OR f.id is not null) AND f.id = :f_id_5 order by f.id asc,f.name desc",
                querySupport.getHql());
        Assertions.assertEquals(
                "select f from Object f where (f.id in (select f1.id from Object f1 where f1.id = 1 ) AND f.id = 2 AND f.id is null OR f.id is not null) AND f.id = 2 order by f.id asc,f.name desc limit 1 offset 1",
                querySupport.getStr());
        System.out.println(querySupport);
    }
}
