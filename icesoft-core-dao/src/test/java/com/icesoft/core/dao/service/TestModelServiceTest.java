package com.icesoft.core.dao.service;

import com.icesoft.core.dao.model.TestModel;
import com.icesoft.test.start.DaoApplication;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;


@SpringBootTest(classes = DaoApplication.class)
@ActiveProfiles("test")
public class TestModelServiceTest {
    @Autowired
    private TestModelService testModelService;
    private static TestModel testModel;

    @BeforeEach
    public void testCreate() {
        if (testModel == null) {
            testModel = new TestModel();
            testModel.setName("name");
            testModel.setAge(10);
            testModelService.create(testModel);
        }
    }

    @Test
    public void testFindByAge() {
        List<TestModel> list = testModelService.findByAge(testModel.getAge());
        Assertions.assertEquals(1, list.size());
    }

    @Test
    public void testFindByName() {
        List<TestModel> list = testModelService.findByName(testModel.getName());
        Assertions.assertEquals(1, list.size());
    }

    @Test
    public void testFindNames() {
        List<String> list = testModelService.findNames();
        Assertions.assertEquals(1, list.size());
        Assertions.assertEquals(testModel.getName(), list.get(0));
    }
}
