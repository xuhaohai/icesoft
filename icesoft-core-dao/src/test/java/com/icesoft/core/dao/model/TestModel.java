package com.icesoft.core.dao.model;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.icesoft.core.dao.base.BaseVersionModel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name="tb_test")
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Cacheable
@NoArgsConstructor
@AllArgsConstructor
public class TestModel extends BaseVersionModel {

	private String name;
	private int age;
}
