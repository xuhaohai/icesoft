package com.icesoft.core.dao.base;

import com.icesoft.core.dao.criteria.FromBuilder;
import com.icesoft.core.dao.criteria.SelectBuilder;
import com.icesoft.core.dao.model.TestModel;
import com.icesoft.test.start.DaoApplication;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.Arrays;
import java.util.List;

@SpringBootTest(classes = DaoApplication.class)
@ActiveProfiles("test")
public class DaoFacadeTest {

	@Autowired
    DaoFacade daoFacade;

	@Test
	public void testCreate() {
		TestModel testModel = new TestModel();
		testModel.setName("name");
		testModel.setAge(10);
		daoFacade.create(testModel);
		Assertions.assertEquals(1, daoFacade.findAll(TestModel.class).size());
		testModel.setAge(100);
		daoFacade.update(testModel);
		testModel = daoFacade.load(TestModel.class, testModel.getId());
		daoFacade.update(testModel);
		Assertions.assertEquals(100, testModel.getAge());
		daoFacade.delete(testModel);
		Assertions.assertEquals(0, daoFacade.countAll(TestModel.class));

	}

	@Test
	public void testHql() {
		TestModel testModel = new TestModel();
		testModel.setName("name");
		testModel.setAge(10);
		daoFacade.batchCreate(Arrays.asList(testModel));
		Assertions.assertEquals(1L, daoFacade.createQuery("select count(*) from TestModel").getSingleResult());

		daoFacade.delete(testModel);
		Assertions.assertEquals(0, daoFacade.countAll(TestModel.class));
	}

	@Test
	public void testQuery() {
		TestModel testModel = new TestModel();
		testModel.setName("name");
		testModel.setAge(10);
		daoFacade.create(testModel);
		FromBuilder fromBuilder = FromBuilder.get(TestModel.class).where().andEq("age", 10).end().order()
				.descNullFirst("id").ascNullLast("age").end();
		List<TestModel> list = daoFacade.findList(SelectBuilder.selectFrom(fromBuilder));
		Assertions.assertEquals(1, list.size());
		daoFacade.findSingle(SelectBuilder.selectFrom((FromBuilder.get(TestModel.class).where().andEq("age", 10).end())));
		TestModel temp = daoFacade
				.findSingle(SelectBuilder.selectFrom(FromBuilder.get(TestModel.class).where().andEq("age", 110).end()));
		Assertions.assertNull(temp);

		daoFacade.delete(testModel);
		Assertions.assertEquals(0, daoFacade.count(fromBuilder));
		Assertions.assertEquals(0, daoFacade.countAll(TestModel.class));
	}
}
