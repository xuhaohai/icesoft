package com.icesoft.core.dao.service;

import com.icesoft.core.dao.base.BaseDao;
import com.icesoft.core.dao.criteria.QueryBuilder;
import com.icesoft.core.dao.criteria.SelectBuilder;
import com.icesoft.core.dao.model.TestModel;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TestModelService extends BaseDao<TestModel> {

    public List<TestModel> findByName(String name) {
        return find(QueryBuilder.get("name", name));
    }

    public List<TestModel> findByAge(int age) {
        return find(QueryBuilder.get("age", age));
    }

    public int findMaxAge() {
        return uniqueSelect(SelectBuilder.select("max", "age"));
    }

    public List<String> findNames() {
        return findSelect(SelectBuilder.select("name"));
    }
}
