package com.icesoft.test.start;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.icesoft")
@EnableCaching
@EntityScan("com.icesoft")
public class DaoApplication {

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(DaoApplication.class);
        app.run(args);
    }
}
