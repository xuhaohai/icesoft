package com.icesoft.core.common.helper.lock.base;

public interface ICacheLockGroup {

	ICacheLock getGroup(String groupName);
}
