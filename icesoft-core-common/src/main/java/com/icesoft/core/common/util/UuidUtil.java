package com.icesoft.core.common.util;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

public class UuidUtil {

    private final static char[] WORDS = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g',
            'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B',
            'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
            'X', 'Y', 'Z'};

    /**
     * 随机ID生成器，由数字、小写字母和大写字母组成 ID长度为12时，当系统中已有十亿笔数据，那么要再插入大约3000笔，才会发生一次重复。
     * ID长度为16时，即使系统中已有百亿笔数据，那么还要再插入大约5亿笔，才会发生一次重复。
     *
     * @param size uuid长度
     * @return 指定长度的随机字符串
     */
    public static String getUUID(int size) {
        Random random = new Random();
        char[] cs = new char[size];
        for (int i = 0; i < cs.length; i++) {
            cs[i] = WORDS[random.nextInt(WORDS.length)];
        }
        return new String(cs);
    }

    public static String get32UUID() {
        return UUID.randomUUID().toString().trim().replaceAll("-", "");
    }

    /**
     * 产生n个只含有数字和字母长度为m（m<=52）的无重复随机字符串
     *
     * @see Random 注意：是通过set集合保证不重复，不是同一次生成的是有可能重复的
     */
    public static Set<String> cDifferentRandoms(int n, int m) {
        if (m > 52) {
            return null;
        } else {
            Set<String> set = new HashSet<>();
            while (set.size() < n) {
                set.add(getUUID(m));
            }
            return set;
        }
    }

    /**
     * 产生1个长度为m随机数字字符串
     */
    public static String cRandomNum(int m) {
        char[] chs = new char[m];
        for (int i = 0; i < m; i++) {
            chs[i] = cNumCharRandom();
        }
        return new String(chs);
    }

   private final static char[] digest = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

    /**
     * 产生一个随机数字
     */
    public static char cNumCharRandom() {
        return digest[iRandom(0, digest.length)];
    }

    /**
     * 产生一个[m,n)之间的随机整数
     */
    public static int iRandom(int m, int n) {
        Random random = new Random();
        int small = Math.min(m, n);
        int big = Math.max(m, n);
        return small + random.nextInt(big - small);
    }

}
