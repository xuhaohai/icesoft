package com.icesoft.core.common.helper;

import org.springframework.beans.BeansException;

public abstract class BeanHold {

	public static <T> T getBean(Class<T> cls){
		try {
			return SpringContextUtils.getBean(cls);
		} catch (BeansException e) {
			return null;
		}
	}
	public static <T> T getBean(String name){
		try {
			return SpringContextUtils.getBean(name);
		} catch (BeansException e) {
			return null;
		}
	}
}
