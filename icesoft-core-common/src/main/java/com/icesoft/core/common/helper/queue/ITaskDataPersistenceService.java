package com.icesoft.core.common.helper.queue;

import java.util.List;

public interface ITaskDataPersistenceService<T> {
	/**
	 * 重新启动时，获取上次正在处理的数据，重新进入处理，会自动调用deleteRunningData删除
	 */
	List<T> listLastRunning();

	/**
	 * 持久化进入处理的数据，方便下次重新启动加入处理
	 */
	void saveRunningData(T data);

	/**
	 * 删除临时保存的数据
	 */
	void deleteRunningData(T data);
}
