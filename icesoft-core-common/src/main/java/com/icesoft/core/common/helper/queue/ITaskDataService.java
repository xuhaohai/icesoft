package com.icesoft.core.common.helper.queue;

public interface ITaskDataService<T> {

	/**
	 * 阻塞出栈，要处理的数据
	 */
	T blockPop();

	/**
	 * 数据处理过程，主要功能，
	 * 
	 * @param data
	 *            待处理的数据
	 */
	void handle(T data) throws Exception;

	/**
	 * 数据处理过程的异常处理
	 * 
	 * @param data
	 *            处理的数据
	 * @param e
	 *            handle方法执行出现的异常
	 */
	void exceptionHandle(T data, Exception e);

	/**
	 * 队列key，唯一，保持重启不变
	 */
	public String taskKey();

	/**
	 * 并发阻塞线程线
	 */
	default int limitThreadSize() {
		return 10;
	}

	/**
	 * 针对正在处理的数据进行持久化，异常停机可以重新进入处理
	 * 
	 * @return 返回null表示跳过持久化
	 */
	default ITaskDataPersistenceService<T> persistenceService() {
		return null;
	}

}
