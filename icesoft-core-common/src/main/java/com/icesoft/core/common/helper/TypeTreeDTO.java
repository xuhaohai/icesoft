package com.icesoft.core.common.helper;

import com.icesoft.core.common.helper.tree.BaseTree;
import lombok.ToString;

import java.util.List;

@ToString
public class TypeTreeDTO implements BaseTree<TypeTreeDTO> {

	int id;
	int pid;
	Class<?> type;
	List<TypeTreeDTO> children;

	@Override
    public List<TypeTreeDTO> getChildren() {
		return children;
	}

	@Override
	public void setChildren(List<TypeTreeDTO> children) {
		this.children = children;
	}

	public Class<?> getType() {
		return type;
	}

	public void setType(Class<?> type) {
		this.type = type;
	}

	@Override
	public boolean isParent(TypeTreeDTO parent) {
		return parent.getId() == this.pid;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

}
