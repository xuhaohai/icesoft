package com.icesoft.core.common.helper;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.TimeZone;

public interface ConstValue {
    String TIME_ZONE_VALUE = "GMT+8";
    TimeZone TIME_ZONE = TimeZone.getTimeZone(ConstValue.TIME_ZONE_VALUE);
    String CHARSET_UTF_8_VALUE = "UTF-8";
    Charset CHARSET_UTF_8 = StandardCharsets.UTF_8;
    String DATE_TIME_PATTEN = "yyyy-MM-dd HH:mm:ss";
}
