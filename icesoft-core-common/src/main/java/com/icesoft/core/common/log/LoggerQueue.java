package com.icesoft.core.common.log;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * 创建一个阻塞队列，作为日志系统输出的日志的一个临时载体
 * 
 * @author jie
 * @date 2018-12-24
 */
public class LoggerQueue {

	/**
	 * 队列大小
	 */
	public static final int QUEUE_MAX_SIZE = 10000;
	private BlockingQueue<LogMessage> blockingQueue = new LinkedBlockingQueue<>(QUEUE_MAX_SIZE);
	public static LoggerQueue alarmMessageQueue = new LoggerQueue();

	/**
	 * 阻塞队列
	 */

	private LoggerQueue() {
	}

	public synchronized static LoggerQueue getInstance() {
		return alarmMessageQueue;
	}

	/**
	 * 消息入队
	 * 
	 * @param log
	 * @return
	 */
	public boolean push(LogMessage log) {
		try {
			return this.blockingQueue.add(log);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 消息出队
	 *
	 * @return
	 * @throws InterruptedException
	 */
	public LogMessage poll() throws InterruptedException {
		return this.blockingQueue.take();
	}
}
