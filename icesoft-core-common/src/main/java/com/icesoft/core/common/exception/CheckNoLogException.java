package com.icesoft.core.common.exception;

/**
 * @author XHH
 */
public class CheckNoLogException extends RuntimeException {

    public CheckNoLogException(String message) {
        super(message);
    }

    public CheckNoLogException(String message, Throwable cause) {
        super(message, cause);
    }

    public CheckNoLogException(Throwable cause) {
        super(cause);
    }
}
