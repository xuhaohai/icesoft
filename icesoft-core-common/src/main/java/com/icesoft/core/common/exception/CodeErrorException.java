package com.icesoft.core.common.exception;
/**
 * @author XHH
 * */
public class CodeErrorException extends RuntimeException{

	public CodeErrorException(String message) {
		super(message);
	}

}
