package com.icesoft.core.common.configurer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.nio.charset.Charset;
import java.util.TimeZone;

@Slf4j
@Component
public class EnvCheckRunner implements ApplicationRunner {
    @Override
    public void run(ApplicationArguments args) {
        TimeZone timeZone = TimeZone.getDefault();
        if (!"GMT+08:00".equals(timeZone.getID())) {
            log.warn("当前时区：{}，默认时区不是GMT+8，推荐启动时指定：-Duser.timezone=GMT+8", timeZone.getID());
        }
        if (!"UTF-8".equals(Charset.defaultCharset().name())) {
            log.warn("默认编码不是UTF8，推荐启动时指定：-Dfile.encoding=UTF-8");
        }
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            log.info("优雅关闭服务 ...");
        }));
    }
}
