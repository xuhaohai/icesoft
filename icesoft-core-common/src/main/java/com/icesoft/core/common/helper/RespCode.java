package com.icesoft.core.common.helper;

public abstract class RespCode {
	public static final int SUCCESS = 0;
	public static final int FAIL = 1;
	public static final int NO_PERMISSION = 403;
	public static final int REDIRECT = 302;
}
