package com.icesoft.core.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.DigestUtils;

import java.io.*;
import java.nio.charset.Charset;

public class MD5Util {
    private static final Logger LOG = LoggerFactory.getLogger(MD5Util.class);

    /**
     * 对一个文件求他的md5值
     *
     * @param f 要求md5值的文件
     * @return md5串
     */
    public static String md5(File f) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(f);
            return md5(fis);
        } catch (FileNotFoundException e) {
            LOG.error("md5 file " + f.getAbsolutePath() + " failed:" + e.getMessage());
            return null;
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException ex) {
                LOG.error("文件操作失败", ex);
            }
        }
    }

    /**
     * 求一个字符串的md5值
     *
     * @param content 字符串
     * @return md5 value
     */
    public static String md5(String content) {
        return DigestUtils.md5DigestAsHex(content.getBytes(Charset.forName("utf8")));
    }

    public static String md5(byte[] bytes) {
        return DigestUtils.md5DigestAsHex(bytes);
    }

    /**
     * 求一个输入流的md5值
     *
     * @param is 输入流
     * @return md5 value
     */
    public static String md5(InputStream is) {
        try {
            return DigestUtils.md5DigestAsHex(is);
        } catch (IOException e) {
            LOG.error("md5 InputStream error", e);
            e.printStackTrace();
        }
        return null;
    }
}