package com.icesoft.core.common.log;

public interface ILogMessageListener {

	void handle(LogMessage logMessage);
}
