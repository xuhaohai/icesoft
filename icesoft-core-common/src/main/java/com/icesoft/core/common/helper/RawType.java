package com.icesoft.core.common.helper;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public abstract class RawType<T> {

	private Type rawType;

	public RawType() {
		Type superClass = this.getClass().getGenericSuperclass();
		if (superClass instanceof Class<?>) {
			throw new IllegalArgumentException(
					"Internal error: RowType constructed without actual type information");
		}
		rawType = ((ParameterizedType) superClass).getActualTypeArguments()[0];
	}

	public Type getRawType() {
		return rawType;
	}

	@SuppressWarnings("unchecked")
	public Class<T> getRawClass() {
		return (Class<T>) rawType;
	}
}
