package com.icesoft.core.common.exception;
/**
 * @author XHH
 * */
public class CheckException extends RuntimeException{

	public CheckException(String message) {
		super(message);
	}

	public CheckException(String message, Throwable cause) {
		super(message, cause);
	}

	public CheckException(Throwable cause) {
		super(cause);
	}
}
