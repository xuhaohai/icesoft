package com.icesoft.core.common.helper;

import org.springframework.core.annotation.AnnotationUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public abstract class AnnotationUtil {

    /**
     * 获取方法上的注解，包含继承的，没有找到则获取类上注解，包含继承的
     */
    public static <T extends Annotation> T findAnyAnnotation(Method method, Class<T> clazz) {
        //查找method上是否有注解，包含继承的
        T annotation = AnnotationUtils.findAnnotation(method, clazz);
        if (annotation != null) {
            return annotation;
        }
        //查找method的所属class上是否有注解，包含继承的
        return AnnotationUtils.findAnnotation(method.getDeclaringClass(), clazz);
    }

}
