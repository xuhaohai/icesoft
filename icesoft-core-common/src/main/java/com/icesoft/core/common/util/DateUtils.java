package com.icesoft.core.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * 描述:公共日期工具类
 */
public class DateUtils {

	public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
	public static final String DATE_TIME_FORMAT_FILE_NAME = "yyyy-MM-dd HH-mm-ss";

	public static final String DATE_FORMAT_CHINESE = "yyyy年M月d日";
	public static final String DATE_TIME_FORMAT_CHINESE = "yyyy年M月d日 HH时mm分ss秒";

	/**
	 * 获取当前日期时间
	 * 
	 * @return yyyy-MM-dd HH:mm:ss
	 * 
	 **/
	public static String getCurrentDateTime() {
		String datestr = null;
		SimpleDateFormat df = new SimpleDateFormat(DateUtils.DATE_TIME_FORMAT);
		datestr = df.format(new Date());
		return datestr;
	}

	/**
	 * 获取当前日期时间
	 * 
	 * @return yyyy-MM-dd HH-mm-ss
	 * 
	 **/
	public static String getCurrentDateTimeFileName() {
		SimpleDateFormat df = new SimpleDateFormat(DateUtils.DATE_TIME_FORMAT_FILE_NAME);
		return df.format(new Date());
	}

	/**
	 * 将日期格式日期转换为字符串格式
	 * 
	 * @param date
	 *            Date
	 * @return yyyy-MM-dd HH:mm:ss
	 */
	public static String format(Date date) {
		return format(date, DateUtils.DATE_TIME_FORMAT);
	}

	/**
	 * 将日期格式日期转换为字符串格式 自定義格式
	 * 
	 * @param date
	 * @param dateformat
	 * @return
	 */
	public static String format(Date date, String dateformat) {
		String datestr = null;
		SimpleDateFormat df = new SimpleDateFormat(dateformat);
		datestr = df.format(date);
		return datestr;
	}

	/**
	 * 将字符串日期转换为日期格式
	 * 
	 * 
	 * @param datestr
	 *            格式要求：yyyy-MM-dd HH:mm:ss
	 * @return
	 * 
	 */
	public static Date parse(String datestr) {
		return parse(datestr, DateUtils.DATE_TIME_FORMAT);
	}

	/**
	 * 将字符串日期转换为日期格式 自定義格式
	 * 
	 * @param datestr
	 * @return
	 * 
	 */
	public static Date parse(String datestr, String dateformat) {
		if (datestr == null || "".equals(datestr)) {
			return null;
		}
		Date date = null;
		SimpleDateFormat df = new SimpleDateFormat(dateformat);
		try {
			date = df.parse(datestr);
		} catch (ParseException e) {
			throw new IllegalArgumentException("参数格式错误：" + datestr);
		}
		return date;
	}

	/**
	 * 获取日期的DAY值
	 * 
	 * 
	 * @param date
	 *            输入日期
	 * @return
	 * 
	 */
	public static int getDay(Date date) {
		int d = 0;
		Calendar cd = Calendar.getInstance();
		cd.setTime(date);
		d = cd.get(Calendar.DAY_OF_MONTH);
		return d;
	}

	/**
	 * 获取当天的开始时间   00:00:00.000
	 * */
	public static Date getDayStartTime() {
		Calendar cal = new GregorianCalendar();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	/**
	 * 获取当天的结束时间 23:59:59.999
	 * */
	public static Date getDayEndTime() {
		Calendar cal = new GregorianCalendar();
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 999);
		return cal.getTime();
	}

	/**
	 * 获取某个日期的开始时间  00:00:00.000
	 * */
	public static Date getDayStartTime(Date d) {
		Calendar calendar = Calendar.getInstance();
		if (null != d)
			calendar.setTime(d);
		calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), 0,
				0, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}

	/**
	 * 获取某个日期的结束时间  23:59:59.999
	 * */
	public static Date getDayEndTime(Date d) {
		Calendar calendar = Calendar.getInstance();
		if (null != d)
			calendar.setTime(d);
		calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), 23,
				59, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		return calendar.getTime();
	}

	/**
	 * 获取日期的MONTH值
	 * 
	 * 
	 * @param date
	 *            输入日期
	 * @return
	 * 
	 */
	public static int getMonth(Date date) {
		int m = 0;
		Calendar cd = Calendar.getInstance();
		cd.setTime(date);
		m = cd.get(Calendar.MONTH) + 1;
		return m;
	}

	/**
	 * 获取日期的YEAR值
	 * 
	 * 
	 * @param date
	 *            输入日期
	 * @return
	 * 
	 */
	public static int getYear(Date date) {
		int y = 0;
		Calendar cd = Calendar.getInstance();
		cd.setTime(date);
		y = cd.get(Calendar.YEAR);
		return y;
	}

	/**
	 * 获取星期几
	 * 
	 * 
	 * @param date
	 *            输入日期
	 * @return
	 * 
	 */
	public static int getWeek(Date date) {
		int wd = 0;
		Calendar cd = Calendar.getInstance();
		cd.setTime(date);
		wd = cd.get(Calendar.DAY_OF_WEEK) - 1;
		return wd;
	}

	/**
	 * 获取输入日期的当月第一天
	 * 
	 * 
	 * @param date
	 *            输入日期
	 * @return
	 * 
	 */
	public static Date getFirstDayOfMonth(Date date) {
		Calendar cd = Calendar.getInstance();
		cd.setTime(date);
		cd.set(Calendar.DAY_OF_MONTH, 1);
		return cd.getTime();
	}

	/**
	 * 获得输入日期的当月最后一天
	 * 
	 * @param date
	 */
	public static Date getLastDayOfMonth(Date date) {
		return DateUtils.addDay(DateUtils.getFirstDayOfMonth(DateUtils.addMonth(date, 1)), -1);
	}

	/**
	 * 判断是否是闰年
	 * 
	 * 
	 * @param date
	 *            输入日期
	 * @return 是true 否false
	 * 
	 */
	public static boolean isLeapYear(Date date) {

		Calendar cd = Calendar.getInstance();
		cd.setTime(date);
		int year = cd.get(Calendar.YEAR);

		if (year % 4 == 0 && year % 100 != 0 | year % 400 == 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 获取年周期对应日
	 * 
	 * @param date
	 *            输入日期
	 * @param iyear
	 *            年数 負數表示之前
	 * @return
	 * 
	 */
	public static Date getYearCycleOfDate(Date date, int iyear) {
		Calendar cd = Calendar.getInstance();
		cd.setTime(date);

		cd.add(Calendar.YEAR, iyear);

		return cd.getTime();
	}

	/**
	 * 获取月周期对应日
	 * 
	 * @param date
	 *            输入日期
	 * @param i
	 * @return
	 * 
	 */
	public static Date getMonthCycleOfDate(Date date, int i) {
		Calendar cd = Calendar.getInstance();
		cd.setTime(date);

		cd.add(Calendar.MONTH, i);

		return cd.getTime();
	}

	/**
	 * 计算 fromDate 到 toDate 相差多少年
	 * 
	 * @param fromDate
	 * @param toDate
	 * @return 年数
	 * 
	 */
	public static int getYearByMinusDate(Date fromDate, Date toDate) {
		Calendar df = Calendar.getInstance();
		df.setTime(fromDate);

		Calendar dt = Calendar.getInstance();
		dt.setTime(toDate);

		return dt.get(Calendar.YEAR) - df.get(Calendar.YEAR);
	}

	/**
	 * 计算 fromDate 到 toDate 相差多少个月
	 * 
	 * @param fromDate
	 * @param toDate
	 * @return 月数
	 * 
	 */
	public static int getMonthByMinusDate(Date fromDate, Date toDate) {
		Calendar df = Calendar.getInstance();
		df.setTime(fromDate);

		Calendar dt = Calendar.getInstance();
		dt.setTime(toDate);

		return dt.get(Calendar.YEAR) * 12 + dt.get(Calendar.MONTH)
				- (df.get(Calendar.YEAR) * 12 + df.get(Calendar.MONTH));
	}

	/**
	 * 计算 fromDate 到 toDate 相差多少天
	 * 
	 * @param fromDate
	 * @param toDate
	 * @return 天数
	 * 
	 */
	public static long getDayByMinusDate(Date fromDate, Date toDate) {

		long fd = fromDate.getTime();
		long td = toDate.getTime();

		return (td - fd) / (24L * 60L * 60L * 1000L);
	}

	/**
	 * 计算年龄
	 * 
	 * @param birthday
	 *            生日日期
	 * @param calcDate
	 *            要计算的日期点
	 * @return
	 * 
	 */
	public static int calcAge(Date birthday, Date calcDate) {

		int cYear = DateUtils.getYear(calcDate);
		int cMonth = DateUtils.getMonth(calcDate);
		int cDay = DateUtils.getDay(calcDate);
		int bYear = DateUtils.getYear(birthday);
		int bMonth = DateUtils.getMonth(birthday);
		int bDay = DateUtils.getDay(birthday);

		if (cMonth > bMonth || (cMonth == bMonth && cDay > bDay)) {
			return cYear - bYear;
		} else {
			return cYear - 1 - bYear;
		}
	}

	/**
	 * 从身份证中获取出生日期
	 * 
	 * @param idno
	 *            身份证号码
	 * @return
	 * 
	 */
	public static String getBirthDayFromIDCard(String idno) {
		Calendar cd = Calendar.getInstance();
		if (idno.length() == 15) {
			cd.set(Calendar.YEAR, Integer.valueOf("19" + idno.substring(6, 8)).intValue());
			cd.set(Calendar.MONTH, Integer.valueOf(idno.substring(8, 10)).intValue() - 1);
			cd.set(Calendar.DAY_OF_MONTH, Integer.valueOf(idno.substring(10, 12)).intValue());
		} else if (idno.length() == 18) {
			cd.set(Calendar.YEAR, Integer.valueOf(idno.substring(6, 10)).intValue());
			cd.set(Calendar.MONTH, Integer.valueOf(idno.substring(10, 12)).intValue() - 1);
			cd.set(Calendar.DAY_OF_MONTH, Integer.valueOf(idno.substring(12, 14)).intValue());
		}
		return DateUtils.format(cd.getTime());
	}

	/**
	 * 在输入日期上增加（+）或减去（-）天数
	 * 
	 * @param date
	 *            输入日期
	 * @param iday
	 *            要增加或减少的天数
	 */
	public static Date addDay(Date date, int iday) {
		Calendar cd = Calendar.getInstance();

		cd.setTime(date);

		cd.add(Calendar.DAY_OF_MONTH, iday);

		return cd.getTime();
	}

	/**
	 * 在输入日期上增加（+）或减去（-）月份
	 * 
	 * @param date
	 *            输入日期
	 * @param imonth
	 *            要增加或减少的月分数
	 */
	public static Date addMonth(Date date, int imonth) {
		Calendar cd = Calendar.getInstance();

		cd.setTime(date);

		cd.add(Calendar.MONTH, imonth);

		return cd.getTime();
	}

	/**
	 * 在输入日期上增加（+）或减去（-）年份
	 * 
	 * @param date
	 *            输入日期
	 * @param iyear
	 *            要增加或减少的年数
	 */
	public static Date addYear(Date date, int iyear) {
		Calendar cd = Calendar.getInstance();

		cd.setTime(date);

		cd.add(Calendar.YEAR, iyear);

		return cd.getTime();
	}

	public static TimeTotal toTotalTime(long second) {
		long minute = second / 60;
		second = second % 60;
		long hour = minute / 60;
		minute = minute % 60;
		long day = hour / 24;
		hour = hour % 24;
		TimeTotal timeTotal = new TimeTotal(day, hour, minute, second);
		return timeTotal;
	}

	public static class TimeTotal {
		private long day;
		private long hour;
		private long minute;
		private long second;

		public TimeTotal() {
		}

		public TimeTotal(long day, long hour, long minute, long second) {
			super();
			this.day = day;
			this.hour = hour;
			this.minute = minute;
			this.second = second;
		}

		public long getDay() {
			return day;
		}

		public void setDay(long day) {
			this.day = day;
		}

		public long getHour() {
			return hour;
		}

		public void setHour(long hour) {
			this.hour = hour;
		}

		public long getMinute() {
			return minute;
		}

		public void setMinute(long minute) {
			this.minute = minute;
		}

		public long getSecond() {
			return second;
		}

		public void setSecond(long second) {
			this.second = second;
		}

		@Override
		public String toString() {
			return toString(false);
		}

		public String toString(boolean isShort) {
			String secondString = second + "秒";
			String minuteString = minute + "分钟";
			String hourString = hour + "小时";
			String dayString = day + "天";
			if (isShort) {
				if (second == 0) {
					secondString = "";
				}
				if (minute == 0) {
					minuteString = "";
				}
				if (hour == 0) {
					hourString = "";
				}
				if (day == 0) {
					dayString = "";
				}
			}
			return dayString + hourString + minuteString + secondString;
		}
	}

}
