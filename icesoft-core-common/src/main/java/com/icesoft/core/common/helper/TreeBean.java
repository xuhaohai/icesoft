package com.icesoft.core.common.helper;

import lombok.ToString;

@ToString
public class TreeBean {

	private String id;
	private String pId;
	private String name;
	private String icon;
	private boolean checked;
	private boolean open = true;
	private boolean nocheck = false;

	public TreeBean() {
	}

	public TreeBean(String id, String pId, String name) {
		this.id = id;
		this.pId = pId;
		this.name = name;
	}

	public TreeBean(String id, String pId, String name, boolean checked, boolean open) {
		this.id = id;
		this.pId = pId;
		this.name = name;
		this.checked = checked;
		this.open = open;
	}

	public TreeBean(String id, String pId, String name, boolean nocheck) {
		this.id = id;
		this.pId = pId;
		this.name = name;
		this.nocheck = nocheck;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getpId() {
		return pId;
	}
	public void setpId(String pId) {
		this.pId = pId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public boolean isChecked() {
		return checked;
	}
	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	public boolean isOpen() {
		return open;
	}
	public void setOpen(boolean open) {
		this.open = open;
	}
	public boolean isNocheck() {
		return nocheck;
	}
	public void setNocheck(boolean nocheck) {
		this.nocheck = nocheck;
	}
}
