package com.icesoft.core.common.helper.tree;

public interface TreeConsumer<T extends BaseTree<T>> {

	void forEach(T current, int level);
}
