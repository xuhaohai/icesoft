package com.icesoft.core.common.helper.uid;

import lombok.Data;

@Data
public class WorkIdDTO {

	public WorkIdDTO(long workerId, long datacenterId) {
		if (workerId > SnowflakeIdWorker.MAX_WORK_ID || workerId < 0) {
			throw new IllegalArgumentException(
					String.format("worker Id can't be greater than %d or less than 0", SnowflakeIdWorker.MAX_WORK_ID));
		}
		if (datacenterId > SnowflakeIdWorker.MAX_DATA_CENTER_ID || datacenterId < 0) {
			throw new IllegalArgumentException(String.format("datacenter Id can't be greater than %d or less than 0",
					SnowflakeIdWorker.MAX_DATA_CENTER_ID));
		}
		this.workerId = workerId;
		this.datacenterId = datacenterId;
	}

	/** 工作机器ID(0~31) */
	private long workerId;

	/** 数据中心ID(0~31) */
	private long datacenterId;
}
