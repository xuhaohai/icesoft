package com.icesoft.core.common.helper.tree;

import java.util.List;

public interface BaseTree<T> {

	boolean isParent(T parent);

	List<T> getChildren();

	void setChildren(List<T> children);

}
