package com.icesoft;

import com.icesoft.core.common.helper.ConstValue;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.freemarker.FreeMarkerAutoConfiguration;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.scheduling.annotation.EnableAsync;

import java.nio.charset.Charset;
import java.util.TimeZone;

/**
 * @author XHH
 */
@SpringBootApplication(exclude = {FreeMarkerAutoConfiguration.class})
@Configuration
@EnableRetry
@EnableCaching /* (proxyTargetClass=true) */
@EnableAsync /* (proxyTargetClass=true) */
@Slf4j
public class Application {

    public static void main(String[] args) {
        if (!"UTF-8".equals(Charset.defaultCharset().name())) {
            log.error("启动命令错误，默认编码不是UTF8，启动时指定：-Dfile.encoding=UTF-8");
            System.exit(0);
        }
        TimeZone.setDefault(ConstValue.TIME_ZONE);
        SpringApplication app = new SpringApplication(Application.class);
        app.run(args);
    }
}
